<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->libdir.'/tablelib.php');

class local_fm_renderer extends plugin_renderer_base {

    protected function render_local_fm_assign_table(local_fm_assign_table $table) {
        global $PAGE;

        $this->page->requires->js('/question/qengine.js');
        $this->page->requires->js_init_call('M.core_scroll_manager.scroll_to_saved_pos');

        $this->page->requires->yui_module('moodle-local_fm-sidebar',
                  'M.local_fm.init_sidebar',
                   array());

        $this->page->requires->yui_module('moodle-local_fm-assign',
                'M.local_fm.init_assign',
                array(array(
                        'ajaxurl' => '/mod/quiz/report/fbmanager/ajax.php',
                        'config' => array('pageparams' => array('cmid'=>$PAGE->url->param('id'), 'questionid'=>$table->get_question()->id))
                ))
        );

        // Need to create table first to process data
        ob_start();
        $table->set_data($this);
        $table->finish_output();
        $assigntable = ob_get_clean();
        //$PAGE->requires->js_init_call('M.local_fm.sortable_list', array('taglist'));

        echo $this->output->container_start('', 'fm_assign_table_container');

        $errors = $table->get_errors();
        if ($errors) {
            $errormsg = join('<br />', $errors);
            echo $this->output->notification($errormsg);
        }

        if ($table->requires_save()) {
            // Cannot use output->notification as it cleans id attributes from anchor tags
            echo $this->output->container($table->get_change_notification(), 'notifyproblem');
        }

        if ($table->use_pages) {
            $numopts = array(10,25,50,75,100,200,500);
            $options = array_combine($numopts, $numopts);
            $select = new single_select($PAGE->url, 'perpage', $options, $table->pagesize, null);
            $select->set_label(get_string('responsesperpage', 'local_fm'));
            echo $this->output->render($select);
        }

        $submiturl = $PAGE->url;
        $submiturl->remove_all_params();
        $attr = array('id' => $table->formid, 'action' => $submiturl, 'method' => 'post');

        echo html_writer::start_tag('form', $attr);
        echo html_writer::input_hidden_params($PAGE->url);
        // Add sesskey
        echo html_writer::empty_tag('input', array('type'=>'hidden', 'name'=>'sesskey', 'value'=>sesskey()))."\n";

        echo $assigntable;

        $submitall = new local_fm_button($table->formid, 'saveall', get_string('saveall', 'local_fm'));
        $submitall->class .= "centerpara submitall";
        //$confirm = new confirm_action(get_string('saveallconfirm', 'local_fm'));
        //$submitall->add_action($confirm);
        echo html_writer::tag('noscript', $this->render($submitall));

        echo html_writer::end_tag('form');

        echo $this->output->container_end();
    }

    protected function render_local_fm_assign_filters_component(local_fm_assign_filters_component $comp){
        $filters = array();
        foreach($comp->get_filters() as $type => $typefilters){
            foreach($typefilters as $filter) {
                $filters[] = $filter->display();
            }
        }

        echo $this->output->container_start('', 'fm_assign_filters_component');

        $helpicon = $this->help_icon('filters', 'local_fm');
        echo $this->output->heading(get_string('filters', 'local_fm').$helpicon);
        $label = get_string('addfilter', 'local_fm').'...';
        $link  = html_writer::link($comp->get_url(), $label);
        echo html_writer::tag('p', $link, array('class' => 'fm_heading_link'));

        $filterlist = html_writer::alist($filters, array('class' => 'notsortable'));
        echo $this->output->container($filterlist, 'filters');

        echo $this->output->container_end();
    }

    protected function render_local_fm_assign_feedback_component(local_fm_assign_feedback_component $comp){
        global $PAGE;

        $citems = array();
        $qitems = array();
        foreach($comp->get_feedback_instances() as $fbinstance){
            $display = $fbinstance->display();
            if ($fbinstance->in_question_context()){
                $qitems[] = $display;
            } else {
                $citems[] = $display;
            }
        }

        $fbdisp = '';;
        if (!empty($citems)) {
            $fbdisp .= $this->output->heading(get_string('allquestions', 'local_fm'), 3);
            $fbdisp .= html_writer::alist($citems, array('class' => 'sortable'));
        }
        if (!empty($qitems)) {
            $fbdisp .= $this->output->heading(get_string('thisquestion', 'local_fm'), 3);
            $fbdisp .= html_writer::alist($qitems, array('class' => 'sortable'));
        }
        //$PAGE->requires->js_init_call('M.local_fm.sortable_list', array('sortable'));

        echo $this->output->container_start('', 'fm_assign_feedback_component');

        $helpicon = $this->help_icon('availablefeedback', 'local_fm');
        echo $this->output->heading(get_string('availablefeedback', 'local_fm').$helpicon);
        $label = get_string('addfrombank', 'local_fm').'...';
        $link = html_writer::link($comp->get_url(), $label);
        echo html_writer::tag('p', $link, array('class' => 'fm_heading_link'));
        echo $this->output->container($fbdisp, 'feedback_instances clearfix');

        echo html_writer::empty_tag('hr');

        // New feedback definition
        $helpicon = $this->help_icon('newfeedback', 'local_fm');
        echo $this->output->heading(get_string('newfeedback', 'local_fm').$helpicon);

        $newfbtable = new html_table();
        $newfbtable->attributes['class'] = 'new_feedback_table';
        $newfbtable->head = array(get_string('label', 'local_fm'), get_string('text', 'local_fm'));
        $row = array();
        $attr = array('type' => 'text', 'name' => "name", 'value' => '', 'size' => 5);
        $row[] = html_writer::empty_tag('input', $attr);
        $attr = array('name' => "text", 'rows' => 5);
        $row[] = html_writer::tag('textarea', '', $attr);
        $newfbtable->data[] = $row;

        $submiturl = $PAGE->url;
        $submiturl->remove_all_params();    // Make sure the form posts all parameters
        $contents = html_writer::input_hidden_params($PAGE->url);
        // Add sesskey
        $contents .= html_writer::empty_tag('input', array('type'=>'hidden', 'name'=>'sesskey', 'value'=>sesskey()))."\n";
        $contents .= html_writer::table($newfbtable);
        $addtoquiz = html_writer::checkbox('addtoquiz', 1, 0, get_string('addfeedbacktoquiz', 'local_fm'));
        $helpicon = $this->help_icon('addfeedbacktoquiz', 'local_fm');
        $contents .= html_writer::tag('p', $addtoquiz.$helpicon, array('class' => 'centerpara'));
        $attr = array('type' => 'submit', 'name' => 'addnew', 'value' => get_string('addnewfeedbacksubmit', 'local_fm'));
        $submitbutton = html_writer::empty_tag('input', $attr);
        $contents .= html_writer::tag('p', $submitbutton, array('class' => 'centerpara'));

        $formattr = array('id' => 'newfeedback', 'action' => $submiturl, 'method' => 'post');
        echo $this->output->container(html_writer::tag('form', $contents, $formattr));

        echo $this->output->container_end();
    }

    public function render_cell_response(question_attempt $attempt){
        if ($attempt->get_question() instanceof qtype_essay_question) {
            $step = $attempt->get_last_step_with_qt_var('answer');
            $text   = $step->get_qt_var('answer');
            $format = $step->get_qt_var('answerformat');
            //$options = array('context' => $this->get_context());
            $options = array('nocache'=>1);
            $responsetext = format_text($text, $format, $options);
        } else {
            $responsetext = $attempt->get_response_summary();
        }

        return html_writer::tag('div', $responsetext, array('class'=>'no-overflow'));
    }
/*
    public function render_attempt_comment(question_attempt $attempt, local_fm_manager $manager) {
        $assignments = $manager->get_attempt_assignments($attempt);
        $allfeedback = array_merge(
                $assignments->get_feedback_custom('sent'),
                $assignments->get_feedback_custom('current'),
                $assignments->get_feedback_assigns('sent'),
                $assignments->get_feedback_assigns('current')
                );
        $allhtml = array();
        foreach ($allfeedback as $feedback) {
            $allhtml[] = text_to_html($feedback->get_text());
        }

        $html = html_writer::tag('h2', 'Instructor feedback');

        $html .= html_writer::alist($allhtml, array(), 'ol');
        return $html;
    }
*/
    public function render_cell_custom_feedback(question_attempt $attempt, local_fm_manager $manager,
                                                $formid, $formdata = false){

        $attemptid = $attempt->get_database_id();
        $assignments = $manager->get_attempt_assignments($attempt);

        $cell = '';
        if ($assignments->has_feedback_custom('sent')) {
            $items = array();
            foreach($assignments->get_feedback_custom('sent') as $custom){
                $items[] = $custom->get_text();
            }
            $cell .= html_writer::alist($items);
        }
        $hasfeedback = $assignments->has_feedback_custom('current');
        if ($formdata === false) {
            if ($hasfeedback) {
                $current = $assignments->get_feedback_custom('current');
                $current = reset($current);
                $text = $current->get_text();
            } else {
                $text = '';
            }
        } else {
            $text = $formdata;
        }
        $submitlabel = $hasfeedback ? get_string('save', 'mod_quiz') : get_string('add');

        $attr = array('name' => "customfeedback[$attemptid]");
        if ($formdata !== false) {
            $attr['class'] = "needsave";
        }
        $textarea = html_writer::tag('textarea', $text, $attr);
        $button = new local_fm_button($formid, "addfeedback[$attemptid]", $submitlabel);
        $button->save_scroll();
        $submit = $this->render($button);
        $cell .= "$textarea <span class=\"spinner\"></span> <noscript> $submit </noscript>";

        return $cell;
    }

    public function render_cell_feedback(question_attempt $attempt, local_fm_manager $manager,
                                                $formid) {
        $attemptid = $attempt->get_database_id();

        $assignments = $manager->get_attempt_assignments($attempt);
        $menu    = $assignments->get_menu_options($manager);
        $current = $assignments->get_menu_current();
        $sent    = $assignments->get_menu_sent();

        $cell = '';
        $cell .= $this->output->container_start('taglist');
        $cell .= $this->render_cell_existingfeedback($attempt, $manager);
        $cell .= $this->output->container_end();

        $formname = "assignfeedback[$attemptid]";
        $options = array();
        $empty = true;
        foreach($menu as $type => $typeoptions) {
            if (empty($typeoptions)) {
                continue;
            }
            $empty = false;
            $options[] = array(get_string('feedbackmenu'.$type, 'local_fm') => $typeoptions);
        }
        if (!$empty) {
            $nothing = array(null => get_string('assign', 'local_fm').'...');
            $select  = new local_fm_select($formid, $formname, $options, null, $nothing, false);
            $select->class .= ' assignfeedback ajax';
            $select->save_scroll();
            $cell .= $this->render($select);
        }
        $cell .= '<span class="spinner"></span>';

        return $cell;
    }

    function render_cell_existingfeedback(question_attempt $attempt, local_fm_manager $manager){
        $assignments = $manager->get_attempt_assignments($attempt);
        $current = $assignments->get_menu_current();
        $sent    = $assignments->get_menu_sent();

        $cell = '';
        if (!empty($current)) {
            $cell .= html_writer::alist($current, array('class' => 'sortable'));
        }
        if (!empty($sent)) {
            $cell .= html_writer::alist($sent, array('class' => 'notsortable'));
        }
        return $cell;
    }

    public function render_cell_grade(question_attempt $attempt, local_fm_manager $manager, $cangrade,
                               $formid, $formdata = false) {
        $attemptid = $attempt->get_database_id();

        $maxmark = $attempt->format_max_mark(2);
        if ($formdata === false) {
            $mark = $attempt->format_mark(2);
        } else {
            $mark = $formdata;
        }

        if (!$cangrade || !$attempt->get_state()->is_finished()) {
            $cell = "$mark / $maxmark";
        } else {
            $attr = array(
                    'type'  => 'text',
                    'size'  => 3,
                    'name'  => "grade[$attemptid]",
                    'value' => $mark,
                    'autocomplete' => 'off',
            );
            if ($formdata !== false) {
                $attr['class'] = 'needsave';
            }
            $cell = html_writer::empty_tag('input', $attr);
            $cell .= " / $maxmark";
            $cell .= '<span class="spinner"> </span>';
            $submitlabel = get_string('save', 'mod_quiz');
            $button = new local_fm_button($formid, "submitgrade[$attemptid]", $submitlabel);
            $button->save_scroll();
            $cell .=  '<noscript>' . $this->render($button) . '</noscript>';
        }

        return $cell;
    }

    public function render_cell_select(question_attempt $attempt){
        $attemptid = $attempt->get_database_id();

        // TODO: implement is_selected() method to develop count
        return html_writer::checkbox("selected[$attemptid]", 1, false, '', array('tabindex'=>"-1"));
    }

    public function render_cell_user(local_fm_group $group, $attemptuser){
        return $group->get_name($attemptuser);
    }

    public function render_cell_picture(local_fm_group $group){
        return $this->render($group->get_picture());
    }

    public function render_local_fm_group_picture(local_fm_group_picture $grouppicture) {
        return print_group_picture($grouppicture->group, $grouppicture->courseid, false, true, true);
    }

    protected function render_local_fm_assign_questions_component(local_fm_assign_questions_component $comp){
        echo $this->output->container_start('', 'fm_assign_questions_component');

        $select = $comp->get_question_select();
        echo $this->output->container($this->output->render($select), 'questionname');

        echo $this->output->container($comp->format_question_text(), 'questiontext');

        echo $this->output->container_end();
    }

    /**
     * Rendering of an implementation of a single_button equivalent inside a form.
     * @param local_fm_button $button
     * @return string HTML fragment
     */
    protected function render_local_fm_button(local_fm_button $button) {
        $button = clone($button);

        if (empty($button->attributes['id'])) {
            $button->attributes['id'] = html_writer::random_id('local_fm_button');
        }
        $id = $button->attributes['id'];

        // Add scroll positioning
        if ($button->savescroll) {
            $scroll = new component_action('click', 'M.core_scroll_manager.save_scroll_action');
            $button->actions[] = $scroll;
        }

        $button->attributes['type'] = 'submit';
        $button->attributes['value'] = $button->label;
        $button->attributes['class'] = 'form_submit';

        if ($button->name) {
            $button->attributes['name'] = $button->name;
        }

        if ($button->disabled) {
            $button->attributes['disabled'] = 'disabled';
        }

        if ($button->tooltip) {
            $button->attributes['title'] = $button->tooltip;
        }

        if ($button->actions) {
            foreach ($button->actions as $action) {
                $this->add_action_handler($action, $id);
            }
        }

        // first the input element
        $output = html_writer::empty_tag('input', $button->attributes);

        // and a wrapper with class
        return html_writer::tag('div', $output, array('class' => $button->class));
    }

    protected function render_local_fm_category_display(local_fm_category_display $catdisp){
        global $PAGE;

        $context    = $catdisp->get_context();
        $categories = $catdisp->get_categories();

        if (empty($categories)) {
            echo $this->output->notification(get_string('nothingdefinedincontext', 'local_fm'));
        }

        foreach($categories as $category){
            $this->render(new local_fm_feedback_table($category));
        }
    }

    protected function render_local_fm_feedback_configure(local_fm_feedback_configure $fbconfig) {
        echo html_writer::start_tag('div', array('id' => 'fm_feedback_container'));

        echo $this->output->heading(get_string('feedbackspecificto', 'local_fm'), 3);

        $fbconfig->print_nav_tabs();

        $viewcontext = $fbconfig->get_context_view();
        $currentview = $viewcontext->get_context_name(false);

        echo $this->output->heading($currentview, 4);

        // Manage link
        $params = array();
        $params['context'] = $viewcontext->id;

        if ($fbconfig->can_manage_feedback()) {
            $manageurl  = new moodle_url('/local/fm/bank/edit.php', $params);
            $action = new popup_action('click', $manageurl, 'popup', array('height' =>  600, 'width' => 900));
            $managelink = $this->output->action_link($manageurl, get_string('managefeedbackbankhere', 'local_fm'), $action);
            echo html_writer::tag('p', $managelink, array('class' => 'centerpara'));
        }

        if (optional_param('saved', false, PARAM_BOOL)) {
            echo $this->output->notification(get_string('changessaved'), 'notifysuccess');
        }

        // Form setup
        $posturl = $fbconfig->get_url_current();
        $submiturl = clone($posturl);
        $submiturl->remove_all_params();
        $formattr = array('id' => $fbconfig->formid, 'action' => $submiturl, 'method' => 'post');
        echo html_writer::start_tag('form', $formattr);
        echo html_writer::input_hidden_params($posturl);
        // Add sesskey
        echo html_writer::empty_tag('input', array('type'=>'hidden', 'name'=>'sesskey', 'value'=>sesskey()))."\n";

        $tables = $fbconfig->get_instance_tables();
        if (empty($tables)) {
            echo $this->output->notification(get_string('nofeedbackhere', 'local_fm'));
        } else {
            foreach ($tables as $table) {
                $this->render($table);
            }
        }

        $attr = array('type' => 'submit', 'name' => 'cancel', 'value' => get_string('cancel'));
        $cancel = html_writer::empty_tag('input', $attr);
        $attr = array('type' => 'submit', 'name' => 'save', 'value' => get_string('savechanges'));
        $save = html_writer::empty_tag('input', $attr);
        echo html_writer::tag('p', $cancel.' '.$save, array('class' => 'centerpara'));

        echo html_writer::end_tag('form');

        echo html_writer::end_tag('div');
    }

    protected function render_local_fm_feedback_instance_table(local_fm_feedback_instance_table $table) {
        $table->setup();

        echo html_writer::tag('h5', $table->get_category_name(), array('class' => 'categoryname'));
        echo html_writer::table($table);
    }

    protected function render_local_fm_feedback_table(local_fm_feedback_table $table){
        global $PAGE;

        $category = $table->get_category();
        if ($category) {
            $children = $category->get_children();
        } else {
            $children = array();
        }

        echo $this->output->box_start('feedback_category');

        // Category information
        if ($category) {
            echo $this->output->box_start('generalbox boxaligncenter');

            $title = html_writer::span($category->get_name(), 'category_title');
            $title .= html_writer::span($table->get_heading_controls($category));
            echo $this->output->box($title, 'category_header');
            echo $this->output->box_end();
            echo $this->output->box_start('generalbox boxaligncenter');
            echo $this->output->container($category->get_info(), 'category_info');
            echo $this->output->box_end();
        }

        $table->setup();
        $table->set_data();

        if (empty($table->data)) {
            echo $this->output->notification(get_string('nofeedbackhere', 'local_fm'));
        } else {
            echo html_writer::table($table);
        }

        if ($table->can_manage_feedback()) {
            $params = array();
            $params['category'] = $table->get_category()->id;
            $addurl = new moodle_url('/local/fm/bank/feedback.php', $params);
            $contents = $this->output->single_button($addurl, get_string('addnewfeedback', 'local_fm'));
            $subcat = '';
            if ($category && $category->can_manage()) {
                $params = array(
                        'context' => $category->get_context()->id,
                        'parent'  => $category->id,
                        'category'=> $PAGE->url->get_param('category'),
                );
                $addurl = new moodle_url('/local/fm/bank/category.php', $params);
                $subcat .= $this->output->single_button($addurl, get_string('addnewsubcategory', 'local_fm'));
            }
            $contents .= html_writer::tag('div', $subcat, array('class' => 'contents'));
            echo $this->output->container($contents, 'centerselect');
        }

        // Render any children.
        foreach ($children as $child) {
            $this->render(new local_fm_feedback_table($child));
        }
        echo $this->output->box_end();
    }

    protected function render_local_fm_send_preview(local_fm_send_preview $preview){
        global $PAGE;

        // Build groups select list if is needed
        if($preview->use_groups()) {
            $label = get_string('selectagroup', 'local_fm').':';
        } else {
            $label = get_string('selectauser', 'local_fm').':';
        }

        $groupmenu = $preview->get_preview_group_menu();
        asort($groupmenu);

        if (empty($groupmenu)) {
            echo $this->output->notification(get_string('nofeedbacktosend', 'local_fm'));
            return;
        }
        echo $this->output->box_start('generalbox', 'quiz_fbmanager_message_preview');
        $selectedgroup = optional_param($preview::PARAM_GROUP, 0, PARAM_INT);
        if (!$selectedgroup) {
            reset($groupmenu);
            $selectedgroup = key($groupmenu);
        }
        $gselect = new single_select($PAGE->url, $preview::PARAM_GROUP, $groupmenu, $selectedgroup, null);
        $gselect->label = $label;
        echo $this->output->render($gselect);
        echo html_writer::start_div('switchformat');
        if ($preview->get_format() == FORMAT_HTML) {
            echo html_writer::tag('a', get_string('switchtoplain', 'local_fm'), array('href'=>$PAGE->url->out(false, array('format'=>FORMAT_PLAIN, $preview::PARAM_GROUP => $selectedgroup))));
        } else {
            echo html_writer::tag('a', get_string('switchtohtml', 'local_fm'), array('href'=>$PAGE->url->out(false, array('format'=>FORMAT_HTML, $preview::PARAM_GROUP => $selectedgroup))));
        }
        echo html_writer::end_div();

        if ($preview->has_message($selectedgroup)) {
            echo $this->output->box_start('generalbox message');
            $message = $preview->create_message($selectedgroup);
            echo $this->output->box(html_writer::tag('strong', get_string('emailsubject', 'local_fm') . ': ').$message->get_subject(), 'generalbox subject');
            if ($preview->get_format() == FORMAT_HTML) {
                echo $this->output->box($message->get_message_html(), 'generalbox text');
            } else {
                echo $this->output->box(html_writer::tag('pre', $message->get_message_plain()), 'generalbox text');
            }
            echo $this->output->box_end();
        } else {
            echo $this->output->notification(get_string('nofeedbacktosend', 'local_fm'));
        }
        echo $this->output->box_end();
    }

    /**
     * Rendering of an implementation of a single_select equivalent inside a form.
     * @param single_select $select
     * @return string HTML fragment
     */
    protected function render_local_fm_select(local_fm_select $select) {
        $select = clone($select);

        if (empty($select->attributes['id'])) {
            $select->attributes['id'] = html_writer::random_id('local_fm_select');
        }
        $id = $select->attributes['id'];

        // Add scroll positioning
        if ($select->savescroll) {
            $scroll = new component_action('focus', 'M.core_scroll_manager.save_scroll_action');
            $select->actions[] = $scroll;
        }
        if ($select->actions) {
            foreach ($select->actions as $action) {
                $this->add_action_handler($action, $id);
            }
        }

        if ($select->disabled) {
            $select->attributes['disabled'] = 'disabled';
        }

        if ($select->tooltip) {
            $select->attributes['title'] = $select->tooltip;
        }

        $output = '';
        if ($select->label) {
            $output .= html_writer::label($select->label, $id);
        }

        if ($select->helpicon instanceof help_icon) {
            $output .= $this->render($select->helpicon);
        }

        if ($select->autosubmit) {
            if (isset($select->attributes['class'])) {
                $select->attributes['class'] .= ' autosubmit';
            } else {
                $select->attributes['class'] = 'autosubmit';
            }
        }

        $output .= html_writer::select($select->options, $select->name, $select->selected, $select->nothing, $select->attributes);

        $go = html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('go')));
        $output .= html_writer::tag('noscript', html_writer::tag('div', $go), array('style' => 'inline'));

        return html_writer::tag('div', $output, array('class' => $select->class));
    }
}

class local_fm_form_submit implements renderable {

    /**
     * @var array List of attached actions
     */
    var $actions = array();

    /**
     * Extra select field attributes
     * @var array
     */
    var $attributes = array();

    /**
     * @var string Form id
     */
    var $formid = null;

    /**
     * @var string Submit name
     */
    var $name;

    /**
     * @var bool Whether to save scroll position
     */
    var $savescroll = false;

    /**
     * @var bool whether to
     */

    /**
     * Shortcut for adding a JS confirm dialog when the button is clicked.
     * The message must be a yes/no question.
     * @param string $message The yes/no confirmation question. If "Yes" is clicked, the original action will occur.
     * @return void
     */
    public function add_confirm_action($confirmmessage) {
        $this->add_action(new component_action('submit', 'M.util.show_confirm_dialog', array('message' => $confirmmessage)));
    }

    /**
     * Add action to the button.
     * @param component_action $action
     * @return void
     */
    public function add_action(component_action $action) {
        $this->actions[] = $action;
    }

    public function save_scroll($save = true){
        $this->savescroll = $save;
    }
}

/**
 * Implements an in-form submit button which is mostly a copy of single_button.
 * @author nickkoeppen
 * @category output
 */
class local_fm_button extends local_fm_form_submit {

    /**
     * @var string Button label
     */
    var $label;

    /**
     * @var string Wrapping div class
     */
    var $class = 'local_fm_button clearfix';

    /**
     * @var bool True if button disabled, false if normal
     */
    var $disabled = false;

    /**
     * @var string Button tooltip
     */
    var $tooltip = null;

    /**
     * Constructor
     * @param string $formid id of the form
     * @param string $name submit name
     * @param string $label button text
     */
    public function __construct($formid, $name='', $label='Submit', $class='') {
        $this->formid = $formid;
        $this->name   = $name;
        $this->label  = $label;
        if (!empty($class)) {
            $this->class .= " $class";
        }
    }

}
/**
 * Implements an in-form select which is mostly a copy of single_select.
 * @author nickkoeppen
 */
class local_fm_select extends local_fm_form_submit {

    /**
     * @var array $options associative array value=>label ex.:
     *              array(1=>'One, 2=>Two)
     *              it is also possible to specify optgroup as complex label array ex.:
     *                array(array('Odd'=>array(1=>'One', 3=>'Three)), array('Even'=>array(2=>'Two')))
     *                array(1=>'One', '--1uniquekey'=>array('More'=>array(2=>'Two', 3=>'Three')))
     */
    var $options;
    /**
     * Selected option
     * @var string
     */
    var $selected;
    /**
     * Nothing selected
     * @var array
     */
    var $nothing;
    /**
     * True to hide the submit button and submit on change
     * @var array
     */
    var $autosubmit;

    /**
     * Button label
     * @var string
     */
    var $label = '';
    /**
     * Wrapping div class
     * @var string
     * */
    var $class = 'local_fm_select clearfix';
    /**
     * True if button disabled, false if normal
     * @var boolean
     */
    var $disabled = false;
    /**
     * Button tooltip
     * @var string
     */
    var $tooltip = null;

    /**
     * @var help_icon
     */
    var $helpicon = null;

    /**
     * Constructor
     * @param string $formid id of the form
     * @param string $name name of selection field - the changing parameter in url
     * @param array $options list of options
     * @param string $selected selected element
     * @param array $nothing
     * @param boolean $autosubmit
     */
    public function __construct($formid, $name, array $options, $selected='', $nothing=array(''=>'choosedots'), $autosubmit = true) {
        $this->formid     = $formid;
        $this->name       = $name;
        $this->options    = $options;
        $this->selected   = $selected;
        $this->nothing    = $nothing;
        $this->autosubmit = $autosubmit;
    }

    /**
     * Adds help icon.
     * @param string $identifier The keyword that defines a help page
     * @param string $component
     * @param bool $linktext add extra text to icon
     * @return void
     */
    public function set_help_icon($identifier, $component = 'moodle') {
        $this->helpicon = new help_icon($identifier, $component);
    }

    /**
     * Sets select's label
     * @param string $label
     * @return void
     */
    public function set_label($label) {
        $this->label = $label;
    }
}
