<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_local_fm_upgrade($oldversion=0) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    // Add reports DB table
    if ($oldversion < 2012090700) {
        $file = $CFG->dirroot.'/local/fm/db/install.xml';
        $dbman->install_one_table_from_xmldb_file($file, 'fm_reports');

        upgrade_plugin_savepoint(true, 2012090700, 'local', 'fm');
    }
    // Add type field to message template definitions
    if ($oldversion < 2012091800) {
        $table = new xmldb_table('fm_templates');
        $field = new xmldb_field('type', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, false, null, 'contextid');
        $dbman->add_field($table, $field);

        // Only quiz_fm created so far
        $DB->set_field('fm_templates', 'type', 'quiz');

        upgrade_plugin_savepoint(true, 2012091800, 'local', 'fm');
    }
    if ($oldversion < 2013121800) {
        // Define table local_fm_feedback_grade to be created
        $table = new xmldb_table('local_fm_feedback_grade');

        // Adding fields to table local_fm_feedback_grade
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('questionattemptid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table local_fm_feedback_grade
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('questionattemptid', XMLDB_KEY_FOREIGN, array('questionattemptid'), 'question_attempts', array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Conditionally launch create table for local_fm_feedback_grade
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // fm savepoint reached
        upgrade_plugin_savepoint(true, 2013121800, 'local', 'fm');
    }
    if ($oldversion < 2014082600) {

        // Define field text to be dropped from local_fm_reports.
        $table = new xmldb_table('local_fm_reports');
        $field = new xmldb_field('type');

        // Conditionally launch drop field text.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Define field text to be dropped from local_fm_reports.
        $table = new xmldb_table('local_fm_reports');
        $field = new xmldb_field('showgrades');

        // Conditionally launch drop field text.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Define field text to be dropped from local_fm_reports.
        $table = new xmldb_table('local_fm_reports');
        $field = new xmldb_field('showfbcount');

        // Conditionally launch drop field text.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Define field config to be added to local_fm_reports.
        $table = new xmldb_table('local_fm_reports');
        $field = new xmldb_field('config', XMLDB_TYPE_TEXT, null, null, null, null, null, 'textformat');

        // Conditionally launch add field config.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Fm savepoint reached.
        upgrade_plugin_savepoint(true, 2014082600, 'local', 'fm');
    }
    if ($oldversion < 2014102100) {

        // Define table local_fm_category_share to be dropped.
        $table = new xmldb_table('local_fm_category_share');

        // Conditionally launch drop table for local_fm_category_share.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Define table local_fm_feedback_share to be dropped.
        $table = new xmldb_table('local_fm_feedback_share');

        // Conditionally launch drop table for local_fm_feedback_share.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Fm savepoint reached.
        upgrade_plugin_savepoint(true, 2014102100, 'local', 'fm');
    }
    return true;
}