<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/message/lib.php');

function xmldb_local_fm_install() {
    global $DB;

    $result = true;

    $dbman = $DB->get_manager();

    // Move old plugin tables to new plugin
    $tables = array(
            'fm_category', 'fm_category_share', 'fm_feedback', 'fm_feedback_share',
            'fm_feedback_instance', 'fm_feedback_assign', 'fm_feedback_custom',
            'fm_messages', 'fm_templates', 'fm_reports'
    );
    foreach($tables as $tablename){
        $table = new xmldb_table($tablename);
        if ($dbman->table_exists($table)) {
            $newtablename = 'local_'.$tablename;
            $newtable = new xmldb_table($newtablename);
            $dbman->drop_table($newtable);
            $dbman->rename_table($table, $newtablename);
        }
    }

    // Default quiz definition
    $definition = new stdClass();
    $definition->contextid = SYSCONTEXTID;
    $definition->type      = 'quiz';
    $definition->subject   = get_string('systemplatesubject', 'local_fm');
    $definition->message   = get_string('systemplatemessage', 'local_fm');
    $definition->messageformat = FORMAT_HTML;
    $template = local_fm_message_template::create($definition);
    $template->save();

    return $result;
}