<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/local/fm/lib.php');

/**
 * Base message class needed since we have need to keep message data since Moodle
 * message data moves from {messages} to {messages_read} without any means of
 * tracking the data via an event or otherwise.
 *
 * TODO: Track Moodle messaging to remove message duplication
 */
class local_fm_message_base extends local_fm_contextual{

    static function can_manage_in_context(context $context){
        if (parent::can_manage_in_context($context)) {
            return true;
        }

        return has_capability('local/fm:messageconfig', $context);
    }

    protected $subject;
    protected $message;
    protected $messageformat;

    function get_db_record(){
        $record = parent::get_db_record();
        $record->subject       = $this->subject;
        $record->message       = $this->message;
        $record->messageformat = $this->messageformat;

        return $record;
    }

    function get_message($filter = false){
        $options = array('context' => $this->get_context(), 'filter' => $filter);
        return format_text($this->message, $this->messageformat, $options);
    }

    function get_message_format(){
        return $this->messageformat;
    }

    function get_subject($filter = false){
        $options = array('context' => $this->get_context(), 'filter' => $filter);
        return format_string($this->subject, true, $options);
    }
}

class local_fm_message extends local_fm_message_base {
    const DB_TABLE = 'local_fm_messages';

    protected $messagehtml;
    protected $time;
    protected $useridfrom;
    protected $_userfrom;
    protected $useridto;
    protected $_userto;

    function get_db_record(){
        $record = parent::get_db_record();
        $record->messagehtml = $this->messagehtml;
        $record->time        = $this->time;
        $record->useridfrom  = $this->useridfrom;
        $record->useridto    = $this->useridto;

        return $record;
    }

    function get_message_html(){
        return $this->messagehtml;
    }

    function get_message_plain(){
        return $this->message;
    }

    function get_time($format = null){
        return userdate($this->time, $format);
    }

    protected function get_user($userid){
        global $DB;

        return $DB->get_record('user', array('id' => $userid));
    }

    function get_user_from(){
        if (!isset($this->_userfrom)) {
            $this->_userfrom = $this->get_user($this->useridfrom);
        }

        return $this->_userfrom;
    }

    function get_user_to(){
        if (!isset($this->_userto)) {
            $this->_userto = $this->get_user($this->useridto);
        }

        return $this->_userto;
    }

    function send(){
        $eventdata = new stdClass();
        $eventdata->component         = $this->send_component();
        $eventdata->name              = 'question_feedback';
        $eventdata->userfrom          = $this->get_user_from();
        $eventdata->userto            = $this->get_user_to();
        $eventdata->subject           = $this->get_subject();
        $eventdata->fullmessage       = $this->get_message();
        $eventdata->fullmessageformat = $this->get_message_format();
        $eventdata->fullmessagehtml   = $this->get_message_html();
        $eventdata->smallmessage      = '';

        $result = message_send($eventdata);
        if ($result){
            $this->time = time();
            $this->save();
            return true;
        }
        return false;
    }

    protected function send_component(){
        return 'local_fm';
    }
}

class local_fm_message_template extends local_fm_message_base {
    const DB_TABLE = 'local_fm_templates';

    protected $type;

    static function get_for_context(context $context, $type = null){
        global $DB;

        $params = array('type' => $type);
        foreach($context->get_parent_contexts(true) as $acontext) {
            $params['contextid'] = $acontext->id;
            $record = $DB->get_record(self::DB_TABLE, $params, '*', IGNORE_MULTIPLE);
            if ($record) {
                return static::create($record);
            }
        }

        // No template exists - shouldn't happen unless site definition is gone
        $definition = new stdClass();
        $definition->contextid = $context->id;
        return static::create($definition);
    }

    static function context_get_template_id($contextid, $type = null){
        global $DB;

        $params = array('type' => $type, 'contextid' => $contextid);
        return $DB->get_field(self::DB_TABLE, 'id', $params);
    }

    /**
     * Create message string by substituting message variable data provided in $messagedata.
     * @param array $messagedata
     * @param int|NULL $format [FORMAT_MOODLE, FORMAT_HTML, FORMAT_PLAIN, FORMAT_MARKDOWN]
     * @return string
     */
    function create_message($messagedata, $format = FORMAT_HTML){
        if ($format == FORMAT_PLAIN && $this->messageformat == FORMAT_HTML) {
            $message = html_to_text($this->message);
        } else {
            $message = $this->message;
        }
        $text = $this->substitute_variables($message, $messagedata);
        return $text;
    }

    /**
     * Create message subject string by substituting message variable data provided in $messagedata.
     * @param array $messagedata
     * @return string
     */
    function create_subject($messagedata){
        $string = $this->substitute_variables($this->subject, $messagedata);
        return $string;
    }

    function form_load_data($formdata){
        $formdata->messageformat = $formdata->message['format'];
        $formdata->message       = $formdata->message['text'];
        parent::form_load_data($formdata);
    }

    function form_set_data(MoodleQuickForm $mform){
        $formdata = $this->get_db_record();
        $formdata->message = array(
            'text' => $formdata->message,
            'format' => $formdata->messageformat,
        );

        $mform->setDefaults((array)$formdata);
    }

    function format_message($descripts = true, $format = null){
        return $this->format_text($this->message, $descripts, $format);
    }

    function format_subject($descripts = true, $format = null){
        return $this->format_text($this->subject, $descripts, $format);
    }

    protected function format_text($text, $descripts = true, $format = null){
        if ($descripts) {
            $text = preg_replace('/\$(\w+)/', '<span class="messagevar messagevar_\1">$\1</span>', $text);
        }
        if (!isset($format)) {
            $format = $this->messageformat;
        }

        // TODO handle message variable styling with filters?
        return format_text($text, $format, array('filter' => false));
    }

    function get_db_record(){
        $record = parent::get_db_record();
        $record->type = $this->type;

        return $record;
    }

    protected function substitute_variables($string, $data){
        $data = (array)$data;

        $patterns = array();
        $values = array();

        foreach ($data as $key=>$value) {
            $patterns[] = '/'.preg_quote($key).'(?![a-zA-Z0-9_])/'; // match only whole words
            $values[] = $value;
        }
        return preg_replace($patterns, $values, $string);
    }
}

