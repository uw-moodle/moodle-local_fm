<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');

/**
 * Form to configure assigned feedback in order to send select feedback.
 * @author nickkoeppen
 */
class local_fm_send_form extends moodleform {

    protected $datacache = false;
    protected $_fbdata;
    protected $_users;
    /**
     * A feedback manager
     * @var local_fm_manager
     */
    protected $manager;

    function definition(){
        $mform = $this->_form;

        $this->manager = $this->_customdata['manager'];

        $sessdata = $this->get_configuration();

        // Assigners
        $mform->addElement('header', 'questionheader', get_string('feedbacksendassignedby', 'local_fm'));

        $opts = array('group' => 1);
        $names = array();
        foreach($this->get_all_assigners() as $uid => $assigner) {
            $elname = "assigners[$uid]";
            // TODO: Capabilities - disable if you can't
            $mform->addElement('advcheckbox', $elname, '', fullname($assigner), $opts);
            $names[$uid] = $elname;
        }
        $all = empty($sessdata) || (count($sessdata->assigners) == count($names));
        $this->add_checkbox_controller(1, get_string('allornone', 'local_fm'), null, $all);

        // Must set constant values again after creating checkbox controller
        if (!$all) {
            foreach($names as $uid => $name){
                $value = $sessdata ? in_array($uid, $sessdata->assigners) : 1;
                $mform->setConstant($name, $value);
            }
        }

        // Questions
        $mform->addElement('header', 'questionheader', get_string('feedbacksendquestions', 'local_fm'));

        $opts = array('group' => 2);
        $names = array();
        foreach($this->get_all_questions() as $qid => $question){
            $elname = "questions[$qid]";
            $mform->addElement('advcheckbox', $elname, '', $question->name, $opts);
            //$mform->addElement('static', 'qtext'.$qid, '', $question->questiontext);
            $names[$qid] = $elname;
        }
        $all = empty($sessdata) || (count($sessdata->questions) == count($names));
        $this->add_checkbox_controller(2, get_string('allornone', 'local_fm'), null, $all);

        // Must set constant values again after creating checkbox controller
        if (!$all) {
            foreach($names as $qid => $name){
                $value = $sessdata ? in_array($qid, $sessdata->questions) : 1;
                $mform->setConstant($name, $value);
            }
        }

        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('submit', 'send', get_string('sendfeedback', 'quiz_fbmanager'), array('class'=>'form-submit'));
        $buttonarray[] = &$mform->createElement('submit', 'preview', get_string('previewfeedback', 'quiz_fbmanager'));
        $buttonarray[] = &$mform->createElement('submit', 'edittemplate', get_string('edittemplate', 'quiz_fbmanager'));
        //$buttonarray[] = &$mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }

    /**
     * @see create_message defined in local_fm_manager
     */
    function create_message($userfrom, $userto, $fbdata){
        return $this->manager->create_message($userfrom, $userto, $fbdata);
    }

    function get_all_assigners(){
        return $this->manager->get_assigners();
    }

    function get_all_questions(){
        return $this->manager->get_questions();
    }

    function get_all_users(){
        return $this->manager->get_attempt_users(true);
    }

    function get_assigner_ids(){
        $data = $this->get_data();
        if ($data) {
            $ids = array_keys(array_filter($data->assigners));
        } else if ($this->has_configuration()) {
            $ids = $this->get_configuration()->assigners;
        } else {
            $ids = array_keys($this->get_all_assigners());
        }

        return $ids;
    }

    function get_configuration(){
        global $SESSION;

        $sesskey = sesskey();
        return !empty($SESSION->fm[$sesskey]) ? $SESSION->fm[$sesskey] : null;
    }

    function get_question_ids(){
        $data = $this->get_data();
        if ($data) {
            $ids = array_keys(array_filter($data->questions));
        } else if ($this->has_configuration()) {
            $ids = $this->get_configuration()->questions;
        } else {
            $ids = array_keys($this->get_all_questions());
        }

        return  $ids;
    }

    function has_configuration(){
        global $SESSION;

        return !empty($SESSION->fm[sesskey()]);
    }

    /**
     * Send feedback for the configuration specified by this form.
     * @return boolean
     */
    function send_feedback(){
        $qids = $this->get_question_ids();
        $assignerids = $this->get_assigner_ids();
        $users = $this->manager->get_current_feedback_users($qids, $assignerids);

        // Go through the users and get their group info
        $groups = array();
        foreach($users as $userid => $user){
            $group = $this->manager->get_groupmanager()->get_user_group($userid);
            $groups[$group->get_uniqueid()] = $group;
        }

        // Now process each group
        $result = true;
        foreach ($groups as $fmgroupid => $group) {
            $fbdata = $this->manager->get_fbdata($qids, $assignerids, $fmgroupid);
            $result |= $this->manager->send_feedback($group->members, $fbdata);
        }
        return $result;
    }

    function update_configuration(){
        global $SESSION;

        if (!isset($SESSION->fm)) {
            $SESSION->fm = array();
        }
        $sesskey = sesskey();
        if (!isset($SESSION->fm[$sesskey])) {
            $SESSION->fm[$sesskey] = new stdClass();
        }

        $SESSION->fm[$sesskey]->assigners = $this->get_assigner_ids();
        $SESSION->fm[$sesskey]->questions = $this->get_question_ids();
    }

    /**
     * Gets the fm instance settings
     *
     * @return object settings
     */
    public function get_fm_settings(){
        return $this->manager->get_instance_settings();
    }

    /**
     * Returns group for the userid specified
     *
     * @param int $userid
     *
     * @return int
     */
    public function get_user_group($userid){
        return $this->manager->get_groupmanager()->get_user_group($userid);
    }

    /**
     * Returns group for the group uniqueid specified
     *
     * @param int $uniqueid
     *
     * @return local_fm_group | false
     */
    public function get_group_by_groupid($uniqueid){
        return $this->manager->get_groupmanager()->get_group($uniqueid);
    }
}

/**
 * Preview of the feedback to be sent.
 * @author nickkoeppen
 */
class local_fm_send_preview implements renderable {
    const PARAM_GROUP = 'groupid';

    /**
     * Feedback manager.
     * @var local_fm_manager
     */
    protected $manager;
    protected $_users;
    protected $_group; // The group selected for preview
    protected $_groups; // Groups for this fm instance
    protected $_usegroups; // Save whether or not the fm instance is using groups
    protected $format; // FORMAT_PLAIN or FORMAT_HTML

    protected $cached_users_and_fbdata; // Cached data for get_users_and_fbdata() method

    protected $qids;
    protected $assignerids;

    function __construct(local_fm_manager $manager, array $qids, array $assignerids, $format){
        $this->manager = $manager;
        $this->qids = $qids;
        $this->assignerids = $assignerids;
        $this->format = $format;
    }

    /**
     * Create the message for the selected user.
     * @return local_fm_message
     */
    function create_message($fmgroupid){
        global $USER;
        $group = $this->get_preview_group($fmgroupid);
        if (!$group) {
            return null;
        }
        $sampleuserto = reset($group->members);

        $fbdata = $this->get_fbdata($fmgroupid);
        $attempts = $this->get_attempts($fmgroupid);
        return $this->manager->create_message($USER, $sampleuserto, $attempts, $fbdata);
    }

    function has_message($fmgroupid){
        $groups = $this->get_preview_group_menu();
        return !empty($groups[$fmgroupid]);
    }

    /**
     * Calls fm instance to get group settings
     *
     * @return bool whether or not this fm instance is set to use groups
     */
    public function use_groups(){

        if(empty($this->_usegroups)){
            $config = $this->manager->get_instance_settings();
            $this->_usegroups = !empty($config->groupingid) ? true : false;
        }

        return $this->_usegroups;
    }

    /**
     * Sends request to send form that holds the fm instance
     *
     * @param int $userid
     *
     * @return int $groupid
     */
    public function get_userid_group($userid){
        return $this->manager->get_groupmanager()->get_user_group($userid);
    }

    /**
     * Gets the list of groups with feedback for the preview report
     *
     * @return array of group name indexed by group unique id
     */
    public function get_preview_group_menu(){
        if(!isset($this->_groups)){ // Build the groups if not already done
            $this->_groups = array();
            foreach($this->get_preview_users() as $id => $user){ // go through the users and get their group info
                $group = $this->get_userid_group($id);
                $groupid = $group->get_uniqueid();
                if (!empty($this->_groups[$groupid])) {
                    // already processed this group
                    continue;
                }
                $this->_groups[$groupid] = $group->get_name();
            }
        }
        return $this->_groups;
    }

    function get_preview_users(){
        if (is_null($this->_users)) {
            $this->_users = $this->manager->get_current_feedback_users($this->get_question_ids(), $this->get_assigner_ids());
        }
        return $this->_users;
    }


    /**
     * Get the group selected if there was one
     *
     * @return int
     */
    public function get_preview_group($fmgroupid){
        if (empty($fmgroupid)) {
            return false;
        } else {
            return $this->manager->get_groupmanager()->get_group($fmgroupid);
        }
    }

    public function get_question_ids() {
        return $this->qids;
    }

    public function get_assigner_ids() {
        return $this->assignerids;
    }

    public function get_format() {
        return $this->format;
    }

    protected function get_fbdata($fmgroupid) {
        $qids = $this->get_question_ids();
        $uids = $this->get_assigner_ids();
        return $this->manager->get_fbdata($qids, $uids, $fmgroupid);
    }

    protected function get_attempts($fmgroupid) {
        $qids = $this->get_question_ids();
        $uids = $this->get_assigner_ids();
        $group = $this->get_preview_group($fmgroupid);
        if (!$group) {
            return array();
        }
        $conditions = array('users'=>array_keys($group->members));
        return $this->manager->get_question_attempts($qids, $conditions);
    }
}


class local_fm_template_edit_form extends moodleform {

    function definition() {
        $mform =& $this->_form;

        $template = $this->_customdata['template'];
        $contextmenu = $this->_customdata['contextmenu'];
        $defaultcontext = $this->_customdata['defaultcontext'];

        $mform->addElement('select', 'contextid', get_string('templatescope', 'local_fm'), $contextmenu);
        $mform->addRule('contextid', null, 'required');
        $mform->addHelpButton('contextid', 'templatescope', 'local_fm');

        $options = array('size' => 60);
        $mform->addElement('text', 'subject', get_string('messagesubject', 'local_fm'), $options);
        $mform->addRule('subject', null, 'required');
        $mform->setType('subject', PARAM_NOTAGS);

        $options = array('rows' => 12, 'cols' => 60);
        $mform->addElement('editor', 'message', get_string('message', 'local_fm'), $options);
        $mform->addRule('message', null, 'required');
        $mform->setType('message', PARAM_RAW);

        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('submit', 'submitButton', get_string('settemplate', 'local_fm'), array('class'=>'form-submit'));
        $buttonarray[] = &$mform->createElement('submit', 'clear', get_string('cleartemplate', 'local_fm'));
        $buttonarray[] = &$mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');

        $template->form_set_data($mform);
        // Set context, as default should always be at activity level
        $mform->setDefault('contextid', $defaultcontext->id);

    }
}