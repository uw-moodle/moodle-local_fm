<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../config.php');
require_once($CFG->dirroot.'/local/fm/locallib.php');
require_once($CFG->dirroot.'/local/fm/message/lib.php');
require_once($CFG->dirroot.'/local/fm/message/forms.php');

$typename   = required_param('type', PARAM_ALPHANUMEXT);
$contextid  = required_param('context', PARAM_INT);
$return     = required_param('return', PARAM_URL);
$templateid = optional_param('id', '', PARAM_INT);

global $PAGE, $OUTPUT;

// Context and capabilities
$manager = local_fm_manager::get_manager($typename, $contextid);
$manager->verify_page();

$modulecontext = $manager->get_context();
require_capability('local/fm:messageconfig', $modulecontext);

// Navigation
$params = array('type' => $typename, 'context' => $contextid, 'return' => $return, 'id' => $templateid);
$baseurl = new moodle_url('/local/fm/message/template.php', $params);
$PAGE->set_url($baseurl);
$manager->set_navigation_base();
$PAGE->navbar->add(get_string('edittemplate', 'local_fm'));

if (empty($return)) {
    $return = $baseurl;
}

$allparentcontexts = $manager->get_context()->get_parent_contexts(true);

if ($templateid) {
    // Loading specific template
    $template = $manager->get_message_template($templateid);
    $templatecontext = $template->get_context();
    if (!isset($allparentcontexts[$templatecontext->id])) {
        throw new moodle_exception('templatenoaccess');
    }

    // Default to given context, if possible, otherwise module context
    if ($template::can_manage_in_context($templatecontext)) {
        $defaultcontext = $templatecontext;
    } else {
        $defaultcontext = $modulecontext;
    }
} else {
    // No template given, so fetch the existing template that we would use to send feedback.
    $template = local_fm_message_template::get_for_context($modulecontext, $manager->get_typename());
    $templatecontext = $template->get_context();
    // Default to module context
    $defaultcontext = $modulecontext;
}

// Compute context menu for templates
$contextmenu = array();
foreach($allparentcontexts as $acontext){
    if (!$template::can_manage_in_context($acontext)) {
        continue;
    }
    $contextmenu[$acontext->id] = $acontext->get_context_name();
}
$contextmenu = array_reverse($contextmenu, true);

$customdata = array('template' => $template, 'contextmenu'=>$contextmenu, 'defaultcontext'=>$defaultcontext);
$editform = new local_fm_template_edit_form($PAGE->url, $customdata);

// Process submissions
if ($editform->is_cancelled()) {
    redirect($return);
} else if ($data = $editform->get_data()) {
    if (!empty($data->clear) && !empty($data->contextid)) {
        if ($data->contextid == context_system::instance()->id) {
            // Don't delete system context, but save defaults
            $data->subject = get_string('systemplatesubject', 'local_fm');
            $data->message['text']   = get_string('systemplatemessage', 'local_fm');
            $data->message['format'] = FORMAT_HTML;
            $template->form_load_data($data);
            $template->save();
        } else {
            // clear template if it exists, and redisplay page
            $idtoclear = local_fm_message_template::context_get_template_id($data->contextid, $manager->get_typename());
            if ($idtoclear !== false) {
                $toclear = local_fm_message_template::get($idtoclear);
                $toclear->delete();
            }
        }
        redirect($baseurl);
    } else if (!empty($data->contextid)) {
        // save template and redirect
        $idtoupdate = local_fm_message_template::context_get_template_id($data->contextid, $manager->get_typename());
        if ($idtoupdate !== false) {
            $savetemplate = local_fm_message_template::get($idtoupdate);
        } else {
            $savetemplate = $manager->create_message_template($data->contextid);
        }
        $savetemplate->form_load_data($data);
        $savetemplate->save();
        redirect($return, get_string('templatesaved', 'local_fm'));
    }
}

// Display the page
$vars = $manager->get_message_variables();
$vartable = new html_table();
$vartable->attributes['class'] = 'generaltable boxaligncenter';
$vartable->head = array();
$vartable->head[] = get_string('templatecoursevar', 'local_fm').$OUTPUT->help_icon('templatecoursevar','local_fm');
$vartable->head[] = get_string('templateuservar', 'local_fm').$OUTPUT->help_icon('templateuservar','local_fm');
$vartable->head[] = get_string('templatefeedbackvar', 'local_fm').$OUTPUT->help_icon('templatefeedbackvar','local_fm');
$cvars = html_writer::alist($vars['context']);
$uvars = html_writer::alist($vars['user']);
$fvars = html_writer::alist($vars['feedback']);
$vartable->data[] = array($cvars, $uvars, $fvars);


// Setup page
$title = get_string('edittemplate', 'local_fm');
$PAGE->set_title($title);
$PAGE->set_heading($PAGE->title);

// Display page
echo $OUTPUT->header();

$titlehelp = $OUTPUT->help_icon ('edittemplate', 'local_fm');
echo $OUTPUT->heading($PAGE->heading.$titlehelp);

echo html_writer::table($vartable);

//if ($template->get_context() instanceof context_system && $template->can_manage()) {
//    echo $OUTPUT->notification('This is the site-wide template which is used by all courses!');
//}

// List all existing templates for loading
$contextmessages = array();
$editurl = new moodle_url($baseurl);
foreach($allparentcontexts as $acontext){
    $templateid = local_fm_message_template::context_get_template_id($acontext->id, $manager->get_typename());
    if ($templateid !== false) {
        $message = get_string('templateexistsfor', 'local_fm', $acontext->get_context_name());
        $editurl->param('id', $templateid);
        $contextmessages[] = html_writer::link($editurl, $message);
    }
}
if (!empty($contextmessages)) {
    $html = html_writer::start_tag('ul');
    foreach ($contextmessages as $contextmessage) {
        $html .= html_writer::tag('li', $contextmessage);
    }
    $html .= html_writer::end_tag('ul');
    echo $OUTPUT->box($html, 'boxaligncenter generalbox');
}

$editform->display();

echo $OUTPUT->footer();
