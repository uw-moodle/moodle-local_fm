<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

abstract class local_fm_base {

    static function can_manage_all(){
        return has_capability('local/fm:manage', context_system::instance());
    }

    static function can_manage_in_context(context $context){
        return has_capability('local/fm:manage', $context);
    }

    /**
     * Create an object given the DB record or object definition.
     * @param stdClass $record
     */
    static function create(stdClass $definition){
        return new static($definition);
    }

    /**
     * Get an object given the DB record id.
     * @param int $id
     * @return local_fm_base
     */
    static function get($id){
        global $DB;

        $record = $DB->get_record(static::DB_TABLE, array('id' => $id));
        if (!$record) {
            throw new moodle_exception('No record found.');
        }
        return static::create($record);
    }

    /**
     * Set page context and require login at all applicable levels.
     * @param context $context
     * @param moodle_page $page (optional) $PAGE used if not provided
     */
    static function set_page_context(context $context, moodle_page $page = null){
        global $PAGE;

        if (!isset($page)) {
            $page = $PAGE;
        }

        $courseid = ($ccontext = $context->get_course_context(false)) ? $ccontext->instanceid : null;
        $cm = ($context instanceof context_module) ? get_coursemodule_from_id(null, $context->instanceid) : null;

        require_login($courseid, false, $cm);

        if (!isset($courseid)) {
            $page->set_context($context);
        }
    }

    public $id;

    function __construct($dbrecord){
        $this->_load_data($dbrecord);
    }

    function __set($name, $value){
        // TODO: Read only?
    }

    protected function _load_data($data){
        foreach($data as $key => $value){
            $this->$key = $value;    //TODO: Specify fields?
        }
    }

    function can_manage(){
        return true;
    }

    function can_use(){
        return true;
    }

    function copy(){
        $copy = $this->get_db_record();
        unset($copy->id);
        $newcopy = static::create($copy);
        $newcopy->save();

        return $newcopy;
    }

    function delete(){
        global $DB;

        $DB->delete_records(static::DB_TABLE, array('id' => $this->id));
    }

    function exists(){
        return !empty($this->id);
    }

    function form_load_data($formdata){
        $this->_load_data($formdata);
    }

    function form_set_data(MoodleQuickForm $mform){
        $formdata = $this->get_db_record();
        $mform->setDefaults((array)$formdata);
    }

    function get_db_record(){
        $record = new stdClass();
        $record->id = $this->id;

        return $record;
    }

    protected function insert_record($record){
        global $DB;

        $this->id = $DB->insert_record(static::DB_TABLE, $record);
    }

    function save(){
        $record = $this->get_db_record();

        if ($this->exists()) {
            $this->update_record($record);
        } else {
            $this->insert_record($record);
        }
    }

    protected function update_record($record){
        global $DB;

        $DB->update_record(static::DB_TABLE, $record);
    }
}

/**
 * Objects which are assigned or defined to a specific context.
 * @author nickkoeppen
 */
abstract class local_fm_contextual extends local_fm_base {

    protected $contextid;
    protected $_context;

    function can_manage(){
        $context = $this->get_context();
        if ($context instanceof context) {
            return static::can_manage_in_context($this->get_context());
        }
        return false;
    }

    function get_context(){
        if (!isset($this->_context)) {
            $this->_context = context::instance_by_id($this->contextid, MUST_EXIST);
        }

        return $this->_context;
    }

    function get_db_record(){
        $record = parent::get_db_record();
        $record->contextid = $this->contextid;

        return $record;
    }

    function verify_page($caps = null, moodle_page $page = null){
        static::set_page_context($this->get_context(), $page);

        if (!empty($caps)) {
            $caps = (array)$caps;
            // Handle complex capabilities defined by objects
            foreach($caps as $cap){
                $method = 'can_'.$cap;
                if (!$this->$method()) {
                    throw new moodle_exception('nopermissions', '', '', $cap);
                }
            }
        }
    }
}

interface local_fm_sort_interface {
    function delete($reorder);

    function is_sorted_first();

    function is_sorted_last();

    function move_down();

    function move_up();
}

/**
 * Objects which maintain a sorting order.
 * @author nickkoeppen
 */
abstract class local_fm_sortable extends local_fm_contextual implements local_fm_sort_interface {

    protected static function _sort_params(stdClass $definition){
        return array();
    }

    static function create(stdClass $definition){
        $defintion = static::create_full_definition($definition);

        return parent::create($defintion);
    }

    protected static function create_full_definition(stdClass $definition){
        if (empty($definition->sortorder)) {
            $last = static::get_last_in_sort_context(static::_sort_params($definition));
            $definition->sortorder = $last + 1;
        }

        return $definition;
    }

    static function get_last_in_sort_context($sortcontext){
        global $DB;

        return (int)$DB->get_field(static::DB_TABLE, 'MAX(sortorder)', $sortcontext);
    }

    protected $sortorder;

    /**
     * Move a sortable object from one sorted context to another.
     * @param stdClass|array $newcontext
     */
    protected function change_sort_context($newcontext){
        $newcontext = (array)$newcontext;

        // Move to bottom of current context
        $this->change_sort_order($this->get_last_sortorder() + 1);
        // Override current context with new context definition
        foreach($this->get_sort_context() as $field => $value){
            $this->$field = $newcontext[$field];
        }
        $newlast = static::get_last_in_sort_context($newcontext);
        $this->sortorder = $newlast + 1;

        $this->save();
    }

    protected function change_sort_order($newpos){
        /* Determine environment */
        $oldpos = $this->sortorder;

        /* Ensure it is a valid change in sortorder */
        if ($newpos == $oldpos) {
            return;    //No change in order
        }
        $records = $this->get_sort_group_records();
        $numingroup = count($records);
        if ($numingroup < 2) {
            return;    //One option in group by itself
        }

        /* Organize in new display order */
        if ($newpos < $oldpos) {
            //If moving up leave all below the same
            for($pos = $oldpos; $pos >= $newpos; $pos--){
                if (isset($records[$pos])) {
                    $records[$pos]->sortorder++;
                }
            }
        } else {
            //If moving down leave all above the same
            for($pos = $oldpos; $pos < $newpos; $pos++){
                if (isset($records[$pos])) {
                    $records[$pos]->sortorder--;
                }
            }
            $newpos--;    //Moving below actually inherits sortorder of current occupant
        }

        /* Change options values to new surroundings */
        if ($newpos > $numingroup) {
            $newpos = $numingroup;
        } else if ($newpos < 1) {
            $newpos = 1;
        }
        $this->sortorder = $newpos;
        $records[$oldpos] = $this->get_db_record();

        /* Update ordering */
        foreach($records as $record){
            $this->update_record($record);
        }
    }

    function copy(){
        global $DB;
        // Create copy at bottom of sort order
        $copy = $this->get_db_record();
        unset ($copy->id);
        // TODO Do something more intelligent than simply appending 'copy'
        $copy->name = get_string('nameofcopieditem', 'local_fm', $copy->name);
        $copy->sortorder = $this->get_last_sortorder() + 1;
        $newcopy = static::create($copy);
        $newcopy->save();

        // Move copy to below current
        $newcopy->change_sort_order($this->sortorder + 1);

        return $newcopy->id;
    }

    function delete($reorder = true){
        if ($reorder) {    // Move to bottom to maintain ordering
            $this->change_sort_order($this->get_last_sortorder() + 1);
        }

        parent::delete();
    }

    function get_db_record(){
        $record = parent::get_db_record();
        $record->sortorder = $this->sortorder;

        return $record;
    }

    /**
     * Get the last sortorder in this sort context.
     * @return number
     */
    function get_last_sortorder(){
        return static::get_last_in_sort_context($this->get_sort_context());
    }

    /**
     * Get the conditions that define the context for sorting.
     * @return array
     */
    function get_sort_context(){
        return static::_sort_params($this->get_db_record());
    }

    /**
     * Return all DB records for the objects in this sort context (including this one).
     * @return array
     */
    protected function get_sort_group_records(){
        global $DB;

        $records = array();
        $rs = $DB->get_recordset(static::DB_TABLE, $this->get_sort_context(), 'sortorder');
        foreach($rs as $record){
            $records[$record->sortorder] = $record;
        }

        return $records;
    }

    function get_sort_order(){
        return $this->sortorder;
    }

    function is_sorted_first(){
        return $this->sortorder == 1;
    }

    function is_sorted_last(){
        return $this->sortorder == $this->get_last_sortorder();
    }

    function move_down(){
        $this->change_sort_order($this->sortorder + 2);
    }

    function move_up(){
        $this->change_sort_order($this->sortorder - 1);
    }

}

class local_fm_invalid_input_exception extends moodle_exception {
    function __construct($debuginfo=null) {
        parent::__construct('invalidinput', 'local_fm', '', null, $debuginfo);
    }
}
?>