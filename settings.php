<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $modname = 'local_fm';

    $manageurl = new moodle_url('/local/fm/bank/edit.php');

    $category = new admin_category('sitefeedback', get_string('feedback'));
    $url = clone($manageurl);
    $url->param('context', context_system::instance()->id);
    $category->add('sitefeedback', new admin_externalpage($modname, get_string('feedbackbank', 'local_fm'), $url));
    $ADMIN->add('localplugins', $category);
}