<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/bank/lib.php');

class local_fm_feedback_instance extends local_fm_sortable {
    const DB_TABLE = 'local_fm_feedback_instance';

    protected static function _sort_params(stdClass $definition){
        return array(
            'contextid'  => $definition->contextid,
            'questionid' => $definition->questionid,
        );
    }

    static function get_all_for_feedback($feedbackid){
        global $DB;

        $feedback = array();
        $records = $DB->get_records(self::DB_TABLE, array('feedbackid' => $feedbackid));
        foreach($records as $id => $record){
            $feedback[$id] = static::create($record);
        }

        return $feedback;
    }

    /**
     * Get feedback instances for a given context or, if provided, only those linked to a specific
     * question instance in that context.
     * @param int $contextid  context id
     * @param int $questionid (optional) question id
     * @param bool $sent (optional) whether the feedback has been sent
     * @return local_fm_feedback_instance[]
     */
    static function get_instances($contextid, $questionid = null, $sent = false){
        global $DB;

        $params = array('cid' => $contextid);
        if (!empty($questionid)) {
            $questionsql = "questionid = :qid";
            $params['qid'] = $questionid;
        } else {
            $questionsql = "questionid IS NULL";
        }
        $sentsql = '';
        if ($sent) {
            $sentsql .= "AND sent IS NOT NULL";
        }
        $where = "contextid = :cid AND ($questionsql) $sentsql";
        $sort = 'questionid DESC, sortorder ASC, sent ASC';
        $records = $DB->get_records_select(self::DB_TABLE, $where, $params, $sort);

        $objects = array();
        foreach($records as $id => $record){
            $objects[$id] = static::create($record);
        }

        return $objects;
    }

    static function is_feedback_used_in_context(local_fm_feedback $feedback, context $context){
        global $DB;

        $conditions = array('feedbackid' => $feedback->id, 'contextid' => $context->id);
        return $DB->record_exists(self::DB_TABLE, $conditions);
    }

    public $feedbackid;
    /**
     * Feedback object
     * @var local_fm_feedback
     */
    protected $_feedback;
    public $questionid;
    /**
     * Question definition
     * @var question_definition
     */
    protected $_question;
    protected $sent;
    protected $name;
    protected $text;

    function delete($reorder = true){
        // Remove all assignments linked to this feedback instance
        foreach($this->get_assignments() as $assign){
            $assign->delete();
        }

        parent::delete($reorder);
    }

    function display(){
        $commands = implode(' ', $this->display_commands());
        $name = $this->get_name();
        $text = $this->get_text();
        $class = $this->was_sent() ? 'fbi_sent' : 'fbi_editable';

        $title = html_writer::tag('span', "$commands <b>$name</b>", array('class' => 'fbtitle'));
        return html_writer::tag('span', "$title $text", array('class' => $class));
    }

    function get_spacer() {
        global $OUTPUT;
        return '<img src="' . $OUTPUT->pix_url('spacer') . '" class="smallicon" alt="" /> ';
    }

    protected function display_commands(){
        global $PAGE, $OUTPUT;

        $url = $PAGE->url;
        $url->params(array('fbiid'=>$this->id, 'sesskey'=>sesskey()));

        $savescroll = new component_action('click', 'M.core_scroll_manager.save_scroll_action');

        $commands = array();

        if (!$this->was_sent()) {
            $url->param('action', 'delete');
            if ($this->in_question_context()){
                $deletestring = get_string('delete');
            } else {
                $deletestring = get_string('removefromquestion', 'local_fm');
            }
            $icon = new pix_icon('t/delete', $deletestring);
            $commands[] = $OUTPUT->action_icon($url, $icon, $savescroll);

            $feedback = $this->get_feedback();
            if ($feedback && $feedback->can_manage()) {
                $params = array('id' => $feedback->id, 'action' => 'edit', 'return' => $PAGE->url);
                $editurl = new moodle_url('/local/fm/bank/feedback.php', $params);
                $commands[] = $OUTPUT->action_icon($editurl, new pix_icon('t/edit', get_string('editfeedback', 'local_fm')));
            } else {
                $commands[] = $this->get_spacer();
            }
        } else {
            $commands[] = $this->get_spacer();
            $commands[] = $this->get_spacer();
        }

        //$noscripts = array();
        if (!$this->is_sorted_first()) {
            $url->param('action', 'moveup');
            $icon = new pix_icon('t/up', get_string('moveup'));
            $commands[] = $OUTPUT->action_icon($url, $icon, $savescroll);
        } else {
            $commands[] = $this->get_spacer();
        }
        if (!$this->is_sorted_last()) {
            $url->param('action', 'movedown');
            $icon = new pix_icon('t/down', get_string('movedown'));
            $commands[] = $OUTPUT->action_icon($url, $icon, $savescroll);
        } else {
            $commands[] = $this->get_spacer();
        }
        //$commands[] = html_writer::tag('noscript', implode(' ', $noscripts));

        return $commands;
    }

    function get_assignments(){
        global $CFG;

        // Don't want to have this library loaded unless absolutely necessary
        require_once($CFG->dirroot.'/local/fm/assign/lib.php');
        return local_fm_feedback_assign::get_all_for_feedback_instance($this->id);
    }

    function get_db_record(){
        $record = parent::get_db_record();
        $record->feedbackid = $this->feedbackid;
        $record->questionid = $this->questionid;
        $record->sent       = $this->sent;
        $record->name       = $this->name;
        $record->text       = $this->text;

        return $record;
    }

    /**
     * Get the feedback used.
     * @return local_fm_feedback
     */
    function get_feedback(){
        if (!isset($this->_feedback)) {
            $this->_feedback = local_fm_feedback::get($this->feedbackid);
        }

        return $this->_feedback;
    }

    function get_name(){
        if ($this->name) {
            $name = $this->name;
        } else if ($this->has_feedback()) {
            $name = $this->get_feedback()->get_name();
        } else {
            $name = '';
        }

        return $name;
    }

    function get_question(){
        if (!isset($this->_question)) {
            $this->_question = question_bank::load_question($this->questionid);
        }
        return $this->questionid;
    }

    function get_text(){
        if ($this->text) {
            $text = $this->text;
        } else if ($this->has_feedback()) {
            $text = $this->get_feedback()->get_text();
        } else {
            $text = '';
        }

        return $text;
    }

    function has_feedback(){
        $feedback = $this->get_feedback();
        return !empty($feedback);
    }

    function in_question_context(){
        return !empty($this->questionid);
    }

    function mark_sent(){
        if (!isset($this->name)) {
            $this->name = $this->get_name();
        }
        // Lock actual text that was sent
        $this->text = $this->get_text();
        $this->sent = time();
        $this->save();
    }

    function was_sent(){
        return !empty($this->sent);
    }
}

abstract class local_fm_manager {

    static function can_manage_site(){
        return has_capability('local/fm:manage', context_system::instance());
    }

    static function context_has_questions($context){
        return ($context instanceof context_module || $context instanceof context_block);
    }

    static function page_context_check($context) {
        global $PAGE;

        $courseid = ($ccontext = $context->get_course_context(false)) ? $ccontext->instanceid : null;
        $cm = ($context instanceof context_module) ? get_coursemodule_from_id(null, $context->instanceid) : null;

        require_login($courseid, false, $cm);
        if (!isset($courseid)) {
            $PAGE->set_context($context);
        }
    }

    static function register_manager_type($typename, $include, $classname){
        global $GLOBALS;

        $GLOBALS['FEEDBACK_MANAGER_TYPES'][$typename] = array(
                'include'   => $include,
                'classname' => $classname,
        );
    }

    /**
     * Get a specific manager type that has been registered using
     * @see local_fm_manager::register_manager_type.
     *
     * @param string $typename
     * @param context|int $contextorid
     * @param int $questionid
     * @return local_fm_manager
     */
    static function get_manager($typename, $contextorid, $questionid = null){
        global $CFG, $GLOBALS;

        if (empty($GLOBALS['FEEDBACK_MANAGER_TYPES'][$typename])) {
            throw new Exception('typenotdefined');
        }
        $typedef = $GLOBALS['FEEDBACK_MANAGER_TYPES'][$typename];

        require_once($CFG->dirroot.$typedef['include']);

        $classname = $typedef['classname'];
        return new $classname($contextorid, $questionid);
    }

    protected $context;
    protected $_qubas;
    protected $_qattempts;

    protected $questionid;
    protected $groupingid;
    protected $_question;

    protected $_assignments;
    protected $_feedbackmap;
    protected $_fbinstances;
    protected $_fbmenu;

    protected $usermanager;
    protected $groupmanager;

    /**
     * Feedback report
     * @var local_fm_report
     */
    protected $_report;
    /**
     * Feedback message template
     * @var local_fm_message_template
     */
    protected $_template;

    /**
     * Create a feedback manager in a given context.
     * @param context|int $contextorid
     * @param int $questionid (optional) question id
     */
    function __construct($contextorid, $questionid = null){
        if ($contextorid instanceof context) {
            $this->context = $contextorid;
        } else if (is_numeric($contextorid)) {
            $this->context = context::instance_by_id($contextorid);
        } else {
            throw new Exception('invalid context supplied');
        }

        $this->questionid = $questionid;
    }

    function get_usermanager(){
        if (!$this->usermanager) {
            // Query our fields plus whatever user_picture requires
            $fields = user_picture::fields('', array('id', 'firstname', 'lastname'));
            $this->usermanager = new local_fm_user_manager($fields);
        }
        return $this->usermanager;
    }

    function get_groupmanager(){
        if (!$this->groupmanager) {
            $this->groupmanager = new local_fm_group_manager($this->get_groupingid(), $this->get_course()->id, $this->get_usermanager());
        }
        return $this->groupmanager;
    }

    function add_feedback_instance($feedbackid, $questionid = true){
        if ($questionid === true) {
            $questionid = $this->get_question()->id;
        }

        // Handle duplicate feedback names
        $feedback = local_fm_feedback::get($feedbackid);
        $fbname = $feedback->get_name();

        $highest = 0;
        foreach($this->get_feedback_instances($questionid) as $id => $instance){
            $matches = array();
            if (preg_match("/".preg_quote($fbname)."$|".preg_quote($fbname)."\(?P:<number>\d+\)$/", $instance->get_name(), $matches)) {
                // Need complicated number search to handle missing numbers
                if (!empty($matches['number'])) {
                    $number = $matches['number'];
                } else {
                    $number = 1;
                }
                if ($highest < $number) {
                    $highest = $number;
                }
            }
        }

        $newdef = new stdClass();
        $newdef->feedbackid = $feedbackid;
        $newdef->contextid  = $this->context->id;
        $newdef->questionid = $questionid;
        $newdef->sent       = null;
        if ($highest > 0) {
            $highest++;
            $newdef->name   = "$fbname($highest)";
        }
        $instance = local_fm_feedback_instance::create($newdef);
        $instance->save();

        $this->_fbinstances[$questionid][$instance->id] = $instance;

        return $instance;
    }

    /**
     * Assign feedback to a specific question attempt.
     * @param question_attempt $questionattempt
     * @param int $fbinstanceid
     */
    function assign_feedback(question_attempt $questionattempt, $fbinstanceid){
        $fbinstances = $this->get_feedback_instances(true);
        if (!isset($fbinstances[$fbinstanceid])) {
            throw new moodle_exception('invalidfbinstance');
        }
        $assignments = $this->get_attempt_assignments($questionattempt);
        $assignments->add_feedback_assign($fbinstanceid);
    }

    /**
     * Assign custom feedback to a specific question attempt.
     * @param question_attempt $questionattempt
     * @param string $customtext
     */
    function assign_feedback_custom(question_attempt $questionattempt, $customtext){
        $assignments = $this->get_attempt_assignments($questionattempt);
        if (empty($customtext) || ctype_space($customtext)) {
            $assignments->delete_feedback_custom();
        } else {
            $assignments->add_feedback_custom($customtext);
        }
    }

    /**
     * Assign grade feedback to a specific question attempt.
     * @param question_attempt $questionattempt
     */
    function assign_feedback_grade(question_attempt $questionattempt){
        $assignments = $this->get_attempt_assignments($questionattempt);
        $assignments->add_feedback_grade();
    }

    function can_assign(){
        return has_capability('local/fm:assignfeedback', $this->context);
    }

    function can_manage_all(){
        return has_capability('local/fm:manage', $this->context);
    }

    function can_manage_feedback(){
        global $USER;

        if ($this->can_manage_all()) {
            return true;
        }

        if ($this->context instanceof context_user && $this->context->instanceid == $USER->id) {
            return has_capability('local/fm:manageownfeedback', context_system::instance());
        }

        return has_capability('local/fm:managefeedback', $this->context);
    }

    function get_groupingid() {
        return null;
    }

    function has_grouping() {
        return !is_null($this->get_groupingid());
    }

    /**
     * Create a message given the user sending the message (userfrom), the user being sent
     * the message (userto), and a subset of assigned feedback in an array grouped by
     * question attempt id.
     * @param stdClass|int $userfrom
     * @param stdClass|int $userto
     * @param array $attempts
     * @param array $fbdata    Array of local_fm_assigned_feedback indexed by qaid
     * @return local_fm_message
     */
    function create_message($userfrom, $userto, $attempts, $fbdata){
        $template = $this->get_message_template();

        $record = new stdClass();
        // Preload (if provided) to avoid extra DB calls
        if (is_object($userfrom)) {
            $record->_userfrom  = $userfrom;
            $record->useridfrom = $userfrom->id;
        } else {
            $record->useridfrom = $userfrom;
        }
        if (is_object($userto)) {
            $record->_userto  = $userto;
            $record->useridto = $userto->id;
        } else {
            $record->useridto = $userto;
        }

        // Plain text
        $messdata = $this->get_message_data($userto, $userfrom, $attempts, $fbdata, FORMAT_PLAIN);
        $record->contextid     = $this->get_context()->id;
        $record->subject       = $template->create_subject($messdata);
        $record->message       = $template->create_message($messdata, FORMAT_PLAIN);
        $record->messageformat = $template->get_message_format();
        // HTML
        $messdata = $this->get_message_data($userto, $userfrom, $attempts, $fbdata, FORMAT_HTML);
        $record->messagehtml   = $template->create_message($messdata, FORMAT_HTML);

        return local_fm_message::create($record);
    }

    function create_message_template($contextid){
        $def = new stdClass();
        $def->contextid = $contextid;
        $def->type      = $this->get_typename();
        return new local_fm_message_template($def);
    }

    function delete_feedback_instance(local_fm_feedback_instance $instance){
        foreach($this->get_assignments() as $attemptid => $assignments){
            if (!$assignments->has_feedback_instance($instance->id)) {
                continue;
            }
            $assignments->delete_feedback_instance_assigns($instance->id);
        }
        $instance->delete();
        unset($this->_fbinstances[$instance->questionid][$instance->id]);
    }

    /**
     * Get all feedback that is instantiated in this managing context.
     * @return local_fm_feedback[]
     */
    function get_all_feedback(){
        $feedback = array();
        foreach($this->get_feedback_instances() as $instance){
            $fbobj = $instance->get_feedback();
            $feedback[$fbobj->id] = $fbobj;
        }

        return $feedback;
    }

    function get_assigners(){
        global $DB;

        $qubaids = $this->get_qubaid_condition();
        $sql = "SELECT DISTINCT COALESCE(fa.userid, fc.userid, fg.userid)
                    FROM {$qubaids->from_question_attempts('qa')}
                    LEFT JOIN {local_fm_feedback_assign} fa ON fa.questionattemptid = qa.id
                    LEFT JOIN {local_fm_feedback_custom} fc ON fc.questionattemptid = qa.id
                    LEFT JOIN {local_fm_feedback_grade} fg ON fg.questionattemptid = qa.id
                    WHERE fa.id IS NOT NULL OR fc.id IS NOT NULL OR fg.id IS NOT NULL
                       AND {$qubaids->where()}
                ";
        $params = $qubaids->from_where_params();
        $userids = $DB->get_records_sql($sql, $params);
        return $this->get_usermanager()->get_all_users(array_keys($userids));
    }

    function preload_assignment_users($assignments) {
        // Preload all users in attempts
        $userids = array();
        foreach ($assignments as $assignment) {
            $userids[] = $assignment->get_attempt_userid();
        }
        $this->get_usermanager()->preload_users($userids);

    }

    /**
     * Get all feedback assignments for each question attempt.
     * @param array|int|true $questionids
     * @param array|null $conditions passed to self::get_qubaid_condition()
     * @return local_fm_question_attempt_feedback
     */
    function get_assignments($questionids = null, $conditions = null){
        global $CFG;

        // Don't want to have this library loaded unless absolutely necessary
        require_once($CFG->dirroot.'/local/fm/assign/lib.php');

        if (!isset($this->_assignments)) {
            $this->_assignments = array();
        }

        $hash = sha1(serialize($conditions));

        $allfromcache = true;
        $assignments = array();
        foreach($this->get_question_attempts($questionids, $conditions) as $questionid => $qattempts){
            if (!isset($this->_assignments[$hash][$questionid])) {
                $this->_assignments[$hash][$questionid] = array();
                // Preload assignment data
                $qaids = array_keys($qattempts);
                $fbinstances = $this->get_feedback_instances($questionid);
                local_fm_question_attempt_feedback::preload_assigned_feedback($qaids, $fbinstances);
                // Create attempt assignment objects
                foreach($qattempts as $qaid => $attempt){
                    $attemptfeedback = new local_fm_question_attempt_feedback($attempt);
                    $this->_assignments[$hash][$questionid][$qaid] = $attemptfeedback;
                }
                $allfromcache = false;
            }
            $assignments += $this->_assignments[$hash][$questionid];
        }
        // Preload all assignment users
        if (!$allfromcache) {
            $this->preload_assignment_users($assignments);
        }

        return $assignments;
    }

    /**
     * Get assignments for a specific question attempt.
     * @param question_attempt $attempt
     * @param array|null $conditions passed to self::get_qubaid_condition()
     * @return local_fm_question_attempt_feedback
     */
    function get_attempt_assignments(question_attempt $attempt, $conditions = null){
        $allassigns = $this->get_assignments($attempt->get_question()->id, $conditions);
        return $allassigns[$attempt->get_database_id()];
    }

    /**
     * Get all fmgroups that have made attempts in this managed context.
     * @return array
     */
    abstract function get_attempt_groups();

    /**
     * Get feedback manager context.
     * @return context
     */
    function get_context(){
        return $this->context;
    }

    function get_course(){
        global $DB;

        if (!isset($this->_course)) {
            $coursecontext = $this->context->get_course_context(false);
            if (!$coursecontext) {
                $course = false;
            } else {
                $course = $DB->get_record('course', array('id' => $coursecontext->instanceid));
            }
            $this->_course = $course;
        }

        return $this->_course;
    }

    function get_default_category(context $context = null) {
        if (!$context) {
            $context = $this->get_context();
        }
        return local_fm_category::get_default_category($context);
    }

    /**
     * Get feedback mapping connecting instance to definition for this manager's instances.
     * @return local_fm_feedback[]
     */
    function get_feedback_map(){
        if (!isset($this->_feedbackmap)) {
            $this->_feedbackmap = array();
            foreach($this->get_feedback_instances() as $fbinstance){
                $this->_feedbackmap[$fbinstance->id] = $fbinstance->get_feedback()->id;
            }
        }

        return $this->_feedbackmap;
    }

    function get_feedback_menu(){
        if (!isset($this->_fbmenu)) {
            $this->_fbmenu = array(
                'context'  => array(),
                'question' => array(),
            );

            foreach($this->get_feedback_instances(true) as $id => $instance){
                $fbname = $instance->get_name();
                if ($instance->in_question_context()) {
                    $this->_fbmenu['question'][$instance->id] = $fbname;
                } else {
                    $this->_fbmenu['context'][$instance->id] = $fbname;
                }
            }
        }

        return $this->_fbmenu;
    }

    /**
     * Get feedback instances available to this manager
     * @param mixed $questionids
     * @return local_fm_feedback_instance[]
     */
    function get_feedback_instances($questionids = null){
        if (!isset($this->_fbinstances)) {
            $this->_fbinstances = array(
                null => local_fm_feedback_instance::get_instances($this->get_context()->id),
            );
        }
        $instances = $this->_fbinstances[null];    //Always include context instances

        $questionids = $this->parse_questionids($questionids);
        foreach($questionids as $questionid){
            if (!isset($this->_fbinstances[$questionid])) {
                $fbinsts = local_fm_feedback_instance::get_instances($this->get_context()->id, $questionid);
                $this->_fbinstances[$questionid] = $fbinsts;
            }
            $instances += $this->_fbinstances[$questionid];
        }

        return $instances;
    }

    function get_instance_of_feedback($feedbackid, $questionid = null, $sent = false) {
        if ($questionid === true) {
            $questionid = $this->get_question()->id;
        }

        foreach($this->get_feedback_instances($questionid) as $instance){
            $wassent = $instance->was_sent();
            if ($sent ? !$wassent : $wassent){
                continue;
            }

            if ($instance->feedbackid == $feedbackid && $instance->questionid == $questionid){
                return $instance;
            }
        }

        return false;
    }

    /**
     * Retrieve all message data required for message creation. The user to and from must
     * be supplied with a subset of assigned feedback in an array grouped by question attempt id.
     * @param stdClass $userto
     * @param stdClass $userfrom
     * @param array $attempts
     * @param array $fbdata    Array of local_fm_assigned_feedback indexed by qaid
     * @param int|null $format [FORMAT_MOODLE, FORMAT_HTML, FORMAT_PLAIN, FORMAT_MARKDOWN]
     * @return array
     */
    function get_message_data($userto, $userfrom, $attempts, $fbdata, $format = null){
        $data = array(
            '$usertofirstname'   => $userto->firstname,
            '$usertolastname'    => $userto->lastname,
            '$userfromfirstname' => $userfrom->firstname,
            '$userfromlastname'  => $userfrom->lastname,
        );

        $course = $this->get_course();
        if ($course) {
            $data['$courseid']        = $course->id;
            $data['$courseshortname'] = $course->shortname;
            $data['$coursefullname']  = $course->fullname;
        }

        $url = new moodle_url('/course/view.php', array('id' => $course->id));
        if ($format == FORMAT_HTML) {
            $data['$courseurl'] = html_writer::link($url, $url);
        } else {
            $data['$courseurl'] = $url->out(false);
        }

        $textindent = '   ';
        $formatoptions = array('context' => $this->get_context());

        $pos = 1;
        $allfeedback = array();
        // TODO: Make this more efficient
        foreach($attempts as $questionid => $qattempts){
            foreach($qattempts as $qaid => $attempt) {
                $qfeedback = array();
                if (isset($fbdata[$qaid])) {
                    foreach($fbdata[$qaid] as $feedback){
                        $qfeedback[] = $feedback->get_text();
                    }
                }

                $allfbinfo = '';

                // Question
                $qobj = $attempt->get_question();
                $data['$questionname'.$pos] = $qobj->name;
                $dataname = '$questiontext'.$pos;
                $qhtml = question_rewrite_question_urls($qobj->questiontext, 'pluginfile.php',
                        $this->get_context()->id, 'question', 'questiontext_preview', array('question'), $qobj->id);
                $qhtml = format_text($qhtml, $qobj->questiontextformat, $formatoptions);

                if ($format == FORMAT_HTML) {
                    //html
                    $data['$questiontext'.$pos] = $qhtml;
                    //all html
                    $allfbinfo .= html_writer::tag('h1', get_string('question') . " $pos", array('style'=>'margin-top:2em;'));
                    $allfbinfo .= html_writer::tag('div', $qhtml, array('style'=>'margin:1em 0 1em 2em;'));
                } else {
                    //text
                    $data['$questiontext'.$pos] = html_to_text($qhtml);
                    //all text
                    $allfbinfo .= html_to_text(get_string('question') . " $pos: $qhtml");
                }

                // Response
                $step = $attempt->get_last_step_with_qt_var('answer');
                $text   = $step->get_qt_var('answer');
                $rformat = $step->get_qt_var('answerformat');
                $rhtml = format_text($text, $rformat, $formatoptions);
                $rhtml = $attempt->get_response_summary();
                $rhtml = format_text($attempt->get_response_summary(), FORMAT_PLAIN, $formatoptions);

                if ($format == FORMAT_HTML) {
                    //html
                    $data['$response'.$pos] = $rhtml;
                    //all html
                    $allfbinfo .= html_writer::start_tag('div', array('style'=>'margin-left: 3em;'));
                    $allfbinfo .= html_writer::tag('strong', get_string('response', 'mod_quiz').': ');
                    $allfbinfo .= html_writer::tag('div', $rhtml, array('style'=>'margin:1em 0 1em 2em;'));
                    $allfbinfo .= html_writer::end_tag('div');
                } else {
                    //text
                    $data['$response'.$pos] = html_to_text($rhtml);
                    //all text
                    $allfbinfo .= "\n\n".html_to_text(get_string('response', 'mod_quiz') . ": $rhtml");
                }

                // Feedback
                if ($format == FORMAT_HTML) {
                    //html
                    $fhtml = html_writer::alist($qfeedback, array('style'=>'list-style-type:none;margin:1em 0 1em 2em;background: none repeat scroll 0 0 #FFF3BF;'));
                    $data['$feedback'.$pos] = $fhtml;
                    //all html
                    $allfbinfo .= html_writer::start_tag('div', array('style'=>'margin-left: 3em;'));
                    $allfbinfo .= html_writer::tag('strong', get_string('feedback') . ": ") . $fhtml;
                    $allfbinfo .= html_writer::end_tag('div');
                } else {
                    $fplain = '';
                    foreach ($qfeedback as $qfb) {
                        $fplain .= "* ".html_to_text($qfb).PHP_EOL;
                    }
                    //text
                    $data['$feedback'.$pos] = $fplain;
                    //all text
                    $allfbinfo .= PHP_EOL.PHP_EOL.get_string('feedback').":".PHP_EOL.PHP_EOL.$fplain;
                }

                // Grades
                $data['$mark'.$pos] = $attempt->format_mark(2);
                $data['$maxmark'.$pos] = $attempt->format_max_mark(2);

                $displaymark = "(" . $data['$mark'.$pos] . " / " . $data['$maxmark'.$pos] . ")";

                if ($format == FORMAT_HTML) {
                    //all html
                    $allfbinfo .= html_writer::start_tag('div', array('style'=>'margin-left: 3em;'));
                    $allfbinfo .= html_writer::tag('strong', get_string('grade').":") . $displaymark;
                    $allfbinfo .= html_writer::end_tag('div');
                } else {
                    //all text
                    $allfbinfo .= PHP_EOL.get_string('grade').": $displaymark";
                }

                $data['$questionresponsefeedback'.$pos] = $allfbinfo;

                $allquestions[] = $allfbinfo;

                if (!empty($qfeedback)) {
                    $allfeedback[] = $allfbinfo;
                }
            }

            $pos++;
        }

        $data['$questionresponsefeedback'] = implode(PHP_EOL.PHP_EOL, $allfeedback);
        $data['$allquestionresponsefeedback'] = implode(PHP_EOL.PHP_EOL, $allquestions);


        return $data;
    }

    /**
     * Get the message template configured for this context. If the optional id parameter if not
     * provided, the template defined at the nearest context to this managing context will be retrieved.
     * @param $id (optional) A specific message template to retrieve
     * @return local_fm_message_template
     */
    function get_message_template($id = null){
        global $CFG;

        require_once($CFG->dirroot.'/local/fm/message/lib.php');

        if (!empty($id)) {
            return local_fm_message_template::get($id);
        }

        if (!isset($this->_template)) {
            $context = $this->get_context();
            $type    = $this->get_typename();
            $this->_template = local_fm_message_template::get_for_context($context, $type);
        }

        return $this->_template;
    }

    /**
     * Get all message variable options.
     * @return multitype:multitype:string
     */
    function get_message_variables(){
        $vars = array();

        $qnameattr = array('title' => '');
        $qtextattr = array('title' => '');
        $num = 1;
        foreach($this->get_questions() as $question){
            $qnameattr['title'] .= "$num. $question->name".PHP_EOL;
            // As in question_definition::get_question_summary()
            $question_summary = question_utils::to_plain_text($question->questiontext, $question->questiontextformat);
            $qtextattr['title'] .= "$num. ".$question_summary.PHP_EOL;
            $num++;
        }

        $questionnames = html_writer::tag('span', '$questionnameX', $qnameattr);
        $questiontexts = html_writer::tag('span', '$questiontextX', $qtextattr);

        $vars['feedback'] = array(
                '$questionresponsefeedback',
                '$allquestionresponsefeedback',
                '$questionresponsefeedbackX',
                $questionnames,
                $questiontexts,
                '$responseX',
                '$feedbackX',
                '$markX',
                '$maxmarkX',
        );

        $vars['context'] = array(
                '$courseshortname',
                '$coursefullname',
                '$courseid',
                '$courseurl',
        );
        $vars['user'] = array(
                '$usertofirstname (i.e. student name)',
                '$usertolastname',
                '$userfromfirstname (i.e. instructor name)',
                '$userfromlastname'
        );

        return $vars;
    }

    function get_navigation_baseurl(){
        $navparams = array('context' => $this->get_context()->id);
        return new moodle_url('/local/fm/bank/edit.php', $navparams);
    }

    function get_page_renderer(moodle_page $page){
        global $CFG;

        require_once($CFG->dirroot.'/local/fm/renderer.php');

        return $page->get_renderer('local_fm');
    }

    /**
     * Get current question (or first if not defined).
     * @return question_definition
     */
    function get_question(){
        if (!isset($this->_question)) {
            $questions = $this->get_questions();
            if (!empty($this->questionid)) {
                $question = $questions[$this->questionid];
            } else {
                $question = reset($questions);
            }
            if (!$question) {
                throw new moodle_exception('No valid question.');
            }
            $this->_question = $question;
        }

        return $this->_question;
    }

    /**
     * Get question attempts in this context with optional restriction to specified questions.
     * @param array|int|true $questionids
     * @param array|null $conditions passed to self::get_qubaid_condition()
     * @return array:array:question_attempt
     */
    function get_question_attempts($questionids = null, $conditions = null){
        global $DB;
        $questionids = $this->parse_questionids($questionids);
        $hash = sha1(serialize($conditions));
        if (!$conditions) {
            $conditions = array();
        }
        $attempts = array();
        $dmready = false;
        foreach($questionids as $questionid) {
            if (!isset($this->_qattempts[$hash][$questionid])) {
                if (!$dmready) {
                    $dm = new question_engine_data_mapper();
                    $qubaids = $this->get_qubaid_condition($conditions);
                    $dmready = true;
                }
                $this->_qattempts[$hash][$questionid] = $dm->load_attempts_at_question($questionid, $qubaids);
            }
            $attempts[$questionid] = $this->_qattempts[$hash][$questionid];
        }

        return $attempts;
    }

    /**
     * Get question usage by activity given a question usage id.
     * @param int $usageid
     */
    function get_question_usage($usageid){
        if (!isset($this->_qubas[$usageid])) {
            $this->_qubas[$usageid] = question_engine::load_questions_usage_by_activity($usageid);
        }

        return $this->_qubas[$usageid];
    }

    protected function get_question_usage_component(){
        return '';
    }

    function get_question_usages(){
        global $DB;

        $usages = array();
        $params = array(
            'contextid' => $this->context->id,
            'component' => $this->get_question_usage_component(),
        );
        $qurecords = $DB->get_records('question_usages', $params);
        foreach($qurecords as $qurecord){
            $usages[$qurecord->id] = $this->get_question_usage($qurecord->id);
        }

        return $usages;
    }

    /**
     * Get question definitions used in this context.
     * @return question_definition[]
     */
    abstract function get_questions();

    /**
     * Get the question usage by activity id condition that determines which question usages
     * apply to this managing context.
     *
     * Possible conditions:
     *    $conditions['users'] = array of userids to fetch attempts for
     *    $conditions['attempts'] = array of specific attemptids to fetch
     *
     * @param array|null conditions
     * @return qubaid_condition
     */
    abstract protected function get_qubaid_condition($conditions);

    /**
     * Get the feedback report for this context.
     * @return local_fm_report
     */
    function get_report(){
        global $CFG;

        require_once($CFG->dirroot.'/local/fm/report/lib.php');

        if (!isset($this->_report)) {
            $this->_report = local_fm_report::get_for_context($this->get_context());
        }

        return $this->_report;
    }

    abstract function get_typename();

    function has_feedback_instance($instanceid){
        $instances = $this->get_feedback_instances();
        return !empty($instances[$instanceid]);
    }

    public function get_user($userid) {
        return $this->get_usermanager()->get_user($userid);
    }

    public function get_all_users(array $userids) {
        return $this->get_usermanager()->get_all_users($userids);
    }


    function has_instance_of_feedback($feedbackid, $questionid = null, $sent = false){
        $instance = $this->get_instance_of_feedback($feedbackid, $questionid, $sent);

        return !empty($instance);
    }

    /**
     * Change mixed question id input into an array of questionids. This aids other functions
     * in processing complex investigations at a subset of questions in the context.
     *
     * - NULL:  All question ids
     * - FALSE: No question ids
     * - TRUE:  Only the question from get_question()
     * - INT:   Adds array wrapper to single question id given
     * - ARRAY: NO CHANGE MADE
     *
     * @param mixed $questionids
     * @return multitype:int
     */
    protected function parse_questionids($questionids){
        if (isset($questionids)) {
            if (is_array($questionids)) {
                $qids = $questionids;
            } else {
                $qids = array();
                if ($questionids === true) {
                    $qids[] = $this->get_question()->id;
                } else if ($questionids !== false) {
                    $qids[] = $questionids;
                }
            }
        } else {
            $qids = array_keys($this->get_questions());
        }

        return $qids;
    }

    /**
     * Get all users with unsent feedback assigned by given users
     * @param array $questionids questionids
     * @param array $assignerids assigners
     * @return multitype:|multitype:multitype:
     */
    function get_current_feedback_users($questionids, array $assignerids){
        global $DB;
        if (!$assignerids) {
            return array();
        }
        $questionids = $this->parse_questionids($questionids);
        // This isn't very elegant, but it keeps the sql straightforward.
        list ($assignersql1, $assignerparams1) = $DB->get_in_or_equal($assignerids ,SQL_PARAMS_NAMED);
        list ($assignersql2, $assignerparams2) = $DB->get_in_or_equal($assignerids ,SQL_PARAMS_NAMED);
        list ($assignersql3, $assignerparams3) = $DB->get_in_or_equal($assignerids ,SQL_PARAMS_NAMED);

        list ($qidsql, $qidparams) = $DB->get_in_or_equal($questionids ,SQL_PARAMS_NAMED);

        $qubaids = $this->get_qubaid_condition(array());
        $sql = "SELECT DISTINCT quiza.userid
                    FROM {$qubaids->from_question_attempts('qa')}
                    LEFT JOIN {local_fm_feedback_assign} fa ON fa.questionattemptid = qa.id AND COALESCE(fa.sent,0) = 0 AND fa.userid $assignersql1
                    LEFT JOIN {local_fm_feedback_custom} fc ON fc.questionattemptid = qa.id AND COALESCE(fc.sent,0) = 0 AND fc.userid $assignersql2
                    LEFT JOIN {local_fm_feedback_grade} fg ON fg.questionattemptid = qa.id AND fg.userid $assignersql3
                    WHERE (fa.id IS NOT NULL OR fc.id IS NOT NULL OR fg.id IS NOT NULL)
                       AND {$qubaids->where()}
                       AND qa.questionid $qidsql
            ";
        $params = array_merge($assignerparams1, $assignerparams2, $assignerparams3, $qidparams, $qubaids->from_where_params());
        $userids = $DB->get_records_sql($sql, $params);
        return $this->get_usermanager()->get_all_users(array_keys($userids));
    }

    function get_fbdata(array $qids, array $uids, $fmgroupid) {
        $groupmanager = $this->get_groupmanager();
        $group = $groupmanager->get_group($fmgroupid);

        $conditions = array('users'=>array_keys($group->members));
        $assignments = $this->get_assignments($qids, $conditions);
        $fbdata = array();
        foreach($assignments as $qattemptid => $assignments){
            $attempt = $assignments->get_question_attempt();
            $assigns = array();
            // Collect assigned feedback
            foreach($assignments->get_feedback_assigns() as $assign){
                // Feedback bank assignments
                if (!in_array($assign->get_userid(), $uids)) {
                    continue;
                }
                if (!$assign->can_send()) {
                    continue;
                }
                $assigns[$assign->id] = $assign;
            }
            if ($assignments->has_feedback_custom('current')) {
                // Custom feedback
                $custom = $assignments->get_feedback_custom();
                $custom = reset($custom);
                if (in_array($custom->get_userid(), $uids)) {
                    $assigns['custom'] = $custom;
                }
            }
            if ($assignments->has_feedback_grade('current')) {
                // Grade feedback
                $grade = $assignments->get_feedback_grade();
                $grade = reset($grade);
                if (in_array($grade->get_userid(), $uids)) {
                    $assigns['grade'] = $grade;
                }
            }
            if (!empty($assigns)) {
                if (!isset($fbdata[$qattemptid])) {
                    $fbdata[$qattemptid] = array();
                }
                $fbdata[$qattemptid] += $assigns;
            }
        }
        return $fbdata;
    }


    /**
     * Send feedback for provided users and a specific subset of the assigned feedback objects.
     * NOTE: Assigned feedback objects including both local_fm_feedback_assign and
     * local_fm_feedback_assign.
     * @param array $users    Array of user objects indexed by userid
     * @param array $fbdata   Array of assigned feedback objects indexed first by userid
     * @return bool
     */
    function send_feedback(array $users, array $fbdata){
        $result = true;
        $attempts = $this->get_attempts(array_keys($fbdata));
        $result = $result && $this->send_quiz_feedback($attempts, $fbdata);
        foreach($users as $id => $user){
            $result = $result && $this->send_user_feedback($user, $attempts, $fbdata);
        }
        return $result;
    }
    /**
     * Send feedback to a user from a set of given assignments.
     * @param stdClass $userto
     * @param array $fbdata
     * @return bool
     */
    function send_user_feedback($userto, $attempts, $fbdata){
        global $USER;

        $message = $this->create_message($USER, $userto, $attempts, $fbdata);

        $result = $message->send();

        if ($result) {
            $timesent = time();
            foreach($fbdata as $qadata){
                foreach($qadata as $feedback){
                    $feedback->mark_sent($timesent);
                }
            }
        }

        return $result;
    }

    /**
     * Send feedback to the moodle quiz so that it will be visible to the student
     * @param array $fbdata
     * @return bool
     */
    function send_quiz_feedback($attempts, $fbdata){
        global $DB;

        foreach ($attempts as $qid => $qattempts) {
            foreach($qattempts as $qaid => $attempt) {
                $qfeedback = array();
                foreach($fbdata[$qaid] as $feedback){
                    $text = $feedback->get_text();
                    if (!empty($text)) {
                        $qfeedback[] = html_writer::tag('p', s($text));
                    }
                }
                if (!empty($qfeedback)) {
                    $qfeedbackhtml  = html_writer::tag('h2', get_string('instructorfeedback', 'local_fm') .' ('.strftime(get_string('strftimedatefullshort')).')');
                    $qfeedbackhtml .= implode("\n", $qfeedback);
                    $this->submit_comment($attempt, $qfeedbackhtml, FORMAT_HTML, true);
                }
            }
        }
        return true;
    }

    function get_attempts($qaids) {
        // load all attempts.
        $dm = new question_engine_data_mapper();
        $attempts = array();
        foreach ($qaids as $qaid) {
            $attempt = $dm->load_question_attempt($qaid);
            $attempts[$attempt->get_question()->id][$qaid] = $attempt;
        }
        return $attempts;
    }

    /**
     * Set feedback instances used in the context.
     * @param array $contextfbids
     * @param array $questionfbids
     * @param int $contextid (optional) A specific context id to limit where feedback is *defined*
     */
    function set_feedback_instances(array $contextfbids, array $questionfbids, $contextid = null){
        // Get all relevant feedback instances sorted for syncing
        $allinstances = array();
        $instances = $this->get_feedback_instances(true);
        foreach($instances as $id => $instance){
            if ($instance->was_sent()) {
                continue;    // Cannot change already sent feedback instances
            }
            $feedback = $instance->get_feedback();
            if (!$feedback) {
                continue;    // No feedback still defined for this instance (artifact)
            }

            // Filter feedback instances with feedback not defined in context and/or question
            if ($feedback->in_question_context()) {
                if (($questionid === false) || ($feedback->get_question()->id != $questionid)) {
                    continue;
                }
            } else {
                if (isset($contextid) && ($feedback->get_context()->id != $contextid)) {
                    continue;
                }
            }

            // Sort relevant feedback instances
            if ($instance->in_question_context()) {
                $allinstances['question'][$feedback->id] = $instance;
            } else {
                $allinstances['context'][$feedback->id] = $instance;
            }
        }

        // Determine how feedback instances are to be configured
        $to_add = array();
        foreach($contextfbids as $feedbackid) {
            if (empty($allinstances['context'][$feedbackid])) {
                $to_add['context'][] = $feedbackid;
            } else {
                unset($allinstances['context'][$feedbackid]);
            }
        }
        foreach($questionfbids as $feedbackid){
            if (empty($allinstances['question'][$feedbackid])) {
                $to_add['question'][] = $feedbackid;
            } else {
                unset($allinstances['question'][$feedbackid]);
            }
        }

        // Remove all feedback instances no longer configured
        foreach($allinstances as $type => $instances){
            foreach($instances as $fbiid => $instance){
                $this->delete_feedback_instance($instance);
            }
        }

        // Insert all new feedback instances to be configured
        foreach($to_add as $type => $newfeedbacks){
            $qid = ($type == 'question') ? true : null;

            foreach($newfeedbacks as $feedbackid){
                $this->add_feedback_instance($feedbackid, $qid);
            }
        }

    }

    function set_navigation_base(){
        $navurl = $this->get_navigation_baseurl();
        navigation_node::override_active_url($navurl);
    }

    function submit_grade(question_attempt $attempt, $mark = null) {
        $this->manual_grade($attempt, $mark, null, null);
    }

    function submit_comment(question_attempt $attempt, $comment, $commentformat, $append = false) {
        if ($append) {
            $prevcomment = $attempt->get_last_behaviour_var('comment');
            $prevcommentformat = $attempt->get_last_behaviour_var('commentformat');
            if (!empty($prevcomment)) {
                if ($commentformat != $prevcommentformat) {
                    error_log("Warning: merging comments with different formats");
                }
                $comment = $prevcomment.$comment;
            }
        }
        $this->manual_grade($attempt, null, $comment, $commentformat);
    }

    function manual_grade(question_attempt $attempt, $mark, $comment, $commentformat){
        if ($attempt->get_question()->qtype->is_manual_graded()) {
            $prevgrade = $attempt->get_fraction() * $attempt->get_max_mark();
            if (is_null($mark)) {
                // resubmit existing grade
                $mark = $prevgrade;
            } else {
                if (abs($mark-$prevgrade) < 0.0001) {
                    $mark = $prevgrade;
                } else {
                    if (!is_numeric($mark)) {
                        throw new local_fm_invalid_input_exception();
                    }
                    $fraction = $mark / $attempt->get_max_mark();
                    if ($fraction > 1 || $fraction < $attempt->get_min_fraction()) {
                        throw new local_fm_invalid_input_exception();
                    }
                    // New grade, so mark attempt with grade feedmark
                    $this->assign_feedback_grade($attempt);
                }
            }
        } else if (!is_null($mark)) {
            // Not manually graded, so we shouldn't be submitting a grade
            throw new local_fm_invalid_input_exception();
        }
        if (is_null($comment)) {
            // resubmit existing comment
            $comment = $attempt->get_last_behaviour_var('comment');
            $commentformat = $attempt->get_last_behaviour_var('commentformat');
        }
        if (is_null($commentformat)) {
            // Make sure this is set so that we don't generate a warning.
            $commentformat = FORMAT_MOODLE;
        }
        global $DB;

        $transaction = $DB->start_delegated_transaction();
        $quba = $this->get_question_usage($attempt->get_usage_id());
        $observer = new question_engine_unit_of_work($quba);
        $attempt->set_observer($observer);
        $attempt->manual_grade($comment, $mark, $commentformat);
        $observer->save(new question_engine_data_mapper());
        $transaction->allow_commit();
    }

    function has_multiple_user_attempts(){
        return false;
    }

    function verify_page(){
        static::page_context_check($this->get_context());
    }

    // TODO
    // - variable substitution
    // - Messaging
}

/**
 * A collection of users for feedback purposes, either a single user of a moodle group of users.
 * @author petro
 *
 */
abstract class local_fm_group {
    public $courseid;

    // array of user records indexed by userid
    // each should contain at least id, firstname, lastname
    public $members = array();

    public function __construct($courseid) {
        $this->courseid = $courseid;
    }

    public function add_member($user) {
        $this->members[$user->id] = $user;
    }

    abstract public function get_name($attemptuser = null);
    abstract public function get_picture();
    abstract public function get_uniqueid();
}

class local_fm_group_moodlegroup extends local_fm_group {
    public $group;

    public function __construct($group, $courseid) {
        $this->group = $group;
        parent::__construct($courseid);
    }

    // unique id is moodle group id
    public function get_uniqueid() {
        return $this->group->id;
    }

    public function get_name($attemptuser = null) {
        if ($attemptuser) {
            $attemptuserhtml = html_writer::tag('span', '('.fullname($attemptuser).')', array('class'=>'attemptuser'));
        } else {
            $attemptuserhtml = '';
        }
        return s($this->group->name) . $attemptuserhtml;
    }

    public function get_picture() {
        $grouppicture = new local_fm_group_picture($this->group);
        $grouppicture->courseid = $this->courseid;
        return $grouppicture;
    }
}

class local_fm_group_moodleuser extends local_fm_group {
    public $user;

    public function __construct($user, $courseid) {
        $this->user = $user;
        parent::__construct($courseid);
    }

    // unique id is (-1)*userid.  This is so that we don't overlap the moodle groupid space.
    public function get_uniqueid() {
        return - $this->user->id;
    }

    public function get_name($attemptuser = null) {
        return s(fullname($this->user));
    }

    public function get_picture() {
        $userpicture = new user_picture($this->user);
        $userpicture->courseid = $this->courseid;
        $userpicture->link = false;  // No link, to improve the tabindex behavior
        return $userpicture;
    }
}

/**
 * Group_picture class.
 *
 * This is analogous to moodle's internal user_picture class.
 *
 * @author petro
 *
 */
class local_fm_group_picture implements renderable {
    public $group;
    public $courseid;

    public function __construct($group) {
        $this->group = $group;
    }
}


/**
 * Group manager
 *
 * This class handles groups of students
 *
 * @author petro
 *
 */
class local_fm_group_manager {

    protected $groupingid;
    protected $courseid;
    protected $usermanager;
    protected $hasoverlappinggroups = false;

    // array of local_fm_group indexed by moodle groupid
    protected $groups;

    // array of local_fm_group indexed by moodle userid
    protected $usergroups;

    public function __construct($groupingid, $courseid, local_fm_user_manager $usermanager) {
        $this->groupingid = $groupingid;
        $this->courseid = $courseid;
        $this->usermanager = $usermanager;
        $this->init_groups();
    }

    public function init_groups() {
        global $DB;
        if (is_null($this->groupingid)) {
            // No groups
            $this->groups = array();
        } else {
            // Get groups in grouping
            $moodlegroups = groups_get_all_groups($this->courseid, 0, $this->groupingid);
            foreach ($moodlegroups as $group) {
                $this->groups[$group->id] = new local_fm_group_moodlegroup($group, $this->courseid);
            }
        }

        if (!empty($this->groups)) {
            // Get users in each group
            list ($ingrouping, $params) = $DB->get_in_or_equal(array_keys($this->groups));
            $members = $DB->get_records_list('groups_members', 'groupid', array_keys($this->groups));

            // preload users so that we (and everyone else) have them
            $userids = array();
            foreach ($members as $member) {
                $userids[] = $member->userid;
            }
            $this->usermanager->preload_users(array_unique($userids));

            $this->usergroups = array();
            foreach ($members as $member) {
                // Update group members
                $this->groups[$member->groupid]->add_member($this->usermanager->get_user($member->userid));
                // Update index by userid
                // Note: this is a simple assignment, so in the case of a user belonging to multiple
                // groups, this is where we decide which to use.
                if (isset($this->usergroups[$member->userid])) {
                    // Already saw this user in another group
                    $this->hasoverlappinggroups = true;
                    // Use the group with the highest group id.  This is arbitrary, but will at least be consistent.
                    if ($this->usergroups[$member->userid]->group->id < $member->groupid) {
                        $this->usergroups[$member->userid] = $this->groups[$member->groupid];
                    }
                } else {
                    // Haven't seen this user, so add to index
                    $this->usergroups[$member->userid] = $this->groups[$member->groupid];
                }
            }

        }
    }

    public function has_overlapping_groups() {
        return $this->hasoverlappinggroups;
    }

    public function get_attempt_group(question_attempt $attempt) {
        $userid = local_fm_question_attempt_feedback::get_question_attempt_userid($attempt);
        return $this->get_user_group($userid);
    }

    public function get_user_group($userid) {
        if (!isset($this->usergroups[$userid])) {
            // Not in an existing group, so make a new group for just this user
            $user = $this->usermanager->get_user($userid);
            $this->usergroups[$userid] = new local_fm_group_moodleuser($user, $this->courseid);
            $this->usergroups[$userid]->add_member($user);
        }
        return $this->usergroups[$userid];
    }

    public function get_group($uniqueid) {
        if ($uniqueid < 0) {
            $userid = - $uniqueid;  // single user
            return $this->get_user_group($userid);
        } else {
            foreach ($this->groups as $fmgroup) {
                if ($fmgroup->get_uniqueid() == $uniqueid) {
                    return $fmgroup;
                }
            }
        }
        return false;
    }

    public function get_attempt_users(question_attempt $attempt) {
        $group = $this->get_attempt_group($attempt);
        return $group->members;
    }
}

/**
 * User manager
 *
 * This class is used for efficiently loading moodle users.
 *
 * @author petro
 *
 */
class local_fm_user_manager {

    protected $userfields;

    protected $users = array();

    public function __construct($userfields) {
        $this->userfields = $userfields;
    }

    public function get_user($userid) {
        global $DB;
        if (!isset($this->users[$userid])) {
            $this->preload_users(array($userid));
        }
        return $this->users[$userid];
    }

    public function get_all_users(array $userids) {
        global $DB;
        $this->preload_users($userids);
        $users = array();
        foreach ($userids as $userid) {
            $users[$userid] = $this->users[$userid];
        }
        return $users;
    }

    public function preload_users(array $userids) {
        global $DB;
        $newuserids = array_diff($userids, array_keys($this->users));
        $newuserids = array_unique($newuserids);
        if (!empty($newuserids)) {
            $newusers = $DB->get_records_list('user', 'id', $newuserids, '', $this->userfields);
            $this->users = $this->users + $newusers;
        }
    }
}

local_fm_manager::register_manager_type('quiz', '/mod/quiz/report/fbmanager/lib.php', 'quiz_fm_manager');

