<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package local_fm
 * @copyright 2014 University of Wisconsin
 * @author Nick Koeppen, Matt Petro
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined ( 'MOODLE_INTERNAL' ) || die ();

/* Moodle plugin strings */
$string['pluginname'] = 'Feedback Manager';
// Capabilities
$string['fm:assignfeedback'] = 'Assign feedback';
$string['fm:manage'] = 'Manage all feedback';
$string['fm:managecategories'] = 'Manage feedback categories';
$string['fm:managefeedback'] = 'Manage other feedback';
$string['fm:manageownfeedback'] = 'Manage own feedback';
$string['fm:messageconfig'] = 'Configure feedback messages';
$string['fm:managesharedfeedback'] = 'Manage shared feedback';
$string['fm:viewfeedbackbank'] = 'View feedback bank';
// Messages
$string['messageprovider:question_feedback'] = 'Question feedback';

/* Plugin custom strings */
$string['actionwithselected'] = 'Action with selected';
$string['actionwithselected_help'] = '<p>The selected answers are marked with a checked checkbox in the rightmost column of the answers table.
An action performed on selected answers will be apply to all selected, even those not visible on the
presently displayed page.</p>

<p><b>Assign tag</b> assigns the selected tag to all selected answers or in the case when no
tag is selected, the action removes the current tag assignment.</p>

<p><b>Assign grade</b> assigns the grade set in the input field next to the "Assign grade" button.
However, if the grade set exceeds the maximum allowable points indicated to the right of the field,
zero will be assigned.</p>';
$string['addcategory'] = 'Add category';
$string['addfeedback'] = 'Add feedback';
$string['addfeedbackcategory'] = 'Add feedback category';
$string['addfeedbacktoquiz'] = 'Add to entire quiz';
$string['addfeedbacktoquiz_help'] = 'Enable this to make the feedback available to every question in the quiz.';
$string['addfrombank'] = 'Add from feedback bank';
$string['addfilter'] = 'Add filter';
$string['addnewcategory'] = 'Add new category...';
$string['addnewsubcategory'] = 'Add new sub-category...';
$string['addnewfeedbacksubmit'] = 'Add new feedback';
$string['addnewfeedback'] = 'Add new feedback...';
$string['allornone'] = 'All / None';
$string['allquestions'] = 'All questions';
$string['answercontains'] = 'Answer contains \'{$a}\'';
$string['answercontainsprompt'] = 'Answer contains';
$string['assign'] = 'Assign';
$string['assignedby'] = 'Assigned by: {$a}';
$string['assignedfeedbackis'] = 'Assigned feedback is \'{$a}\'';
$string['assignfeedbackbutton'] = 'Return to assign feedback...';
$string['availablefeedback'] = 'Available feedback';
$string['availablefeedback_help'] = 'This columns lists predefined feedback which can be assigned to student responses.  New feedback can be added directly on this page, or by adding items from a feedback bank.';
$string['category'] = 'Feedback category';
$string['categoryinfo'] = 'Description';
$string['clearallconfirm'] = 'All changes not yet saved on this page will be deleted. Are you sure you want to continue?';
$string['cleartemplate'] = 'Remove template from scope';
$string['configurefeedback'] = 'Configure feedback';
$string['controls'] = 'Controls';
$string['createdby'] = 'Created by';
$string['customfeedback'] = 'Individual feedback';
$string['customfeedback_help'] = 'Add individual feedback to a response.';
$string['deletefeedback'] = 'Delete this feedback';
$string['editcategory'] = 'Edit category';
$string['editfeedback'] = 'Edit feedback';
$string['editfeedbackcategory'] = 'Edit feedback category';
$string['editfilter'] = 'Edit filter';
$string['editsharing'] = 'Edit feedback sharing';
$string['edittemplate'] = 'Edit feedback template';
$string['edittemplate_help'] = 'Feedback templates are used to format the email messages sent to students.  The variables in the table below are available for substitution.';
$string['emailsubject'] = 'Subject: ';
$string['feedback'] = 'Assign feedback';
$string['feedback_help'] = 'Assign a predefined feedback item to a response.  Predefined feedback is created and edited to the left of the responses table.<br /><br />This column also allows marking of student responses for reporting purposes, and removing existing feedback assignments.  Feedback which has already been sent to students cannot be modified.';
$string['feedbackbank'] = 'Feedback bank';
$string['feedbackcategories'] = 'Show feedback categories: ';
$string['feedbackmenucontext'] = 'All questions';
$string['feedbackmenuquestion'] = 'This question';
$string['feedbackname'] = 'Feedback name';
$string['feedbackprivateshare'] = 'This feedback is currently private and must be changed to allow sharing.';
$string['feedbacksendassignedby'] = 'Send feedback assigned by:';
$string['feedbacksendquestions'] = 'Send feedback for questions:';
$string['feedbackspecificto'] = 'Feedback specific to:';
$string['feedbacktext'] = 'Feedback text';
$string['grade'] = 'Grade';
$string['grade_help'] = 'Edit grade for attempt.';
$string['filters'] = 'Filters';
$string['filters_help'] = 'Filters are used to limit the student attempts displayed on this page.  For example, you can limit attempts by class group, question answer or question grade.';
$string['filter_answer'] = 'Question answer';
$string['filter_feedback_custom'] = 'Individual feedback';
$string['filter_feedback'] = 'Assigned feedback';
$string['filter_grade'] = 'Question grade';
$string['filter_group'] = 'Group membership';
$string['filter_message_last'] = 'Last user message date';
$string['filter_message_count'] = 'User message count';
$string['inallquestions'] = 'Use in all questions';
$string['inthisquestion'] = 'Use in this question';
$string['instructorfeedback'] = 'Instructor feedback';
$string['invalidassignsubmission'] = 'Invalid submission data. Ensure grades are within range and operations are allowed.';
$string['invalidinput'] = 'Invalid input given';
$string['label'] = 'Label';
$string['lastmessageafter'] = 'Last message after {$a}.';
$string['lastmessagebefore'] = 'Last message before {$a}.';
$string['newfeedback'] = 'New feedback';
$string['newfeedback_help'] = 'Use this form to create new feedback entries which can be assigned to student responses.  If you enter a label but no text, then the feedback will not be visible to students.  You can use this feature to tag student responses.';
$string['managefeedback'] = 'Manage feedback';
$string['managefeedbackbankhere'] = 'Manage feedback bank here...';
$string['memberof'] = 'Member of {$a}.';
$string['message'] = 'Message';
$string['messagecount'] = 'Message count';
$string['messagesubject'] = 'Message subject';
$string['modifiedby'] = 'Modified by';
$string['module'] = 'Quiz'; // TODO Make this more general
$string['namecantbeblank'] = 'Name cannot be blank';
$string['nameofcopieditem'] = '{$a} copy';
$string['needsave'] = 'Changes made requiring save.';
$string['nofeedback'] = 'No feedback';
$string['nofeedbackhere'] = 'No feedback defined here.';
$string['nofeedbackassigned'] = 'No feedback assigned.';
$string['nofeedbackincategory'] = 'No feedback in this category.';
$string['nofeedbacktosend'] = 'No new feedback to send.';
$string['nothingdefinedincontext'] = 'Nothing defined in this context.';
$string['notlogged'] = 'You are not logged in or are not authorized to use this service.  Please reload the page and try again.';
$string['notoutsidecourse'] = 'Not applicable outside of course context.';
$string['notypical'] = 'Show this answer in report';
$string['parentcategory'] = 'Parent category';
$string['privatenosend'] = 'Instructor tag.  Will not be sent to students.';
$string['removefromquestion'] = 'Remove from this question.';
$string['reporttext'] = 'Report text';
$string['responsesperpage'] = 'Responses per page:';
$string['saveall'] = 'Save all';
$string['saveallconfirm'] = 'All changes not yet submitted on this page will be saved. Are you sure you want to continue?';
$string['savechanges'] = 'Save changes';
$string['select'] = 'Select';
$string['select_help'] = 'Select responses for use by bulk operations at the bottom of the table.';
$string['selectagroup'] = 'Select a group';
$string['selectauser'] = 'Select a user';
$string['sent'] = 'Sent';
$string['settemplate'] = 'Set template';
$string['sharegeneral'] = 'Shared';
$string['shareinvalud'] = 'INVALID';
$string['shareprivate'] = 'Private';
$string['sharing'] = 'Sharing';
$string['sharing_help'] = 'Configure sharing for this feedback:
<p><ul>
<li> Shared - Share this feedback with all feedback managers in this context </li>
<li> Private - Do not allow any sharing of this feedback </li>
</ul></p>';
$string['showall'] = 'Show all';
$string['subcategories'] = 'Sub-categories';
$string['switchtohtml'] = 'Switch to html format...';
$string['switchtoplain'] = 'Switch to plain format...';
$string['systemplatesubject'] = 'Feedback on $quizname';
$string['systemplatemessage'] = 'Here is feedback for the recent quiz, $quizname, in $coursefullname:<br/><br/>$questionresponsefeedback<br/><br/>You can view the graded quiz here:<br/><br/>$quizurl<br/><br/>Please contact me if you have questions.<br/><br/>$userfromfirstname $userfromlastname';
$string['templatecoursevar'] = 'Course/Activity';
$string['templatecoursevar_help'] = 'These variables contain course and activity information.';
$string['templatefeedbackvar'] = 'Feedback';
$string['templatefeedbackvar_help'] = 'These variables contain question and feedback information.
<ul>
<li><b>questionresponsefeedback</b>: <br /> Quiz questions, feedback and grades for questions with feedback</li>
<li><b>allquestionresponsefeedback</b>: <br /> All Quiz questions, feedback and grades for questions with or without feedback</li>
<li><b>questionresponsefeedbackX</b>: <br /> Question, feedback and grade for only question #X</li>
<li><b>questionnameX, questiontextX, responseX, feedbackX, markX, maxmarkX</b>: <br /> Specific elements for question #X</li>
</ul>';
$string['templateuservar'] = 'From/To';
$string['templateuservar_help'] = 'These variables contain student and instructor names.  The instructor variable contains the name of the person actually sending the feedback.';
$string['templateexistsfor'] = 'Load existing template for {$a}.';
$string['templatesaved'] = 'The template has been saved.';
$string['templatescope'] = 'Template scope';
$string['templatescope_help'] = 'Templates can be defined just for this activity, for all activities in a course, or for all courses in a course category.  The most specific match is always used, so a template on the activity will override any other template.';
$string['text'] = 'Text';
$string['timeafter'] = 'after';
$string['timebefore'] = 'before';
$string['timerange'] = 'Time range';
$string['thisquestion'] = 'This question';
$string['typical'] = 'Omit this answer from report';
$string['unused'] = 'Unused';
$string['userinfo'] = 'User information';
