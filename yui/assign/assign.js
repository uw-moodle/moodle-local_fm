YUI.add('moodle-local_fm-assign', function(Y) {
    // The CSS selectors we use
    var CSS = {
        ASSIGNTABLE : 'table.fm_assign_table',
        CUSTOMFB : '.col_customfb textarea',
        FEEDBACK : '.col_feedback select',
        FEEDBACKCOMMANDS : '.col_feedback .commands a',
        GRADE : '.col_grade input[type="text"]',
        TABLECELL : 'td',
        ASSIGNDIV : 'div.assignfeedback',
        TAGLISTDIV : 'div.taglist',
        PAGECONTENT : 'div#page-content'
    };
    
    /**
     * The fb assign table class
     */
    var ASSIGN = function() {
        ASSIGN.superclass.constructor.apply(this, arguments);
    }

    Y.extend(ASSIGN, Y.Base, {
        initializer : function(config) {
            var table = Y.one(CSS.ASSIGNTABLE);
            table.delegate('change', this.submit_custom_feedback, CSS.CUSTOMFB, this);
            table.delegate('change', this.submit_grade, CSS.GRADE, this);
            // Prevent the enter key from submitting the form
            table.delegate('keypress', function(e) {if (e.keyCode == 13) {this.submit_grade(e)}}, CSS.GRADE, this);
            table.delegate('change', this.submit_feedback, CSS.FEEDBACK, this);
            table.delegate('click', this.submit_feedback_command, CSS.FEEDBACKCOMMANDS, this);
            // Auto select grade item on click
            var focused;
            table.delegate('click', function(e) {if (focused != this) {focused = this; this.select();}}, CSS.GRADE);
            table.delegate('blur', function(e) {focused = false;}, CSS.GRADE);
        },
        submit_custom_feedback : function(e) {
            // Prevent the default action
            e.preventDefault();
            
            var data = [];
            
            var name = e.target.getAttribute('name');
            var id = name.match(/\[(.*)\]/)[0];
            
            data[name] = e.target.get('value');
            data['addfeedback'+id] = '1';
            
            var ajaxcontrols = new this.add_ajaxcontrols(Y, e.target.ancestor('td'));
            this.send_request(data, ajaxcontrols);
        },
        submit_grade : function(e) {
            // See if this is already set to autosubmit form
            if (e.target.hasClass('autosubmit') || e.target.hasClass('noajax')) {
                return;
            }
            // Prevent the default action
            e.preventDefault();
            
            var data = [];
            
            var name = e.target.getAttribute('name');
            var id = name.match(/\[(.*)\]/)[0];
            
            data[name] = e.target.get('value');
            data['submitgrade'+id] = '1';
            
            var ajaxcontrols = new this.add_ajaxcontrols(Y, e.target.ancestor('td'));
            this.send_request(data, ajaxcontrols);
        },
        submit_feedback : function(e) {
            // See if this is already set to autosubmit form
            if (e.target.hasClass('autosubmit') || e.target.hasClass('noajax')) {
                return;
            }
            // Prevent the default action
            e.preventDefault();
            
            var data = [], value;
            
            value = e.target.get('value');
            if (value !== "") {
                data[e.target.getAttribute('name')] = value;
                var ajaxcontrols = new this.add_ajaxcontrols(Y, e.target.ancestor('td'));
                this.send_request(data, ajaxcontrols);
            }
        },
        submit_feedback_command : function(e) {
            var href = e.currentTarget.getAttribute('href');
            var parts = href.split('?', 2);
            if (parts.length == 2) {
                // Prevent the default button action
                e.preventDefault();
                var params = Y.QueryString.parse(parts[1]);
                var ajaxcontrols = new this.add_ajaxcontrols(Y, e.target.ancestor('td'));
                this.send_request(params, ajaxcontrols);
            }
        },
        
        handle_response : function(response) {
            var node;
            for (id in response.cells) {
                node = Y.one('#'+id);
                if (node) {
                    node.setContent(response.cells[id]);
                }
            }
        },

        /**
         * Send a request using the REST API
         *
         * @param data The data to submit
         * @param statusspinner A statusspinner objext
         * @param ajaxresponse An ajax response object
         * @return response responseText field from responce
         */
        send_request : function(data, ajaxcontrols) {
            // Default data structure
            if (!data) {
                data = {};
            }
            // Handle any variables which we must pass back through to
            var pageparams = this.get('config').pageparams;
            for (varname in pageparams) {
                data[varname] = pageparams[varname];
            }

            data.sesskey = M.cfg.sesskey;

            var uri = M.cfg.wwwroot + this.get('ajaxurl');

            // Define the configuration to send with the request
            var responsetext = [];
            var config = {
                method: 'POST',
                data: data,
                on: {
                    success: function(tid, response) {
                        try {
                            responsetext = Y.JSON.parse(response.responseText);
                            if (responsetext.error) {
                                // fatal error
                                ajaxcontrols.error(responsetext.error);
                                new M.core.ajaxException(responsetext);
                            } else if (responsetext.validationerror) {
                                // input validation error
                                ajaxcontrols.error(responsetext.validationerror);
                            } else {
                                // success
                                this.handle_response(responsetext);
                                ajaxcontrols.complete();
                            }
                        } catch (e) {
                            // unknown error
                            ajaxcontrols.error('Error communicating with server');
                            new M.core.ajaxException(response);
                        }
                    },
                    failure : function(tid, response) {
                        ajaxcontrols.error('Error communicating with server');
                        new M.core.ajaxException(response);
                    }
                },
                context: this,
                sync: true
            }

            ajaxcontrols.showspinner();
            ajaxcontrols.hideresponse();

            // Send the request
            Y.io(uri, config);
            return responsetext;
        },
        /**
         * Appends a hidden spinner and response element to the specified node.
         *
         * @param {YUI} Y
         * @param {Node} the node the spinner should be added to
         * @return {object} the spinner object
         */
        add_ajaxcontrols : function(Y, node) {
            var WAITICON = {'pix':"i/loading_small",'component':'moodle'};
            
            if (node.one('.spinner')) {
                this.spinnernode = node.one('.spinner');
            } else {
                this.spinnernode = Y.Node.create('<span/>').addClass('spinner').hide();
                node.append(this.spinnernode);
            }
            if (node.one('.ajaxresponse')) {
                this.responsenode = node.one('.ajaxresponse');
            } else {
                this.responsenode = Y.Node.create('<span/>').addClass('ajaxresponse').hide();
                node.append(this.responsenode);
            }
            this.spinnernode.setContent('<img src="'+M.util.image_url(WAITICON.pix, WAITICON.component)+'" class="iconsmall"/>');
            this.hidespinner = function() {
                this.spinnernode.hide();
            }
            this.showspinner = function() {
                this.spinnernode.show();
            }
            this.hideresponse = function() {
                this.responsenode.hide();
            }
            this.showresponse = function() {
                this.responsenode.show();
            }
            this.error = function(message) {
                this.responsenode.setContent('<span class="ajaxerror">&#x2718;&nbsp;'+message+'</span>');
                this.showresponse();
                this.hidespinner();
            }
            this.complete = function() {
                this.spinnernode.setContent('<span class="ajaxok">&#x2714;</span>');
                this.hideresponse();
            }

            return this;
        },
    },
    {
        NAME : 'local_fm-assign',
        ATTRS : {
            ajaxurl : {
                'value' : 0
            },
            config : {
                'value' : 0
            }
        }
    }
    );

    M.local_fm = M.local_fm || {};

    M.local_fm.init_assign = function(config) {
        return new ASSIGN(config);
    };
},
'@VERSION@', {
    requires : ['base', 'node', 'event', 'io', 'json', 'querystring-parse', 'moodle-core-notification']
}
);
