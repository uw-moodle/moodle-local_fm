YUI.add('moodle-local_fm-sidebar', function(Y) {
    var SIDEBARNAME = 'local_fm_sidebar';
    var SIDEBAR = function() {
        SIDEBAR.superclass.constructor.apply(this, arguments);
    }

    Y.extend(SIDEBAR, Y.Base, {
        initializer : function(params) {
            var sidebar = Y.one('#quiz_fbmanager_assign_sidebar');
            var sidebarpos = Y.one('#quiz_fbmanager_assign_sidebar_marker').getY();
            var fixedheader = Y.one('header.navbar-fixed-top')
            var fixedheaderheight = (fixedheader)? parseInt(fixedheader.getComputedStyle('height')) : 0;
            var pinned = 0;

            var checksidebar = function () {
                var scrolltop = ((window.pageYOffset + 1) || (document.scrollTop + 1)) - 1;
                if (scrolltop > sidebarpos - fixedheaderheight && !pinned) {
                    pinned = 1;
                    sidebar.addClass('pinned');
                    sidebar.setStyle('top', fixedheaderheight);
                } else if (scrolltop <= sidebarpos && pinned) {
                    pinned = 0;
                    sidebar.removeClass('pinned');
                    sidebar.setStyle('top', 0);
                }
            };
            var delayedscrollhandler = function () {
                setTimeout(function() {
                    checksidebar();
                    Y.once('scroll', delayedscrollhandler);
                }, 100);
            };
            checksidebar();
            Y.once('scroll', delayedscrollhandler);
        }
    },{
        NAME : SIDEBARNAME,
        ATTRS : {}
    });

    M.local_fm = M.local_fm || {};
    M.local_fm.init_sidebar = function(config) {
        return new SIDEBAR(config);
    };
},
'@VERSION@', {
    requires : ['base', 'node', 'event']
}
);
