<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->libdir.'/tablelib.php');

class local_fm_report_edit_form extends moodleform {

    function definition(){
        $mform = $this->_form;

        $report = $this->_customdata['report'];

        $options = array('rows' => 12, 'cols' => 60);
        $mform->addElement('editor', 'text', get_string('reporttext', 'local_fm'), $options);
        $mform->setType('text', PARAM_RAW);

        $this->add_action_buttons();

        $report->form_set_data($mform);
    }

}

class local_fm_user_report extends html_table {

    /**
     * Feedback manager
     * @var local_fm_manager
     */
    protected $manager;

    function __construct(local_fm_manager $manager){
        global $DB;

        $this->manager = $manager;
    }

    function get_users_for_display(){
        return $this->manager->get_attempt_users();
    }

    function set_data(){

    }

    function setup(){
        $this->id    = 'fm_user_report_table';

        $this->head  = array();
        $count = 1;
        foreach($this->manager->get_questions() as $qid => $question){
            $this->head[] = 'Q. '.$count;
        }

        $this->align = array_fill(0, count($this->head), 'center');
    }

}