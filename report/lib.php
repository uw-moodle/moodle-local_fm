<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class local_fm_report extends local_fm_contextual {
    const DB_TABLE = 'local_fm_reports';

    const TYPE_FEEDBACK = 1;
    const TYPE_RESPONSE = 2;

    /**
     * Get (or create anew) the report defined at this context with an optional type
     * specified.
     * @param context $context
     * @param int $type (optional)
     * @return local_fm_report
     */
    static function get_for_context(context $context){
        global $DB;

        $params = array('contextid' => $context->id);
        $record = $DB->get_record(self::DB_TABLE, $params);
        if (!$record) {
            // Create a new definition
            $record = new stdClass();
            $record->contextid = $context->id;
        }

        return static::create($record);
    }

    protected $text;
    protected $textformat;
    protected $config;

    function form_load_data($formdata){
        if (isset($formdata->text)) {
            $formdata->textformat = $formdata->text['format'];
            $formdata->text       = $formdata->text['text'];
        }
        if (isset($formdata->config)) {
            $formdata->config = json_encode($formdata->config);
        }
        parent::form_load_data($formdata);
    }

    function form_set_data(MoodleQuickForm $mform){
        $formdata = $this->get_db_record();
        $formdata->text = array(
            'text'   => $formdata->text,
            'format' => $formdata->textformat,
        );

        $mform->setDefaults((array)$formdata);
    }

    function format_text($format = null){
        if (!isset($format)) {
            $format = $this->textformat;
        }

        return format_text($this->text, $format, array('filter' => false));
    }

    function get_db_record(){
        $record = parent::get_db_record();
        $record->text = $this->text;
        $record->textformat  = $this->textformat;
        $record->config = $this->config;

        return $record;
    }

    function get_config() {
        $decoded = json_decode($this->config);
        if (!$decoded) {
            $decoded = new stdClass();
        }
        return $decoded;
    }

}