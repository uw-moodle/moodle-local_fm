<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../config.php');
require_once($CFG->dirroot.'/local/fm/locallib.php');
require_once($CFG->dirroot.'/local/fm/assign/forms.php');

$typename   = required_param('type', PARAM_ALPHANUMEXT);
$contextid  = required_param('context', PARAM_INT);
$questionid = optional_param('question', null, PARAM_INT);
$return     = required_param('return', PARAM_URL);

global $PAGE, $OUTPUT;

// Context and capabilities
$manager = local_fm_manager::get_manager($typename, $contextid, $questionid);

// Verify general access
$manager->verify_page();

// Verify manage feedback access
if (!$manager->can_manage_all()) {
    throw new moodle_exception('cannotmanage');
}

// Navigation
$baseurl = new moodle_url('/local/fm/assign/instances.php');
$params = array('type' => $typename, 'context' => $contextid, 'return' => $return);
if ($questionid) {
    $params['question'] = $questionid;
}
$PAGE->set_url($baseurl, $params);
$manager->set_navigation_base();

// Data processing
$fbconfig = new local_fm_feedback_configure($manager);
// Verify feedback bank access
$fbconfig->verify_access();

if ($fbconfig->was_cancelled()) {
    redirect($fbconfig->get_url_current());
} else if ($fbconfig->was_saved()) {
    require_sesskey();
    $viewcid = $fbconfig->get_context_view()->id;
    list($cselect, $qselect) = $fbconfig->get_selected();

    $manager->set_feedback_instances($cselect, $qselect, $viewcid);

    $url = $fbconfig->get_url_current();
    $url->param('saved', true);
    redirect($url);
}

// Setup page
$PAGE->set_title(get_string('configurefeedback', 'local_fm'));
$PAGE->set_heading($PAGE->title);
$PAGE->navbar->add($PAGE->title);

$renderer = $manager->get_page_renderer($PAGE);

// Display page
echo $OUTPUT->header();

echo $OUTPUT->heading($PAGE->heading);

echo $OUTPUT->single_button($fbconfig->get_url_return(), get_string('assignfeedbackbutton', 'local_fm'));

$renderer->render($fbconfig);

echo $OUTPUT->footer();

?>