<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');

class local_fm_filter_edit_form extends moodleform {

    function definition(){
        $mform = $this->_form;

        $filter = $this->_customdata['filter'];
        if (! ($filter instanceof local_fm_filter)) {
            return;
        }

        if ($filter->exists()) {
            $actionstr = get_string('editfilter', 'local_fm');
        } else {
            $actionstr = get_string('addfilter', 'local_fm');
        }

        $mform->addElement('header', 'filterheader', $actionstr);

        $filter->add_to_edit_form($mform);

        $this->add_action_buttons(true, $actionstr);
    }
}