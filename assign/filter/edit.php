<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../../config.php');
require_once($CFG->dirroot.'/local/fm/locallib.php');
require_once($CFG->dirroot.'/local/fm/assign/forms.php');
require_once($CFG->dirroot.'/local/fm/assign/filter/forms.php');
require_once($CFG->dirroot.'/local/fm/renderer.php');

$typename  = required_param('type', PARAM_ALPHANUMEXT);
$contextid = required_param('contextid', PARAM_INT);
$return    = required_param('return', PARAM_URL);

$action = optional_param('action', null, PARAM_ALPHA);
$id     = optional_param('id', null, PARAM_INT);
//OR
$type   = optional_param('filtertype', null, PARAM_ALPHANUMEXT);

global $PAGE, $OUTPUT, $DB, $USER;

// Context and capabilities
$manager = local_fm_manager::get_manager($typename, $contextid);
$manager->verify_page();
if (!$manager->can_assign()) {
    throw new moodle_exception('noaccess');
}

$params = array('type' => $typename, 'contextid' => $contextid, 'return' => $return);
if ($id) {
    $params['id'] = $id;
} else if ($type) {
    $params['filtertype'] = $type;
}
$baseurl = new moodle_url('/local/fm/assign/filter/edit.php');
$PAGE->set_url($baseurl, $params);
$manager->set_navigation_base();

$assigntable = new local_fm_assign_table($manager);
$filters = $assigntable->get_component('filters');

$filter = null;
if ($id) {
    $filter = $filters->get_filter($id);
} else if ($type) {
    $filter = $filters->create_filter($type);
} else {
    $selector = $filters->get_filter_select();
}

if ($action == 'delete') {
    $filters->filter_delete($filter);

    redirect($return);
}

$customdata = array('filter' => $filter);
$editform = new local_fm_filter_edit_form($PAGE->url, $customdata);

if ($editform->is_cancelled()) {
    redirect($return);
} else if ($data = $editform->get_data()) {
    $filters->filter_update($filter, $data);

    redirect($return);
}

$title = $id ? get_string('editfilter', 'local_fm') : get_string('addfilter', 'local_fm');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->navbar->add($title);

$renderer = $manager->get_page_renderer($PAGE);

echo $OUTPUT->header();

echo $OUTPUT->heading($PAGE->heading);

if (isset($selector)) {
    echo $renderer->render($selector);

    echo $OUTPUT->single_button($return, get_string('cancel'));
} else {
    echo $editform->display();
}

echo $OUTPUT->footer();

?>