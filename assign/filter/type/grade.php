<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/assign/filter/lib.php');

class fm_grade_filter extends local_fm_filter_question {
    protected $operators = array(
        'lt' => '<',
        'le' => '<=',
        'eq' => '=',
        'ge' => '>=',
        'gt' => '>',
    );
    protected $operator;
    protected $gradeval;

    function add_to_edit_form(MoodleQuickForm &$mform){
        $opselect = &$mform->createElement('select', 'operator', '', $this->operators);
        $gradeval = &$mform->createElement('text', 'gradeval', '', array('size' => 5));

        $groupels = array($opselect, $gradeval);
        $mform->addGroup($groupels, null, get_string('grade'), array(' '), false);
        $mform->setDefault('operator', $this->operator);
        $mform->setDefault('gradeval', $this->gradeval);
        // TODO make this work with ',' separator
        $mform->setType('gradeval', PARAM_FLOAT);
    }

    function filter(question_attempt $attempt){
        $grade = $attempt->get_mark();
        switch($this->operator){
            case 'lt':
                return !($grade < $this->gradeval);
            case 'le':
                return !($grade <= $this->gradeval);
            case 'eq':
                return !($grade == $this->gradeval);
            case 'ge':
                return !($grade >= $this->gradeval);
            case 'gt':
                return !($grade > $this->gradeval);
        }
    }

    function get_display_label(){
        return get_string('grade').' '.$this->get_operator().' '.$this->gradeval;
    }

    function get_filter_data(){
        $filterdata = parent::get_filter_data();
        $filterdata['operator'] = $this->operator;
        $filterdata['gradeval'] = $this->gradeval;

        return $filterdata;
    }

    function get_operator(){
        return $this->operators[$this->operator];
    }

    function get_type(){
        return 'grade';
    }

    function update_from_edit_form(stdClass $formdata){
        $this->operator = $formdata->operator;
        $this->gradeval = $formdata->gradeval;
    }
}