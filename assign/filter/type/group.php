<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/assign/filter/lib.php');

class fm_group_filter extends local_fm_filter_user {

    protected $groupid;
    protected $_groupmembers;

    function add_to_edit_form(MoodleQuickForm &$mform){
        $course = $this->manager->get_course();
        if (!$course) {
            $mform->addElement('static', 'notice', get_string('notoutsidecourse', 'local_fm'));
        } else {
            $groups = groups_get_all_groups($course->id);

            $options = array();
            foreach($groups as $group){
                $options[$group->id] = $group->name;
            }
            $mform->addElement('select', 'groupid', get_string('group', 'group'), $options);
            $mform->setDefault('groupid', $this->groupid);
        }
    }

    function filter(stdClass $user){
        if (!isset($this->_groupmembers)) {
            $this->_groupmembers = groups_get_members($this->groupid);
        }

        return empty($this->_groupmembers[$user->id]);
    }

    function get_display_label(){
        return get_string('memberof', 'local_fm', groups_get_group_name($this->groupid));
    }

    function get_filter_data(){
        $filterdata = parent::get_filter_data();
        $filterdata['groupid'] = $this->groupid;

        return $filterdata;
    }

    function get_type(){
        return 'group';
    }

    function update_from_edit_form(stdClass $formdata){
        $this->groupid = $formdata->groupid;
    }

}