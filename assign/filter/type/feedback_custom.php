<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/assign/filter/lib.php');

class fm_feedback_custom_filter extends local_fm_filter_question {

    protected $customtext;

    function add_to_edit_form(MoodleQuickForm &$mform){
        $mform->addElement('text', 'customtext', get_string('customfeedback', 'local_fm'), array('size' => 25));
        $mform->setType('customtext', PARAM_TEXT);
        $mform->setDefault('customtext', $this->customtext);
    }

    function filter(question_attempt $attempt){
        $assignments = $this->manager->get_attempt_assignments($attempt);

        $customtexts = array();
        if ($assignments->has_feedback_custom('current')) {
            $custom = $assignments->get_feedback_custom('current');
            $custom = reset($custom);
            $customtexts[] = $custom->get_text();
        }
        if ($assignments->has_feedback_custom('sent')) {
            foreach($assignments->get_feedback_custom('sent') as $custom){
                $customtexts[] = $custom->get_text();
            }
        }

        foreach ($customtexts as $text) {
            if ((stristr($text, $this->customtext) !== false)) {
                return false;
            }
        }

        return true;
    }

    function get_display_label(){
        return $this->get_name()." contains '$this->customtext'";
    }

    function get_filter_data(){
        $filterdata = parent::get_filter_data();
        $filterdata['customtext'] = $this->customtext;

        return $filterdata;
    }

    function get_type(){
        return 'feedback_custom';
    }

    function update_from_edit_form(stdClass $formdata){
        $this->customtext = $formdata->customtext;
    }
}