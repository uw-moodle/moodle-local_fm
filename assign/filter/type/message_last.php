<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/assign/filter/lib.php');

class fm_message_last_filter extends local_fm_filter_user {

    protected $range;
    protected $time;

    function add_to_edit_form(MoodleQuickForm &$mform){
        $options = array('before' => get_string('timebefore', 'local_fm'), 'after' => get_string('timeafter', 'local_fm'));
        $mform->addElement('select', 'range', get_string('timerange', 'local_fm'), $options);
        $mform->setDefault('range', $this->range);

        $mform->addElement('date_time_selector', 'time', get_string('time'));
        $mform->setDefault('time', $this->time);
    }

    function filter(stdClass $user){
        global $DB;

        $lasttime = (int)$DB->get_field('local_fm_messages', 'MAX(time)', array('useridto' => $user->id));

        switch($this->range){
            case 'before':
                return !($lasttime < $this->time);
            case 'after':
                return !($lasttime > $this->time);
        }

        return false;
    }

    function get_display_label(){
        switch ($this->range) {
            case 'before':
                return get_string('lastmessagebefore', 'local_fm', userdate($this->time));
            case 'after':
                return get_string('lastmessageafter', 'local_fm', userdate($this->time));
        }
        return 'Unknown date specification.';  // Shouldn't happen
    }

    function get_filter_data(){
        $filterdata = parent::get_filter_data();
        $filterdata['time']  = $this->time;
        $filterdata['range'] = $this->range;

        return $filterdata;
    }

    function get_type(){
        return 'message_last';
    }

    function update_from_edit_form(stdClass $formdata){
        $this->time  = $formdata->time;
        $this->range = $formdata->range;
    }
}