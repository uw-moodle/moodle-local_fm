<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/assign/filter/lib.php');

class fm_feedback_filter extends local_fm_filter_question {

    protected $fbinstanceid;

    function add_to_edit_form(MoodleQuickForm &$mform){
        $menu = $this->manager->get_feedback_menu();
        $options = array(
            get_string('nofeedback', 'local_fm')   => array(0 => get_string('nofeedbackassigned', 'local_fm')),
            get_string('allquestions', 'local_fm') => $menu['context'],
            get_string('thisquestion', 'local_fm') => $menu['question'],
        );
        $mform->addElement('selectgroups', 'fbinstanceid', get_string('feedback', 'local_fm'), $options, null, true);
        if (isset($this->fbinstanceid)) {
            $mform->setDefault('fbinstanceid', $this->fbinstanceid);
        }
    }

    function filter(question_attempt $attempt){
        $assignments = $this->manager->get_attempt_assignments($attempt);

        if ($this->has_feedback_instance()) {
            return !$assignments->has_feedback_instance($this->fbinstanceid);
        } else {
            $instances = $assignments->get_feedback_instances();
            return !empty($instances);
        }
    }

    function get_display_label(){
        if ($this->has_feedback_instance()) {
            $fbinstance = local_fm_feedback_instance::get($this->fbinstanceid);
            $name = $fbinstance->get_name();
            $text = get_string('assignedfeedbackis', 'local_fm', $name);;
        } else {
            $text = get_string('nofeedbackassigned', 'local_fm');
        }

        return $text;
    }

    function get_filter_data(){
        $filterdata = parent::get_filter_data();
        $filterdata['fbinstanceid'] = $this->fbinstanceid;

        return $filterdata;
    }

    function get_type(){
        return 'feedback';
    }

    function has_feedback_instance(){
        return !empty($this->fbinstanceid);
    }

    function update_from_edit_form(stdClass $formdata){
        $this->fbinstanceid = $formdata->fbinstanceid;
    }
}