<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/assign/filter/lib.php');

class fm_message_count_filter extends local_fm_filter_user {
    protected $operators = array(
        'lt' => '<',
        'le' => '<=',
        'eq' => '=',
        'ge' => '>=',
        'gt' => '>',
    );
    protected $operator;
    protected $countval;

    function add_to_edit_form(MoodleQuickForm &$mform){
        $options = array(
            'lt' => '<',
            'le' => '<=',
            'eq' => '=',
            'ge' => '>=',
            'gt' => '>',
        );
        $opselect = &$mform->createElement('select', 'operator', '', $options);
        $countval = &$mform->createElement('text', 'countval', '', array('size' => 2));

        $groupels = array($opselect, $countval);
        $mform->addGroup($groupels, null, get_string('messagecount', 'local_fm'), array(' '), false);
        $mform->setDefault('operator', $this->operator);
        $mform->setDefault('countval', $this->countval);
        $mform->setType('countval', PARAM_INT);
    }

    function filter(stdClass $user){
        global $DB;

        $count = $DB->count_records('local_fm_messages', array('useridto' => $user->id));
        switch($this->operator){
            case 'lt':
                return !($count < $this->countval);
            case 'le':
                return !($count <= $this->countval);
            case 'eq':
                return !($count == $this->countval);
            case 'ge':
                return !($count >= $this->countval);
            case 'gt':
                return !($count > $this->countval);
        }
    }

    function get_display_label(){
        return get_string('messagecount', 'local_fm').' '.$this->get_operator().' '.$this->countval;
    }

    function get_filter_data(){
        $filterdata = parent::get_filter_data();
        $filterdata['operator'] = $this->operator;
        $filterdata['countval'] = $this->countval;

        return $filterdata;
    }

    function get_type(){
        return 'message_count';
    }

    function get_operator(){
        return $this->operators[$this->operator];
    }

    function update_from_edit_form(stdClass $formdata){
        $this->operator = $formdata->operator;
        $this->countval = $formdata->countval;
    }

}