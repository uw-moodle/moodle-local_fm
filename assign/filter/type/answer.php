<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/assign/filter/lib.php');

class fm_answer_filter extends local_fm_filter_question {

    protected $search;

    function add_to_edit_form(MoodleQuickForm &$mform){
        $mform->addElement('text', 'search', get_string('answercontainsprompt', 'local_fm'), array('size' => 30));
        $mform->setDefault('search', $this->search);
        $mform->SetType('search', PARAM_TEXT);
    }

    function filter(question_attempt $attempt){
        $answer = $attempt->get_response_summary();
        return (stristr($answer, $this->search) === false);
    }

    function get_display_label(){
        return get_string('answercontains', 'local_fm', $this->search);
    }

    function get_filter_data(){
        $filterdata = parent::get_filter_data();
        $filterdata['search'] = $this->search;

        return $filterdata;
    }

    function get_type(){
        return 'answer';
    }

    function update_from_edit_form(stdClass $formdata){
        $this->search = $formdata->search;
    }
}