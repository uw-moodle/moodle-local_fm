<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

abstract class local_fm_filter {

    public $id;

    /**
     * Feedback manager
     * @var local_fm_manager
     */
    protected $manager;
    /**
     * Url to edit this filter
     * @var moodle_url
     */
    protected $_editurl;

    function __construct(local_fm_manager $manager){
        $this->manager = $manager;
    }

    abstract function add_to_edit_form(MoodleQuickForm &$mform);

    function display(){
        $attr = array('class' => 'commands');
        $commands = html_writer::tag('span', implode(' ', $this->display_commands()), $attr);

        $attr = array('class' => 'title');
        $label = html_writer::tag('span', $this->get_display_label(), $attr);

        $attr = array('class' => 'filter_display');
        return html_writer::tag('div', "$commands $label", $attr);
    }

    protected function display_commands(){
        global $OUTPUT;

        $url = $this->get_editurl();

        $commands = array();
        //TODO: Capabilities
        $editurl = clone($url);
        $commands[] = $OUTPUT->action_icon($editurl, new pix_icon('t/edit', get_string('edit')));
        $editurl = clone($url);
        $editurl->param('action', 'delete');
        $commands[] = $OUTPUT->action_icon($editurl, new pix_icon('t/delete', get_string('delete')));

        return $commands;
    }

    function exists(){
        return isset($this->id);
    }

    function get_display_label(){
        return $this->get_name();
    }

    function get_editurl(){
        global $PAGE;

        if (!isset($this->_editurl)) {
            $params = array(
                'id'        => $this->id,
                'type'      => $this->manager->get_typename(),
                'contextid' => $this->manager->get_context()->id,
                'return'    => $PAGE->url,
            );
            $this->_editurl = new moodle_url('/local/fm/assign/filter/edit.php', $params);
        }

        return $this->_editurl;
    }

    function get_filter_data(){
        $filterdata = array();
        $filterdata['id']      = $this->id;
        $filterdata['type']    = $this->get_type();
        $filterdata['editurl'] = $this->get_editurl()->out();
        $filterdata['context'] = $this->manager->get_context()->id;
        //TODO: $filterdata['question'] = '';
        $filterdata['persist'] = $this->persist();

        return $filterdata;
    }

    function get_name(){
        return get_string('filter_'.$this->get_type(), 'local_fm');
    }

    abstract function get_type();

    function persist(){
        return false;
    }

    function set_data(array $filterdata){
        if ($filterdata['type'] != $this->get_type()) {
            return;
        }

        foreach($filterdata as $key => $val){
            $this->$key = $val;
        }
    }

    function set_editurl(moodle_url $editurl){
        $this->_editurl = $editurl;
    }

    abstract function update_from_edit_form(stdClass $formdata);

}

abstract class local_fm_filter_user extends local_fm_filter {

    /**
     * Whether this user should be filtered.
     * @param stdClass $user
     * @return bool
     */
    abstract function filter(stdClass $user);

    function persist(){
        return true;
    }
}

abstract class local_fm_filter_question extends local_fm_filter {

    /**
     * Whether this question attempt should be filtered.
     * @param question_attempt $attempt
     * @return bool
     */
    abstract function filter(question_attempt $attempt);
}