<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->dirroot.'/local/fm/assign/lib.php');

class local_fm_assign_ajax_controller {
    /**
     * Feedback manager
     * @var local_fm_manager
     */
    protected $manager;
    protected $renderer;
    protected $changes = array();

    public $formid  = 'feedback_assign';

    function __construct(local_fm_manager $manager){
        global $PAGE;
        $this->formid  = 'feedback_assign';
        $this->manager = $manager;
        $this->renderer = $manager->get_page_renderer($PAGE);
    }

    public function cell_changed($htmlid, $contents) {
        // Newer changes overwrite older changes
        $this->changes[$htmlid] = $contents;
    }

    public function get_ajax_response() {
        return $this->changes;
    }

    public function process_submission(){
        // Main table form submission
        $questionid = $this->manager->get_question()->id;

        // Feedback assignments
        $assigns = array_filter(optional_param_array('assignfeedback', array(), PARAM_INT));
        if (!empty($assigns)) {
            unset($assigns['selected']);
            foreach($assigns as $qaid => $fbinstanceid){
                $attempt = $this->get_question_attempt($qaid, $questionid);
                $this->assign_feedback($attempt, $fbinstanceid);
            }
        }

        // Custom feedback
        $submit =  array_filter(optional_param_array('addfeedback', array(), PARAM_BOOL));
        if (!empty($submit)) {
            $custom = optional_param_array('customfeedback', array(), PARAM_TEXT);
            if (!empty($custom)) {
                foreach($submit as $qaid => $submitted){
                    $attempt = $this->get_question_attempt($qaid, $questionid);
                    $this->submit_feedback_custom($attempt, $custom[$qaid]);
                }
            }
        }

        // Grades
        $submit = array_filter(optional_param_array('submitgrade', array(), PARAM_BOOL));
        if (!empty($submit)) {
            // Use PARAM_RAW_TRIMMED since PARAM_FLOAT would do type conversions.  We check later to see if it's a
            // valid number.
            $grades = optional_param_array('grade', array(), PARAM_RAW_TRIMMED);
            if (!empty($grades)) {
                unset($grades['selected']);
                foreach($submit as $qaid => $submitted){
                    $grade = isset($grades[$qaid]) ? $grades[$qaid] : 0;
                    $attempt = $this->get_question_attempt($qaid, $questionid);
                    $this->submit_grade($attempt, $grade);
                }
            }
        }

        // Feedback component submission
        $fbaid  = optional_param('fbaid', 0, PARAM_INT);
        $action = optional_param('action', '', PARAM_ALPHA);
        if ($action && $fbaid) {
            $object = local_fm_feedback_assign::get($fbaid);
            $attempt = $this->get_question_attempt($object->get_db_record()->questionattemptid, $questionid);
            switch($action){
                case 'typical':
                    $switch = !$object->is_typical();
                    $object->mark_typical($switch);
                    break;
                case 'delete':
                    $object->delete();
                    break;
                case 'movedown':
                    $object->move_down();
                    break;
                case 'moveup':
                    $object->move_up();
                    break;
            }
            $cellcontents = $this->renderer->render_cell_feedback($attempt, $this->manager, $this->formid);
            $this->cell_changed(local_fm_assign_table::get_cell_feedback_htmlid($attempt), $cellcontents);
        }
    }

    /**
     * Get a single question attempt and verify that it belongs to this context.
     *
     * We duplicate this code here for efficiency since the local_fm_manager routines load all attempts.
     *
     * @param integer $attemptid
     * @param integer $questionid
     * @throws moodle_exception
     * @return question_attempt
     */
    protected function get_question_attempt($attemptid, $questionid) {
        global $DB;
        $dm = new question_engine_data_mapper();
        $attempt = $dm->load_question_attempt($attemptid);
        if (!$attempt) {
            throw new moodle_exception('Invalid question attempt');
        }

        // Validate question id
        $validquestionid = $attempt->get_question()->id == $questionid;

        // Validate context
        $params = array('id'=>$attempt->get_usage_id(), 'contextid'=>$this->manager->get_context()->id);
        $validcontext = $DB->record_exists('question_usages', $params);

        $valid = $validcontext && $validquestionid;
        if (!$valid) {
            throw new moodle_exception('Invalid question attempt');
        }
        return $attempt;
    }

    protected function submit_grade(question_attempt $attempt, $grade){
        $cangrade = $this->manager->get_question()->qtype->is_manual_graded();
        if (!$cangrade) {
            return;
        }
        $this->manager->submit_grade($attempt, $grade);

        //$cellcontent = $this->renderer->render_cell_grade($attempt, $this->manager, $cangrade, $this->formid);
        //$this->cell_changed(local_fm_assign_table::get_cell_grade_htmlid($attempt), $cellcontent);
    }

    protected function assign_feedback(question_attempt $attempt, $fbinstanceid){
        $this->manager->assign_feedback($attempt, $fbinstanceid);
        $cellcontents = $this->renderer->render_cell_feedback($attempt, $this->manager, $this->formid);
        $this->cell_changed(local_fm_assign_table::get_cell_feedback_htmlid($attempt), $cellcontents);
    }

    protected function submit_feedback_custom(question_attempt $attempt, $text){
        $this->manager->assign_feedback_custom($attempt, $text);
        //$cellcontents = $this->renderer->render_cell_custom_feedback($attempt, $this->manager, $this->formid);
        //$this->cell_changed(local_fm_assign_table::get_cell_custom_feedback_htmlid($attempt), $cellcontents);
    }
}

class local_fm_assign_table extends flexible_table implements renderable {
    const PER_PAGE_VAR = 'perpage';
    const PER_PAGE_DEF = 25;
    const PER_PAGE_SESSION_VAR = 'local_fm_pagesize';

    public $formid;
    /**
     * Feedback manager
     * @var local_fm_manager
     */
    protected $manager;
    protected $errors = array();

    /**
     * Create a feedback assignment table instance.
     * @param local_fm_manager $manager
     */
    function __construct(local_fm_manager $manager){
        global $SESSION;
        $uniqueid = 'fm_assign_table_'.$manager->get_context()->id;
        parent::__construct($uniqueid);

        $this->formid  = 'feedback_assign';
        $this->manager = $manager;
        $this->pagesize = optional_param(static::PER_PAGE_VAR, 0 , PARAM_INT);
        // persist the pagesize in the user's session
        if ($this->pagesize) {
            $SESSION->{static::PER_PAGE_SESSION_VAR} = $this->pagesize;
        } else if (!empty($SESSION->{static::PER_PAGE_SESSION_VAR})) {
            $this->pagesize = $SESSION->{static::PER_PAGE_SESSION_VAR};
        } else {
            $this->pagesize = static::PER_PAGE_DEF;
        }
        // $this->currpage is set later on in setup(), but we need to know it earlier when the report queries the page parameters
        $this->currpage = optional_param($this->request[TABLE_VAR_PAGE], 0, PARAM_INT);
    }

    function add_cell_response(question_attempt $attempt, local_fm_renderer $renderer){
        if (!empty($this->sess->collapse['response'])) {
            return '';
        }

        return $renderer->render_cell_response($attempt);
    }

    function add_cell_custom_feedback(question_attempt $attempt, local_fm_renderer $renderer){
        if (!empty($this->sess->collapse['customfb'])) {
            return '';
        }

        $attemptid = $attempt->get_database_id();
        $assignments = $this->manager->get_attempt_assignments($attempt);

        $formdata = $this->get_data_form('custom', $attemptid);
        $hasfeedback = $assignments->has_feedback_custom('current');
        if ($formdata === false) {
            if ($hasfeedback) {
                $current = $assignments->get_feedback_custom('current');
                $current = reset($current);
                $text = $current->get_text();
            } else {
                $text = '';
            }
            $this->store_data_db('custom', $attemptid, $text);
        }
        $cell = $renderer->render_cell_custom_feedback($attempt, $this->manager,
                                                $this->formid, $formdata);
        return html_writer::tag('div', $cell, array('id'=>self::get_cell_custom_feedback_htmlid($attempt)));
    }

    function add_cell_feedback(question_attempt $attempt, local_fm_renderer $renderer){
        if (!empty($this->sess->collapse['feedback'])) {
            return '';
        }
        $attemptid = $attempt->get_database_id();
        $cell = $renderer->render_cell_feedback($attempt, $this->manager, $this->formid);
        return html_writer::tag('div', $cell, array('id'=>self::get_cell_feedback_htmlid($attempt)));
    }

    function add_cell_grade(question_attempt $attempt, local_fm_renderer $renderer, $cangrade){
        if (!empty($this->sess->collapse['grade'])) {
            return '';
        }

        $attemptid = $attempt->get_database_id();
        $formdata = $this->get_data_form('grade', $attemptid);
        if ($formdata === false) {
            $mark = $attempt->format_mark(2);
            $this->store_data_db('grade', $attemptid, $mark);
        }

        $cell = $renderer->render_cell_grade($attempt, $this->manager, $cangrade, $this->formid, $formdata);
        return html_writer::tag('div', $cell, array('id'=>self::get_cell_grade_htmlid($attempt)));
    }

    function add_cell_picture($user, $renderer){
        if (!empty($this->sess->collapse['picture'])) {
            return '';
        }

        return $renderer->render_cell_picture($user);
    }

    function add_cell_select(question_attempt $attempt, local_fm_renderer $renderer){
        if (!empty($this->sess->collapse['select'])) {
            return '';
        }
        return $renderer->render_cell_select($attempt);
    }

    function add_cell_user($group, $attemptuser, local_fm_renderer $renderer){
        if (!empty($this->sess->collapse['user'])) {
            return '';
        }
        return $renderer->render_cell_user($group, $attemptuser);
    }

    function define_baseurl($url = null){
        if (!isset($url)) {
            global $PAGE;

            $url = $PAGE->url;
            $url->param(static::PER_PAGE_VAR, $this->pagesize);
        }
        parent::define_baseurl($url);
    }

    protected function delete_all_data(){
        unset($this->sess->formdata);
    }

    protected function delete_data_form($type, $qaid){
        unset($this->sess->formdata[$type][$qaid]);
        if (empty($this->sess->formdata[$type])) {
            unset($this->sess->formdata[$type]);
        }
    }

    function get_change_notification(){
        global $PAGE, $OUTPUT;

        $url = $PAGE->url;
        $url->param('clearall', true);
        $icon = new pix_icon('t/delete', get_string('delete'));
        $confirm = new confirm_action(get_string('clearallconfirm', 'local_fm'));
        $delete  = $OUTPUT->action_icon($url, $icon, $confirm);

        return get_string('needsave', 'local_fm') . ' ' . $delete;
    }

    function get_context(){
        return $this->manager->get_context();
    }

    function get_component($name){
        $classname = 'local_fm_assign_'.$name.'_component';
        return new $classname($this);
    }

    protected function get_data_by_datatype($datatype) {
        return !empty($this->sess->$datatype) ? $this->sess->$datatype : array();
    }

    protected function get_data_for_qa($datatype, $type, $qaid) {
        $data = $this->get_data_by_datatype($datatype);
        return isset($data[$type][$qaid]) ? $data[$type][$qaid] : false;
    }

    protected function get_data_db($type, $qaid){
        return $this->get_data_for_qa('dbdata', $type, $qaid);
    }

    protected function get_data_form($type, $qaid){
        return $this->get_data_for_qa('formdata', $type, $qaid);
    }

    function get_feedback_instances($questionids = null){
        return $this->manager->get_feedback_instances($questionids);
    }

    function get_user_responses_info_for_display(){
        $filtercomp = $this->get_component('filters');

        // Paging
        $perpage = $this->get_page_size();
        $total   = 0;

        $grouped = array();
        foreach($this->manager->get_assignments(true) as $qaid => $assignments){
            // Question attempt filtering
            $attempt = $assignments->get_question_attempt();
            if ($attempt->get_state()->is_active()) {
                continue;    // Only no longer active attempts
            }
            if ($filtercomp->filter_question_attempt($attempt)) {
                continue;
            }

            // User filtering
            $user = $this->manager->get_user($assignments->get_attempt_userid());
            if (!$user) {
                continue;
            }
            if ($filtercomp->filter_user($user)) {
                continue;
            }

            $grouped[$user->id][$qaid] = $assignments;
            // Complex filtering doesn't allow SQL limits
            $total++;
        }

        $this->pagesize($perpage, $total);

        return $grouped;
    }

    function get_form_entries(){
        return ;
    }

    function get_manager(){
        return $this->manager;
    }

    function get_page_params(){
        $params = array(
            static::PER_PAGE_VAR => $this->pagesize,
            $this->request[TABLE_VAR_PAGE] => $this->currpage,
        );
        $params += $this->get_component('questions')->get_page_params();

        return $params;
    }

    function get_question(){
        return $this->manager->get_question();
    }

    function get_questions(){
        return $this->manager->get_questions();
    }

    /**
     * The current URL for rendering this table (no actions)
     * @return moodle_url
     */
    function get_url(){
        if (!isset($this->baseurl)) {
            $this->define_baseurl();
        }
        $url = clone($this->baseurl);
        $url->params($this->get_page_params());

        return $url;
    }

    function process_submission(){
        $feedback = $this->get_component('feedback');
        $feedback->process_submission();

        if (!optional_param('sesskey', null, PARAM_RAW)) {
            // No session key, so don't process form data.
            // Note that since the data is stored in the session, we may see data presented here
            // and legitimately not have a session key.
            return;
        }
        require_sesskey();

        /* Main table form submission */
        $questionid = $this->get_question()->id;
        $attempts = $this->manager->get_question_attempts($questionid);
        $attempts = $attempts[$questionid];

        // Cache data
        if (optional_param('clearall', false, PARAM_BOOL)) {
            $this->delete_all_data();
        } else {
            // Get form data which the user changed
            $formdata = $this->get_data_by_datatype('formdata');

            // Custom feedback
            if (isset($formdata['custom'])) {
                foreach($formdata['custom'] as $qaid => $text){
                    if (isset($attempts[$qaid])) {
                        $this->submit_feedback_custom($attempts[$qaid], $text);
                    }
                }
            }
            // Grades
            if (isset($formdata['grade'])) {
                foreach($formdata['grade'] as $qaid => $grade){
                    if (isset($attempts[$qaid])) {
                        $this->submit_grade($attempts[$qaid], $grade);
                    }
                }
            }

            $selected = array_filter(optional_param_array('selected', array(), PARAM_INT));

            // Feedback assignments
            $assigns = array_filter(optional_param_array('assignfeedback', array(), PARAM_INT));
            if (!empty($assigns['selected'])) {
                $fbinstanceid = $assigns['selected'];
                foreach($selected as $qaid => $selectval){
                    if (isset($attempts[$qaid])) {
                        $this->assign_feedback($attempts[$qaid], $fbinstanceid);
                    }
                }
            } else if (!empty($assigns)) {
                unset($assigns['selected']);
                foreach($assigns as $qaid => $fbinstanceid){
                    if (isset($attempts[$qaid])) {
                        $this->assign_feedback($attempts[$qaid], $fbinstanceid);
                    }
                }
            }

            // Grades (selected)
            $submitgrade = optional_param_array('submitgrade', array(), PARAM_ALPHA);
            if (!empty($submitgrade['selected'])) {
                $grades = optional_param_array('grade', array(), PARAM_FLOAT);
                foreach($selected as $qaid => $selectval){
                    if (isset($attempts[$qaid])) {
                        $this->submit_grade($attempts[$qaid], $grades['selected']);
                    }
                }
            }
        }
    }

    protected function store_data_db($type, $qaid, $value){
        if (!isset($this->sess->dbdata)) {
            $this->sess->dbdata = array();
        }

        $this->sess->dbdata[$type][$qaid] = $value;
    }

    function submit_grade(question_attempt $attempt, $grade){
        try {
            $this->manager->submit_grade($attempt, $grade);
            $this->delete_data_form('grade', $attempt->get_database_id());
        } catch (local_fm_invalid_input_exception $e) {
            $this->errors[] = get_string('invalidassignsubmission', 'local_fm');
        }
    }

    function assign_feedback(question_attempt $attempt, $fbinstanceid){
        $this->manager->assign_feedback($attempt, $fbinstanceid);
    }

    function submit_feedback_custom(question_attempt $attempt, $text){
        $this->manager->assign_feedback_custom($attempt, $text);
        $this->delete_data_form('custom', $attempt->get_database_id());
    }

    protected function sync_data_form($type, $submitdata){
        if (!empty($submitdata)) {
            foreach($submitdata as $qaid => $submitvalue) {
                $dbvalue = $this->sess->dbdata[$type][$qaid];
                if ($submitvalue !== $dbvalue) {
                    $this->sess->formdata[$type][$qaid] = $submitvalue;
                } else {
                    $this->delete_data_form($type, $qaid);
                }
            }
        }
    }

    protected function refresh_submit_data(){
        if (!isset($this->sess->dbdata)) {
            return;    // First view in session - no changes
        }
        if (!isset($this->sess->formdata)) {
            $this->sess->formdata = array();
        }

        $custom = optional_param_array('customfeedback', array(), PARAM_TEXT);
        unset($custom['selected']);
        if (!empty($custom)) {
            require_sesskey();
            $this->sync_data_form('custom', $custom);
        }

        $grades = optional_param_array('grade', array(), PARAM_TEXT);
        unset($grades['selected']);
        if (!empty($grades)) {
            require_sesskey();
            $this->sync_data_form('grade', $grades);
        }
    }

    function requires_save(){
        return !empty($this->sess->formdata);
    }

    function set_data(renderer_base $renderer){
        global $OUTPUT;

        $assignments = $this->get_user_responses_info_for_display();
        if (empty($assignments)) {
            return;    // Nothing to display
        }

        $cangrade = $this->get_question()->qtype->is_manual_graded();
        $pixoptions = array();
        $context = $this->get_context();
        if ($context instanceof context_course) {
            $pixoptions['courseid'] = $context->instanceid;
        }

        $perpage   = $this->pagesize;
        $pagestart = $this->currpage * $perpage + 1;
        $pagestop  = $pagestart + $perpage;

        $attemptnum = 0;
        foreach($assignments as $uassignments){
            foreach($uassignments as $assignment) {
                $attemptnum++;
                if (($attemptnum < $pagestart) || ($attemptnum >= $pagestop)) {
                    continue;
                }

                $attempt   = $assignment->get_question_attempt();
                $attemptid = $attempt->get_database_id();
                $group     = $this->manager->get_groupmanager()->get_attempt_group($attempt);
                $attemptuser = $this->manager->get_user($assignment->get_attempt_userid());

                $row = array();
                $row[] = $this->add_cell_picture($group, $renderer);
                $row[] = $this->add_cell_user($group, $attemptuser, $renderer);
                $row[] = $this->add_cell_response($attempt, $renderer);
                $row[] = $this->add_cell_custom_feedback($attempt, $renderer);
                $row[] = $this->add_cell_feedback($attempt, $renderer);
                $row[] = $this->add_cell_grade($attempt, $renderer, $cangrade);
                $row[] = $this->add_cell_select($attempt, $renderer);

                $this->add_data($row);
            }
        }

        $selectedCount = 0;    //TODO
        $helpicon = $OUTPUT->help_icon('actionwithselected', 'local_fm');
        $desc = $OUTPUT->heading(get_string('actionwithselected', 'local_fm').' '.$helpicon, 4);
        //$desc .= html_writer::tag('p', "Selected: ".html_writer::tag('span', $selectedCount));

        $menu = $this->manager->get_feedback_menu();
        $options = array(
                array(get_string('allquestions', 'local_fm') => $menu['context']),
                array(get_string('thisquestion', 'local_fm') => $menu['question']),
        );
        $nothing = array(null => '');
        $factions = html_writer::select($options, "assignfeedback[selected]", null, $nothing, array('class'=>'noajax'));
        $go = new local_fm_button($this->formid, 'submit', get_string('assign', 'local_fm'), 'inline');
        $factions .= $renderer->render($go);

        $gactions = '';
        if ($cangrade) {
            $attr = array('type' => 'text', 'name' => 'grade[selected]', 'size' => 3, 'class'=>'noajax');
            $gactions .= html_writer::empty_tag('input', $attr);
            $button = new local_fm_button($this->formid, 'submitgrade[selected]', get_string('assign', 'local_fm'), 'inline');
            $gactions .= $renderer->render($button);
        }

        $this->add_data(array('', '', $desc, '', $factions, $gactions, ''));
    }

    function setup(){
        global $OUTPUT;

        //$this->sortable(true); TODO: Add sorting
        $this->collapsible(true);
        $this->pageable(true);

        $columns = array(
            'picture'    => '&nbsp;',
            'fullname'   => get_string('name'),
            'response'   => get_string('answer'),
            'customfb'   => get_string('customfeedback', 'local_fm').$OUTPUT->help_icon('customfeedback', 'local_fm'),
            'feedback'   => get_string('feedback', 'local_fm').$OUTPUT->help_icon('feedback', 'local_fm'),
            'grade'      => get_string('grade').$OUTPUT->help_icon('grade', 'local_fm'),
            'select'     => get_string('select').$OUTPUT->help_icon('select', 'local_fm'),
        );

        $this->define_baseurl();
        $this->define_columns(array_keys($columns));
        $this->define_headers(array_values($columns));
        foreach(array_keys($columns) as $column){
            $this->column_class($column, 'col_'.$column);
        }

        //$this->column_suppress('picture');
        //$this->column_suppress('fullname');

//         $this->no_sorting('response');
//         $this->no_sorting('customfb');
//         $this->no_sorting('feedback');
//         $this->no_sorting('select');

        parent::setup();

        $collapsed = array_filter($this->sess->collapse);
        $classes = 'fm_assign_table';
        if (empty($collapsed)) {
            $classes .= ' fixed';
        }
        $this->set_attribute('class', $classes);
        $this->set_attribute('cellspacing', '0');

        $this->refresh_submit_data();
    }

    function get_errors() {
        return $this->errors;
    }

    static function get_cell_custom_feedback_htmlid($attempt) {
        return 'cell_custom_feedback_'.$attempt->get_database_id();
    }

    static function get_cell_feedback_htmlid($attempt) {
        return 'cell_feedback_'.$attempt->get_database_id();
    }

    static function get_cell_grade_htmlid($attempt) {
        return 'cell_gradek_'.$attempt->get_database_id();
    }
}

class local_fm_assign_component implements renderable {

    protected $table;

    function __construct(local_fm_assign_table $table){
        $this->table = $table;
    }

    function get_page_params(){
        return array();
    }

    // TODO: Expand components to load up, be tested for params, and function with table
    function has_page_params(){
        return false;
    }
}

class local_fm_assign_filters_component extends local_fm_assign_component {

    protected $filterdata;
    protected $_filters;

    function _load_filter_data(){
        if (!$this->table->setup) {
            $this->table->setup();
        }
        if (!isset($this->table->sess->filter)) {
            $this->table->sess->filter = array();
        }

        $this->filterdata =& $this->table->sess->filter;

        return $this->filterdata;
    }

    function create_filter($type){
        global $CFG;

        $dirroot = $this->get_filter_dir();
        $filename = $type.'.php';

        require_once("$dirroot/$filename");

        $classname = 'fm_'.$type.'_filter';
        return new $classname($this->table->get_manager());
    }

    function filter_question_attempt(question_attempt $attempt){
        $filters = $this->get_filters('question');

        foreach($filters as $qfilter){
            if ($qfilter->filter($attempt)) {
                return true;
            }
        }

        return false;
    }

    function filter_user($user){
        $filters = $this->get_filters('user');

        foreach($filters as $ufilter){
            if ($ufilter->filter($user)) {
                return true;
            }
        }

        return false;
    }

    function filter_update(local_fm_filter $filter, stdClass $formdata){
        $filterdata = $this->_load_filter_data();

        if (!isset($filter->id)) {
            if (empty($filterdata)) {
                $filter->id = 1;
            } else {
                end($filterdata);
                $filter->id = key($filterdata) + 1;
            }
        }

        $filter->update_from_edit_form($formdata);

        $this->filterdata[$filter->id] = $filter->get_filter_data();
    }

    function filter_delete(local_fm_filter $filter){
        $this->_load_filter_data();

        unset($this->filterdata[$filter->id]);
    }

    function get_filter($id){
        $allfilterdata = $this->_load_filter_data();

        $filterdata = $allfilterdata[$id];
        $filter = $this->create_filter($filterdata['type']);
        $filter->set_data($filterdata);

        return $filter;
    }

    function get_filter_dir(){
        global $CFG;
        return $CFG->dirroot.'/local/fm/assign/filter/type';
    }

    function get_filter_options(){
        $options = array();
        $dir = dir($this->get_filter_dir());
        while (false !== ($entry = $dir->read())) {
           if (is_dir($entry) || strstr($entry, 'filter_')) {
               continue;
           }
           $type = basename($entry, '.php');
           $options[$type] = $this->create_filter($type);
        }

        $dir->close();

        return $options;
    }

    function get_filter_select(){
        global $PAGE;

        $filteroptions = $this->get_filter_options();

        $ufilters = array();
        $qfilters = array();
        foreach($filteroptions as $filtertype => $filter){
            $filtername = $filter->get_name();
            if ($filter instanceof local_fm_filter_user) {
                $ufilters[$filtertype] = $filtername;
            } else if ($filter instanceof local_fm_filter_question) {
                $qfilters[$filtertype] = $filtername;
            }
        }
        asort($ufilters);
        asort($qfilters);

        $options = array(
                array(get_string('user') => $ufilters),
                array(get_string('question') => $qfilters),
        );

        return new single_select($PAGE->url, 'filtertype', $options);
    }

    function get_filters($type = null){
        if (!isset($this->_filters)) {
            $filterdata = $this->_load_filter_data();

            $this->_filters = array();
            foreach($filterdata as $data){
                if (!isset($data['type'])) {
                    continue;
                }
                $filter = $this->create_filter($data['type']);
                $filter->set_data($data);

                if ($filter instanceof local_fm_filter_user) {
                    $this->_filters['user'][$filter->id] = $filter;
                } else if ($filter instanceof local_fm_filter_question) {
                    $this->_filters['question'][$filter->id] = $filter;
                }
            }
        }

        if (isset($type)) {
            return !empty($this->_filters[$type]) ? $this->_filters[$type] : array();
        }

        return $this->_filters;
    }

    function get_url(){
        if (!isset($this->filterurl)) {
            $params = array(
                'type'      => $this->table->get_manager()->get_typename(),
                'contextid' => $this->table->get_context()->id,
                'return'    => $this->table->get_url(),
            );
            $this->filterurl = new moodle_url('/local/fm/assign/filter/edit.php', $params);
        }

        return $this->filterurl;
    }
}

class local_fm_assign_feedback_component extends local_fm_assign_component {

    function get_feedback_instances(){
        return $this->table->get_manager()->get_feedback_instances(true);
    }

    function get_url(){
        $question = $this->table->get_question();

        $params = array(
            'type'     => $this->table->get_manager()->get_typename(),
            'context'  => $this->table->get_context()->id,
            'return'   => $this->table->get_url(),
            'question' => $question->id,
        );

        return new moodle_url('/local/fm/assign/instances.php', $params);
    }

    function process_submission(){
        $fbaid  = optional_param('fbaid', 0, PARAM_INT);
        $fbiid  = optional_param('fbiid', 0, PARAM_INT);
        $action = optional_param('action', '', PARAM_ALPHA);
        $addtoquiz = optional_param('addtoquiz', 0, PARAM_BOOL);

        $manager = $this->table->get_manager();

        if ($action && ($fbaid || $fbiid)) {
            // Assign or instance adjustments
            require_sesskey();
            try {
                if ($fbaid) {
                    $object = local_fm_feedback_assign::get($fbaid);
                    // Validate attemptid.
                    // The following fetches all attempts, but we'll need them later anyhow.
                    $attempts = $manager->get_question_attempts(true);
                    $attempts = reset($attempts); // For current question.
                    if (!isset($attempts[$object->get_db_record()->questionattemptid])) {
                        throw new moodle_exception('invalidattempt');
                    }
                } else if ($fbiid) {
                    $object = local_fm_feedback_instance::get($fbiid);
                    // Validate instance context.
                    if ($object->get_context() != $manager->get_context()) {
                        throw new moodle_exception('invalidinstance');
                    }
                }
            } catch(Exception $e) {
                return false;
            }

            switch($action){
                case 'typical':
                    $switch = !$object->is_typical();
                    $object->mark_typical($switch);
                    break;
                case 'delete':
                    $object->delete();
                    break;
                case 'movedown':
                    $object->move_down();
                    break;
                case 'moveup':
                    $object->move_up();
                    break;
            }

        } else if (optional_param('addnew', false, PARAM_BOOL)) {
            require_sesskey();
            $manager = $this->table->get_manager();
            // Adding new feedback linked to question
            global $USER;
            $defaultcategory = $manager->get_default_category();

            $newdef = new stdClass();
            $newdef->categoryid = $defaultcategory->id;
            $newdef->name       = optional_param('name', '', PARAM_TEXT);
            $newdef->text       = optional_param('text', '', PARAM_TEXT);
            $newdef->createdby  = $USER->id;
            $newdef->context    = $manager->get_context()->id;
            $feedback = local_fm_feedback::create($newdef);
            $feedback->save();

            if ($addtoquiz) {
                $manager->add_feedback_instance($feedback->id, null);
            } else {
                $manager->add_feedback_instance($feedback->id, $manager->get_question()->id);
            }
        }

        return true;
    }

}

class local_fm_assign_questions_component extends local_fm_assign_component {
    const PARAM_NAME = 'qid';

    function get_page_params(){
        return array(self::PARAM_NAME => $this->table->get_question()->id);
    }

    function get_question_select(){
        global $PAGE;

        $questions = $this->table->get_questions();
        $question  = $this->table->get_question();

        $quizquests = array();
        $url = $this->table->get_url();
        $url->remove_params($this->table->request[TABLE_VAR_PAGE]);
        foreach($questions as $id => $quest){
            $quizquests[$url->out(false, array('qid' => $id))] = $quest->name;
        }
        $selected = $url->out(false, array('qid' => $question->id));
        $select = new url_select($quizquests, $selected, null);
        $select->set_label(get_string('question').': ');

        return $select;
    }

    function format_question_text(){
        $question = $this->table->get_question();
        $options = array(
            'para'        => false,
            'context'     => $this->table->get_context(),
            'overflowdiv' => true
        );
        $text = question_rewrite_question_urls($question->questiontext, 'pluginfile.php',
                $this->table->get_context()->id, 'question', 'questiontext_preview', array('question'), $question->id);
        return format_text($text, $question->questiontextformat, $options);
    }

}

class local_fm_feedback_configure implements renderable {
    const SELECT_CONTEXT  = 1;
    const SELECT_NONE     = 0;
    const SELECT_QUESTION = 2;
    const CONTEXT_PARAM  = 'fbcontext';
    const FEEDBACK_PARAM = 'fbid';

    /**
     * Create a context tab.
     * @param context $context
     * @param unknown_type $url
     * @return tabobject
     */
    protected static function get_context_tab(context $context, moodle_url $url){
        $link = $url->out(false, array(self::CONTEXT_PARAM => $context->id));

        switch($context->contextlevel){
            case CONTEXT_SYSTEM:
                return new tabobject($context->id, $link, get_string('site'));
            case CONTEXT_COURSECAT:
                return new tabobject($context->id, $link, $context->get_context_name());
            case CONTEXT_COURSE:
                return new tabobject($context->id, $link, get_string('course'));
            case CONTEXT_MODULE:
                return new tabobject($context->id, $link, get_string('module', 'local_fm'));
            case CONTEXT_BLOCK:
                return new tabobject($context->id, $link, get_string('block'));
            case CONTEXT_USER:
                return new tabobject($context->id, $link, get_string('user'));
        }
    }

    /**
     * Table form id
     * @var string
     */
    public $formid;
    /**
     * Feedback manager defining configuration context
     * @var local_fm_manager
     */
    protected $manager;

    /**
     * Create a feedback instance configuration table.
     * @param local_fm_manager $manager
     * @param int $questionid (optional) question id
     */
    function __construct(local_fm_manager $manager){
        $this->formid = 'feedback_instances';
        $this->manager = $manager;
    }

    function verify_access() {
        global $USER;
        $context = $this->get_context_view();

        if ($context instanceof context_user && $context->instanceid == $USER->id) {
            require_capability('local/fm:manageownfeedback', context_system::instance());
        } else {
            require_capability('local/fm:viewfeedbackbank', $context);
        }
    }

    function can_manage_feedback() {
        global $USER;
        $context = $this->get_context_view();

        if ($context instanceof context_user && $context->instanceid == $USER->id) {
            return has_capability('local/fm:manageownfeedback', context_system::instance());
        }

        return has_capability('local/fm:manage', $context);
    }

    function get_context_config(){
        return $this->manager->get_context();
    }

    function get_context_view(){
        global $PAGE;

        $contextid = optional_param(self::CONTEXT_PARAM, null, PARAM_INT);
        if ($contextid) {
            $context = context::instance_by_id($contextid);
        } else {
            $context = $PAGE->context;
        }

        return $context;
    }

    function get_feedback_categories() {
        return local_fm_category::get_options_menu($this->get_context_view());
    }

    function get_feedback_for_context(){
        return local_fm_feedback::get_all_in_context($this->get_context_view());
    }

    function get_page_params(){
        $params = array();
        $params[self::CONTEXT_PARAM] = $this->get_context_view()->id;
        return $params;
    }

    function get_selected(){
        $select = optional_param_array('select', array(), PARAM_INT);

        $cselect = array();
        $qselect = array();
        foreach($select as $instanceid => $selectval) {
            switch($selectval){
                case static::SELECT_NONE:
                    continue;
                case static::SELECT_CONTEXT:
                    $cselect[] = $instanceid;
                    break;
                case static::SELECT_QUESTION:
                    $qselect[] = $instanceid;
                    break;
            }
        }

        // Since we're not using the form api, we need to validate selections here.

        // Get all feedback organized by category
        $allfeedback = $this->get_feedback_for_context();
        // Flatten array
        $feedbackids = array();
        foreach ($allfeedback as $catid => $feedback) {
            $feedbackids = array_merge($feedbackids, array_keys($feedback));
        }

        // Filter selections
        $qselect = array_intersect($qselect, $feedbackids);
        $cselect = array_intersect($cselect, $feedbackids);

        return array($cselect, $qselect);
    }

    function get_url_current(){
        global $PAGE;

        $url = $PAGE->url;
        $url->param(self::CONTEXT_PARAM, $this->get_context_view()->id);
        return $url;
    }

    function get_url_edit($feedback){
        global $PAGE;

        $editurl = new moodle_url('/local/fm/bank/feedback.php');
        $editurl->params(array('id' => $feedback->id, 'return' => $PAGE->url));

        return $editurl;
    }

    function get_url_return(){
        return new moodle_url(required_param('return', PARAM_URL));
    }

    function print_nav_tabs(moodle_page $page = null){
        global $PAGE, $USER;

        if (!isset($page)) {
            $page = $PAGE;
        }

        $manager = $this->manager;
        $url = $page->url;
        $contexts = $page->context->get_parent_contexts();
        $usercontext = context_user::instance($USER->id);

        $currenttab = static::get_context_tab($this->get_context_view(), $url);
        $currtabid = $currenttab->id;

        $row = array();
        $row[] = static::get_context_tab($usercontext, $url);
        $row[] = static::get_context_tab($page->context, $url);
        foreach($contexts as $context){
            if (has_capability('local/fm:viewfeedbackbank', $context)) {
                $row[] = static::get_context_tab($context, $url);
            }
        }

        print_tabs(array($row), $currtabid);
    }

    function was_cancelled(){
        return optional_param('cancel', false, PARAM_BOOL);
    }

    function was_saved(){
        return optional_param('save', false, PARAM_BOOL);
    }

    function get_instance_tables() {
        global $USER;

        $allfeedback = $this->get_feedback_for_context();
        $feedbackcats = $this->get_feedback_categories();

        $tables = array();

        foreach ($feedbackcats as $catid => $catname) {
            if (!empty($allfeedback[$catid])) {
                $catfeedback = $allfeedback[$catid];
            } else {
                $catfeedback = array();
            }

            $table = new local_fm_feedback_instance_table($this->manager, $this->formid, $catname);
            $table->set_data($catfeedback);

            $tables[] = $table;
        }
      return $tables;
    }
}

class local_fm_feedback_instance_table extends html_table implements renderable {

    /**
     * Table form id
     * @var string
     */
    public $formid;
    /**
     * Feedback manager defining configuration context
     * @var local_fm_manager
     */
    protected $manager;
    /**
     * Feedback category name
     * @var string
     */
    public $catname;

    /**
     * Create a feedback instance configuration table.
     * @param local_fm_manager $manager
     * @param int $questionid (optional) question id
     */
    function __construct(local_fm_manager $manager, $formid, $catname){
        $this->formid = $formid;
        $this->manager = $manager;
        $this->catname = $catname;
    }

    function add_cell_select_context(local_fm_feedback $feedback){
        $checked  = $this->manager->has_instance_of_feedback($feedback->id);
        $attr = array(
                'type'  => 'radio',
                'name'  => "select[$feedback->id]",
                'value' => local_fm_feedback_configure::SELECT_CONTEXT,
        );
        if ($checked) {
            $attr['checked'] = 'checked';
        }
        $radio = html_writer::empty_tag('input', $attr);

        return new html_table_cell($radio);
    }

    function add_cell_select_question(local_fm_feedback $feedback){
        $submitname = "select[$feedback->id]";
        $submitval  = local_fm_feedback_configure::SELECT_QUESTION;
        $checked  = $this->manager->has_instance_of_feedback($feedback->id, true);

        $attr = array(
                'type'  => 'radio',
                'name'  => $submitname,
                'value' => $submitval,
        );
        if ($checked) {
            $attr['checked'] = 'checked';
        }
        $html = html_writer::empty_tag('input', $attr);

        return new html_table_cell($html);
    }

    function add_cell_select_none(local_fm_feedback $feedback){
        $checked = !$this->manager->has_instance_of_feedback($feedback->id, null) &&
        !$this->manager->has_instance_of_feedback($feedback->id, true);
        $attr = array(
                'type'  => 'radio',
                'name'  => "select[$feedback->id]",
                'value' => local_fm_feedback_configure::SELECT_NONE,
        );
        if ($checked) {
            $attr['checked'] = 'checked';
        }
        $radio = html_writer::empty_tag('input', $attr);

        return new html_table_cell($radio);
    }

    function add_cell_name(local_fm_feedback $feedback){
        return new html_table_cell($feedback->get_name());
    }

    function add_cell_sent(local_fm_feedback $feedback){
        $wassent = $this->manager->has_instance_of_feedback($feedback->id, null, true) ||
        $this->manager->has_instance_of_feedback($feedback->id, true, true);
        $wassentstr = $wassent ? get_string('yes') : get_string('no');

        return new html_table_cell($wassentstr);
    }

    function add_cell_text(local_fm_feedback $feedback){
        return new html_table_cell($feedback->get_text());
    }

    function add_cell_creator(local_fm_feedback $feedback){
        $user = $feedback->get_user_creator();
        if ($user) {
            return new html_table_cell(fullname($user));
        }
    }

    function add_cell_modifier(local_fm_feedback $feedback){
        $user = $feedback->get_user_modifier();
        if ($user) {
            return new html_table_cell(fullname($user));
        }
    }

    function add_row_feedback(local_fm_feedback $feedback){
        $row = new html_table_row();
        $row->attributes['class'] = 'row_feedback';

        $row->cells[] = $this->add_cell_name($feedback);
        $row->cells[] = $this->add_cell_text($feedback);
        $row->cells[] = $this->add_cell_sent($feedback);
        $row->cells[] = $this->add_cell_select_none($feedback);
        $row->cells[] = $this->add_cell_select_context($feedback);
        $row->cells[] = $this->add_cell_select_question($feedback);
        $row->cells[] = $this->add_cell_creator($feedback);
        $row->cells[] = $this->add_cell_modifier($feedback);

        $this->data[] = $row;
    }

    function get_grouped_feedback($allfeedback){

        $grouped  = array();
        $creators = array();

        // Group feedback by user that created it
        foreach($allfeedback as $id => $feedback){
            $creator = $feedback->get_user_creator();
            if (!isset($creators[$creator->id])) {
                $creators[$creator->id] = $creator;
            }

            $grouped[$creator->id][$id] = $feedback;
        }

        return array($grouped, $creators);
    }

    function set_data($allfeedback){
        global $USER;

        list($feedbackbyuser, $users) = $this->get_grouped_feedback($allfeedback);
        // Show feedback created by current user first
        if (!empty($feedbackbyuser[$USER->id])) {
            foreach($feedbackbyuser[$USER->id] as $feedback){
                $this->add_row_feedback($feedback);
            }
            unset($users[$USER->id]);
        }

        // Other users' feedback
        foreach($users as $userid => $user){
            if (!empty($feedbackbyuser[$userid])) {
                foreach($feedbackbyuser[$userid] as $id => $feedback){
                    $this->add_row_feedback($feedback);
                }
            }
        }
        if (empty($this->data)) {
            $row = new html_table_row();
            $row->attributes['class'] = 'row_category';

            $namecell = new html_table_cell(get_string('nofeedbackincategory', 'local_fm'));
            $namecell->colspan = 8;
            $namecell->attributes = array('class'=>'categoryname');
            $row->cells[] = $namecell;
            $this->data[] = $row;
        }
    }

    function setup(){
        $this->attributes['class'] = 'generaltable fm_feedback_table';

        $this->head  = array();
        $this->head[] = get_string('label', 'local_fm');
        $this->head[] = get_string('text', 'local_fm');
        $this->head[] = get_string('sent', 'local_fm');
        $this->head[] = get_string('unused', 'local_fm');
        $this->head[] = get_string('inallquestions', 'local_fm');
        $this->head[] = get_string('inthisquestion', 'local_fm');
        $this->head[] = get_string('createdby', 'local_fm');
        $this->head[] = get_string('modifiedby', 'local_fm');

        $this->colclasses = array('', '', '', 'controls', 'controls', 'controls', 'username','username');

        //$this->align = array_fill(0, count($this->head), 'center');
    }

    function get_category_name() {
        return $this->catname;
    }
}

