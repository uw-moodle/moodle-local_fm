<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/local/fm/bank/lib.php');

interface local_fm_assigned_feedback {
    function get_text();

    function mark_sent($time);

    function was_sent();

    function is_typical();
}


class local_fm_feedback_assign extends local_fm_sortable implements local_fm_assigned_feedback {
    const DB_TABLE = 'local_fm_feedback_assign';

    static protected $_qacache = array();
    static protected $_maxordercache = array();
    static protected $_usercache = array();

    protected static function _sort_params(stdClass $definition){
        return array(
            'questionattemptid' => $definition->questionattemptid,
            'sent'              => $definition->sent,    // All sent assignments must have same time
        );
    }

    static function count_for_feedback_instance($fbinstanceid){
        global $DB;

        return $DB->count_records(static::DB_TABLE, array('feedbackinstanceid' => $fbinstanceid));
    }

    static function get_all_for_attempt($questionattemptid){
        global $DB;

        if (isset(static::$_qacache[$questionattemptid])) {
            $assigns = static::$_qacache[$questionattemptid];
        } else {
            $assigns = array();
            $conditions = array('questionattemptid' => $questionattemptid);
            $records = $DB->get_records(static::DB_TABLE, $conditions, 'sortorder');
            foreach($records as $id => $record){
                $assigns[$id] = static::create($record);
            }
        }

        return $assigns;
    }

    static function get_all_for_feedback_instance($fbinstanceid){
        global $DB;

        $assigns = array();
        $records = $DB->get_records(static::DB_TABLE, array('feedbackinstanceid' => $fbinstanceid));
        foreach($records as $id => $record){
            $assigns[$id] = static::create($record);
        }

        return $assigns;
    }

    /**
     * Preload feedback assigns into a cache for use by several objects.
     * @param array|stdClass $qaids
     * @param array $fbinstances
     */
    static function preload_for_question_attempts($qaids, array $fbinstances = null){
        global $DB;

        static::$_qacache += array_fill_keys($qaids, array());
        $records = $DB->get_records_list(static::DB_TABLE, 'questionattemptid', $qaids, 'sortorder');
        foreach($records as $id => $record){
            if (!empty($fbinstances[$record->feedbackinstanceid])) {
                $record->_fbinstance = $fbinstances[$record->feedbackinstanceid];
            }
            static::$_qacache[$record->questionattemptid][$id] = static::create($record);
        }
        static::preload_max_sortorder($qaids);
    }

    static function preload_max_sortorder($qaids) {
        global $DB;
        if (!empty($qaids)) {
            list ($qaidsql,$qaidparams) = $DB->get_in_or_equal($qaids, SQL_PARAMS_NAMED);
            $sql = "SELECT questionattemptid, MAX(sortorder)
                    FROM {".static::DB_TABLE."}
                    WHERE questionattemptid $qaidsql AND sent = 0
                    GROUP BY questionattemptid";
            static::$_maxordercache += $DB->get_records_sql_menu($sql, $qaidparams);
        }
    }

    static function get_last_in_sort_context($sortcontext){
        global $DB;
        $qaid = $sortcontext['questionattemptid'];
        if (isset(static::$_maxordercache[$qaid])) {
            return (int)static::$_maxordercache[$qaid];
        } else {
            return (int)$DB->get_field(static::DB_TABLE, 'MAX(sortorder)', $sortcontext);
        }
    }

    protected $feedbackinstanceid;
    /**
     * Feedback instance assigned to this question attempt
     * @var local_fm_feedback_instance
     */
    protected $_fbinstance;
    protected $questionattemptid;
    protected $sent;
    protected $typical;
    protected $userid;


    function can_send(){
        $text = $this->get_text();
        if (empty($text) || ctype_space($text)) {
            // Don't send empty feedback
            // This gives instructors a way to tag student responses without generating feedback
            return false;
        } else {
            return true;
        }
    }

    function display(){
        $attr = array('class' => 'commands');
        $commands = html_writer::tag('span', implode(' ', $this->display_commands()), $attr);

        $info = array(get_string('assignedby', 'local_fm', fullname($this->get_user())));
        $classes = array('title');
        if ($this->was_sent()) {
            $classes[] = 'sent';
            $info[] = 'Sent on: '.$this->format_time_sent();
        }
        if (!$this->can_send()) {
            $classes[] = 'private';
            $info[] = get_string('privatenosend', 'local_fm');
        }
        if ($this->is_typical()) {
            $classes[] = 'typical';
        }

        $attr = array(
            'class' => implode(' ', $classes),
            'title' => implode("\n", $info),
        );
        $fbinstance = $this->get_feedback_instance();
        $name = html_writer::tag('span', $fbinstance->get_name(), $attr);

        $attr = array('class' => 'feedback_assign');
        return html_writer::tag('span', "$commands $name", $attr);
    }

    protected function display_commands(){
        global $PAGE, $OUTPUT;

        $commands = array();

        $savescroll = new component_action('click', 'M.core_scroll_manager.save_scroll_action');

        $attrs = array('tabindex'=>'-1');
        $url = $PAGE->url;
        $url->params(array('fbaid'=>$this->id, 'sesskey'=>sesskey()));
        if (!$this->was_sent()) {
            $url->param('action', 'delete');
            $commands[] = $OUTPUT->action_icon($url, new pix_icon('t/delete', get_string('delete')), $savescroll, $attrs);

            //$noscripts = array();
            if (!$this->is_sorted_first()) {
                $url->param('action', 'moveup');
                $commands[] = $OUTPUT->action_icon($url, new pix_icon('t/up', 'Move up'), $savescroll, $attrs);
            }
            if (!$this->is_sorted_last()) {
                $url->param('action', 'movedown');
                $commands[] = $OUTPUT->action_icon($url, new pix_icon('t/down', 'Move down'), $savescroll, $attrs);
            }
            //$commands[] = html_writer::tag('noscript', implode(' ', $noscripts));
        }
        $url->param('action', 'typical');
        $commands[] = $OUTPUT->action_icon($url, $this->get_typical_icon(), $savescroll, $attrs);

        return $commands;
    }

    protected function get_typical_icon(){
        $typstatus = $this->typical ? 'typical' : 'notypical';
        $typstring = get_string($typstatus, 'local_fm');
        return new pix_icon($typstatus, $typstring, 'local_fm');
    }

    function format_time_sent($format = null){
        $time = $this->get_time_sent();
        return userdate($time, $format);
    }

    function get_context(){
        return null;    //Context defined by question attempt
    }

    function get_db_record(){
        $record = parent::get_db_record();
        unset($record->contextid);    //Context defined by question attempt
        $record->questionattemptid  = $this->questionattemptid;
        $record->feedbackinstanceid = $this->feedbackinstanceid;
        $record->typical            = (bool)$this->typical;
        $record->userid             = $this->userid;
        $record->sent               = $this->sent;

        return $record;
    }

    function get_feedback(){
        return $this->get_feedback_instance()->get_feedback();
    }

    function get_feedback_instance(){
        if (!isset($this->_fbinstance)) {
            $this->_fbinstance = local_fm_feedback_instance::get($this->feedbackinstanceid);
        }

        return $this->_fbinstance;
    }

    function get_text(){
        return $this->get_feedback_instance()->get_text();
    }

    function get_time_sent(){
        return $this->sent;
    }

    function get_user(){
        global $DB;

        // Use static cache for feedback assigners
        if (!isset(self::$_usercache[$this->userid])) {
            self::$_usercache[$this->userid] = $DB->get_record('user', array('id' => $this->userid));
        }
        return self::$_usercache[$this->userid];
    }

    function get_userid(){
        return $this->userid;
    }

    function is_typical(){
        return !empty($this->typical);
    }

    function mark_sent($time = null){
        if (!isset($time)) {
            $time = time();
        }
        $this->sent = $time;
        $this->save();

        $fbinstance = $this->get_feedback_instance();
        if (!$fbinstance->was_sent()) {
            $fbinstance->mark_sent();
        }
    }

    function mark_typical($typical = true){
        $this->typical = $typical;
        $this->save();
    }

    function was_sent(){
        return !empty($this->sent);
    }
}

/**
 * Individual custom feedback given to a specific questionattempt.
 * @author nickkoeppen
 */
class local_fm_feedback_custom extends local_fm_contextual implements local_fm_assigned_feedback{
    const DB_TABLE = 'local_fm_feedback_custom';

    static protected $_qacache = array();

    static function get_all_for_attempt($questionattemptid){
        global $DB;

        if (isset(static::$_qacache[$questionattemptid])) {
            $customs = static::$_qacache[$questionattemptid];
        } else {
            $customs = array();
            $conditions = array('questionattemptid' => $questionattemptid);
            $records = $DB->get_records(static::DB_TABLE, $conditions);
            foreach($records as $id => $record){
                $customs[$id] = static::create($record);
            }
        }

        return $customs;
    }

    static function preload_for_question_attempts($qaids){
        global $DB;

        static::$_qacache += array_fill_keys($qaids, array());
        $records = $DB->get_records_list(static::DB_TABLE, 'questionattemptid', $qaids);
        foreach($records as $id => $record){
            static::$_qacache[$record->questionattemptid][$id] = static::create($record);
        }
    }

    protected $questionattemptid;
    protected $sent;
    protected $text;
    protected $userid;

    function get_db_record(){
        $record = parent::get_db_record();
        $record->questionattemptid = $this->questionattemptid;
        $record->sent   = $this->sent;
        $record->text   = $this->text;
        $record->userid = $this->userid;

        return $record;
    }

    function get_text(){
        return format_string($this->text);
    }

    function get_user(){
        global $DB;

        return $DB->get_record('user', array('id' => $this->userid));
    }

    function get_userid(){
        return $this->userid;
    }

    function mark_sent($time = null){
        if (!isset($time)) {
            $time = time();
        }
        $this->sent = $time;
        $this->save();
    }

    function update_text($text, $userid = null){
        global $USER;

        if (!isset($user)) {
            $userid = $USER->id;
        }

        $this->text   = $text;
        $this->userid = $userid;
        $this->save();
    }

    function was_sent(){
        return !empty($this->sent);
    }

    function is_typical(){
        return false;
    }
}

/**
 * Pseudo feedback class which indicates that a grade was assigned to a question attempt.  This is used for
 * flagging when feedback should be sent to the student.
 *
 * @author Matt Petro
 */
class local_fm_feedback_grade extends local_fm_contextual implements local_fm_assigned_feedback{
    const DB_TABLE = 'local_fm_feedback_grade';

    static protected $_qacache = array();

    static function get_all_for_attempt($questionattemptid){
        global $DB;

        if (isset(static::$_qacache[$questionattemptid])) {
            $grades = static::$_qacache[$questionattemptid];
        } else {
            $grades = array();
            $conditions = array('questionattemptid' => $questionattemptid);
            $record = $DB->get_record(static::DB_TABLE, $conditions);
            if ($record) {
                $grades[$record->id] = static::create($record);
            }
        }
        return $grades;
    }

    static function preload_for_question_attempts($qaids){
        global $DB;

        static::$_qacache += array_fill_keys($qaids, array());
        $records = $DB->get_records_list(static::DB_TABLE, 'questionattemptid', $qaids);
        foreach($records as $id => $record){
            static::$_qacache[$record->questionattemptid][$id] = static::create($record);
        }
    }

    protected $questionattemptid;
    protected $userid;

    function get_db_record(){
        $record = parent::get_db_record();
        $record->questionattemptid = $this->questionattemptid;
        $record->userid = $this->userid;

        return $record;
    }

    function get_text(){
        return '';
    }

    function get_user(){
        global $DB;

        return $DB->get_record('user', array('id' => $this->userid));
    }

    function get_userid(){
        return $this->userid;
    }

    function mark_sent($time = null){
        // Just delete the record if feedback was sent
        $this->delete();
    }

    function was_sent(){
        return false;
    }

    function is_typical(){
        return false;
    }
}

/**
 * Get all feedback given for a specific question attempt.
 * @author nickkoeppen
 */
class local_fm_question_attempt_feedback {

    /**
     * Determine the user that attempted a specific question attempt.
     * @param question_attempt $attempt
     * @return boolean|int
     */
    static function get_question_attempt_userid(question_attempt $attempt){
        $userid = false;
        foreach($attempt->get_step_iterator() as $step){
            //if ($step->get_state() instanceof question_state_todo) {
                $userid = $step->get_user_id();
                break;
            //}
        }

        return $userid;
    }

    /**
     * Preload assigned feedback data for given array of question attempt ids.
     * @param array|stdClass $qaids
     */
    static function preload_assigned_feedback($qaids, $fbinstances = null){
        local_fm_feedback_assign::preload_for_question_attempts($qaids, $fbinstances);
        local_fm_feedback_custom::preload_for_question_attempts($qaids);
        local_fm_feedback_grade::preload_for_question_attempts($qaids);
    }

    protected $assigns = array();
    protected $_fbinstances;
    protected $customs  = array();
    protected $grades  = array();
    protected $questionattemptid;
    /**
     * Question attempt requiring feedback
     * @var question_attempt
     */
    protected $_questionattempt;
    protected $_attemptuserid;
    protected $_attemptuser;
    protected $_question;

    /**
     * Constructs a feedback assigment
     * @param question_attempt|int $qattemptorid
     */
    function __construct($qattemptorid){
        global $DB;

        if ($qattemptorid instanceof question_attempt){
            $this->_questionattempt = $qattemptorid;
            $qaid = $qattemptorid->get_database_id();
        } else {
            $qaid = $qattemptorid;
        }
        $this->questionattemptid = $qaid;

        $assigns = local_fm_feedback_assign::get_all_for_attempt($qaid);
        foreach($assigns as $assign){
            $this->add_assign($assign);
        }

        $customs = local_fm_feedback_custom::get_all_for_attempt($qaid);
        foreach($customs as $custom){
            $this->add_custom($custom);
        }

        $grades = local_fm_feedback_grade::get_all_for_attempt($qaid);
        foreach($grades as $grade){
            $this->add_grade($grade);
        }
    }

    protected function add_assign(local_fm_feedback_assign $assign) {
        $status = $this->get_assign_status($assign);
        $sort   = $assign->get_sort_order();
        $this->assigns[$status][$sort] = $assign;

        // Update feedback instance cache
        $fbinstance = $assign->get_feedback_instance();
        $this->_fbinstances[$fbinstance->id] = $fbinstance;
    }

    protected function add_custom(local_fm_feedback_custom $custom){
        if ($custom->was_sent()) {
            $this->customs['sent'][] = $custom;     // Can be many
        } else {
            $this->customs['current'][0] = $custom;    // Only one
        }
    }

    protected function add_grade(local_fm_feedback_grade $grade){
        $this->grades['current'][0] = $grade;    // Only one
    }

    function add_feedback_assign($fbinstanceid) {
        global $USER;

        if (isset($this->_fbinstances[$fbinstanceid])) {
            // Already assigned, so just ignore.
            return;
        }

        $assigndef = new stdClass();
        $assigndef->questionattemptid  = $this->questionattemptid;
        $assigndef->feedbackinstanceid = $fbinstanceid;
        $assigndef->userid             = $USER->id;
        $assigndef->sent               = null;

        $assign = local_fm_feedback_assign::create($assigndef);
        $assign->save();

        $this->add_assign($assign);
    }

    function add_feedback_custom($customtext){
        global $USER;

        // Add record if none exists
        if (!$this->has_feedback_custom('current')) {
            $customdef = new stdClass();
            $customdef->questionattemptid = $this->questionattemptid;
            $customdef->text              = $customtext;
            $customdef->userid            = $USER->id;
            $customdef->sent              = false;

            $custom = local_fm_feedback_custom::create($customdef);
            $custom->save();

            $this->add_custom($custom);
        } else {
            $custom = $this->get_feedback_custom();
            $custom = reset($custom);
            $custom->update_text($customtext);
        }
    }

    function add_feedback_grade(){
        global $USER;

        // Add record if none exists
        if (!$this->has_feedback_grade('current')) {
            $gradedef = new stdClass();
            $gradedef->questionattemptid = $this->questionattemptid;
            $gradedef->userid            = $USER->id;

            $grade = local_fm_feedback_grade::create($gradedef);
            $grade->save();

            $this->add_grade($grade);
        }
    }


    function delete_assign($assignorid){
        if ($assignorid instanceof local_fm_feedback_assign) {
            $assign = $assignorid;
        } else {
            // Find assign object in data structure
            foreach($this->assigns as $status => $assigns) {
                foreach($assigns as $sortorder => $assignobj) {
                    if ($assignobj->id == $assignorid) {
                        $assign = $assignobj;
                        break;
                    }
                }
            }
        }

        if (!empty($assign)) {
            $status    = $this->get_assign_status($assign);
            $sortorder = $assign->get_sort_order();
            $this->assigns[$status][$sortorder]->delete();
            unset($this->assigns[$status][$sortorder]);
        }
    }

    function delete_feedback_custom($customid = null){
        if (!isset($customid)) {
            // Remove the current feedback
            if (!empty($this->customs['current'])) {
                $assign = reset($this->customs['current']);
                $assign->delete();
                unset($this->customs['current']);
            }
        }
    }

    function delete_feedback_instance_assigns($fbinstanceid) {
        if (!empty($this->assigns['current'])) {
            // Only can delete current feedback instances anyway
            foreach($this->assigns['current'] as $sortorder => $assign) {
                if ($assign->get_feedback_instance()->id == $fbinstanceid) {
                    $this->delete_assign($assign);
                }
            }
        }
    }

    protected function get_assign_status(local_fm_feedback_assign $assign){
        return $assign->was_sent() ? 'sent' : 'current';
    }

    function get_assigner_userids(){
        $userids = array();
        foreach($this->get_feedback_assigns('current') as $assign){
            $userids[] = $assign->get_userid();
        }
        foreach($this->get_feedback_custom('current') as $custom){
            $userids[] = $custom->get_userid();
        }
        foreach($this->get_feedback_grade('current') as $grade){
            $userids[] = $grade->get_userid();
        }
        $userids = array_unique($userids);

        return $userids;
    }

    function get_feedback_assigns($status = 'current'){
        return !empty($this->assigns[$status]) ? $this->assigns[$status] : array();
    }

    function get_feedback_custom($status = 'current'){
        return !empty($this->customs[$status]) ? $this->customs[$status] : array();
    }

    function get_feedback_grade($status = 'current'){
        return !empty($this->grades[$status]) ? $this->grades[$status] : array();
    }

    function get_feedback_instances(){
        if (!isset($this->_fbinstances)) {
            $this->_fbinstances = array();
            foreach($this->assigns as $assigns){
                foreach($assigns as $assign) {
                    $fbinstance = $assign->get_feedback_instance();
                    $this->_fbinstances[$fbinstance->id] = $fbinstance;
                }
            }
        }

        return $this->_fbinstances;
    }

    /**
     * Get the question definition related to this attempt.
     * @return question_definition
     */
    function get_question(){
        return $this->get_question_attempt()->get_question();
    }

    /**
     * Get question attempt to which these assignments belong.
     * @return question_attempt
     */
    function get_question_attempt(){
        if (!isset($this->_questionattempt)) {
            $dm = new question_engine_data_mapper();
            $this->_questionattempt = $dm->load_question_attempt($this->questionattemptid);
        }

        return $this->_questionattempt;
    }

    function get_attempt_userid(){
        if (!isset($this->_attemptuserid)) {
            $userid = false;
            $attempt = $this->get_question_attempt();
            if ($attempt) {
                $userid = static::get_question_attempt_userid($attempt);
            }

            $this->_attemptuserid = $userid;
        }

        return $this->_attemptuserid;
    }

    function get_attempt_group(local_fm_manager $manager){
        $userid = $this->get_attempt_userid();
        return $manager->get_group_for_user($userid);
    }

    function get_menu_current(){
        $menu = array();
        foreach($this->get_feedback_assigns() as $assign){
            $menu[$assign->id] = $assign->display();
        }

        return $menu;
    }

    function get_menu_options(local_fm_manager $manager){
        $assigned = $this->get_feedback_instances();

        foreach($manager->get_feedback_menu() as $status => $feedback){
            $menu[$status] = array_diff_key($feedback, $assigned);
        }

        return $menu;
    }

    function get_menu_sent(){
        $menu = array();
        foreach($this->get_feedback_assigns('sent') as $assign){
            $menu[$assign->id] = $assign->display();
        }

        return $menu;
    }

    function has_feedback_assign($onlystatus = false, $assignid = null){
        foreach($this->assigns as $status => $assigns){
            if ($onlystatus && $status != $onlystatus) {
                continue;
            }
            if (!isset($assignid)) {
                return true;
            }
            if (!empty($assigns[$assignid])) {
                return true;
            }
        }

        return false;
    }

    function has_feedback_custom($onlystatus = false){
        return !$onlystatus ? !empty($this->customs) : !empty($this->customs[$onlystatus]);
    }

    function has_feedback_grade($onlystatus = false){
        return !$onlystatus ? !empty($this->grades) : !empty($this->grades[$onlystatus]);
    }

    function has_feedback_instance($fbinstanceid){
        $fbinstances = $this->get_feedback_instances();
        return !empty($fbinstances[$fbinstanceid]);
    }
}

?>