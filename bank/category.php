<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../config.php');
require_once($CFG->dirroot.'/local/fm/bank/lib.php');
require_once($CFG->dirroot.'/local/fm/bank/forms.php');

$id        = optional_param('id', null, PARAM_INT);
$action    = optional_param('action', null, PARAM_ALPHA);
// OR
$contextid = optional_param('context', null, PARAM_INT);
$parentid  = optional_param('parent', null, PARAM_INT);

$return = optional_param('return', null, PARAM_URL);
$returncategory = optional_param('category', null, PARAM_INT);

global $PAGE, $OUTPUT, $USER;

// Configuration
$params = array();
if ($id) {
    $params['id'] = $id;
    $category = local_fm_category::get($id);
    $context = $category->get_context();
    $contextid = $context->id;
    $parentid = null;
} else if ($contextid) {
    $params['context'] = $contextid;
    if ($parentid) {
        $parent = local_fm_category::get($parentid);
        if (!$parent->can_manage()) {
            throw new moodle_exception('cannotmanage');
        }
        $params['parent'] = $parentid;
    }
    $context = context::instance_by_id($contextid);
    $catdef = new stdClass();
    $catdef->contextid = $context->id;
    $catdef->parentid  = $parentid;
    $catdef->createdby = $USER->id;
    $category = local_fm_category::create($catdef);
    $id = null;
    $action = null;
} else {
    throw new Exception('invalidparameters');
}
$params['category'] = $returncategory;

// Context and capabilities
local_fm_category::set_page_context($context);
if (!$category->can_manage()) {
    throw new Exception('cannotmanage');
}

// Navigation
$baseurl = new moodle_url('/local/fm/bank/category.php');
$returnurl = new moodle_url('/local/fm/bank/edit.php', array('category'=>$returncategory, 'context'=>$context->id));
$PAGE->set_url($baseurl, $params);
navigation_node::override_active_url(new moodle_url($returnurl, array('context' => $context->id)));

// Data processing
if ($action && $action != 'edit' && $category->exists()) {
    require_sesskey();
    switch($action){
        case 'delete':
            $category->delete();
            // Ensure we don't try to return to this category
            if ($category->id == $returncategory) {
                $returnurl->param('category', null);
            }
            break;
        case 'down':
            $category->move_down();
            break;
        case 'up':
            $category->move_up();
            break;
    }

    redirect($returnurl);
}

$customdata = array('category' => $category);
$editform = new fm_category_edit_form($PAGE->url, $customdata);

if ($editform->is_cancelled()) {
    redirect($returnurl);
} else if ($definition = $editform->get_data()) {
    $category->form_load_data($definition);
    $category->save();

    redirect($returnurl);
}

// Page setup
if ($category->exists()) {
    $title = get_string('editfeedbackcategory', 'local_fm');
} else {
    $title = get_string('addfeedbackcategory', 'local_fm');
}
$PAGE->set_title($title);
$PAGE->set_heading($title);
if (isset($category)) {
    $category->add_navigation($PAGE);
}
$PAGE->navbar->add($title);

// Page display
echo $OUTPUT->header();

echo $OUTPUT->heading($PAGE->heading);

$editform->display();

echo $OUTPUT->footer();