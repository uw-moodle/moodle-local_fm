<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../config.php');
require_once($CFG->dirroot.'/local/fm/bank/lib.php');
require_once($CFG->dirroot.'/local/fm/bank/forms.php');

// Edit using
$id         = optional_param('id', null, PARAM_INT);
$action     = optional_param('action', null, PARAM_ALPHA);
// OR
// Create using
$categoryid = optional_param('category', null, PARAM_INT);

// Optional behaviors
$return     = optional_param('return', null, PARAM_URL);
$returncategory = optional_param('category', null, PARAM_INT);

global $PAGE, $OUTPUT, $USER;

// Configuration
$params = array();
if ($id) {
    $params['id'] = $id;
    $feedback = local_fm_feedback::get($id);
    $categoryid = null;
} else {
    $category = local_fm_category::get($categoryid);
    if (!$category->can_manage()) {
        throw new moodle_exception('cannotmanage');
    }
    $params['category'] = $categoryid;

    $newdef = new stdClass();
    $newdef->createdby = $USER->id;
    $newdef->categoryid = $category->id;

    $feedback = local_fm_feedback::create($newdef);
    $id = null;
    $action = null;
}

// Context and capabilities
$feedback->verify_page('manage');

// Navigation
$baseurl = new moodle_url('/local/fm/bank/feedback.php');
$navbase = new moodle_url('/local/fm/bank/edit.php');
if ($return) {
    $params['return'] = $return;
} else {
    $navparams = array();
    $navparams['category'] = $returncategory;
    $navparams['context'] = $feedback->get_context()->id;
    $return = new moodle_url($navbase, $navparams);
}
$params['category'] = $returncategory;

$PAGE->set_url($baseurl, $params);
$navurl = new moodle_url($navbase, array('context' => $feedback->get_context()->id));
navigation_node::override_active_url($navurl);

// Data processing
if ($action && $action != 'edit' && $feedback->exists()) {
    require_sesskey();
    switch($action) {
        case 'copy':
            $feedback->copy();
            break;
        case 'delete':
            $feedback->delete();
            break;
        case 'down':
            $feedback->move_down();
            break;
        case 'info':
            //Redirect to info
        case 'up':
            $feedback->move_up();
            break;
    }

    redirect($return);
}
$customdata = array('feedback' => $feedback);
$editform = new fm_feedback_edit_form($PAGE->url, $customdata);

if ($editform->is_cancelled()) {
    redirect($return);
} else if ($definition = $editform->get_data()) {
    $feedback->form_load_data($definition);
    $feedback->save();

    redirect($return);
}

// Page setup
if ($feedback->exists()) {
    $title = get_string('editfeedback', 'local_fm');
} else {
    $title = get_string('addfeedback', 'local_fm');
}
$PAGE->set_title($title);
$PAGE->set_heading($PAGE->title);
$feedback->get_category()->add_navigation($PAGE);

$PAGE->navbar->add($title);

// Page display
echo $OUTPUT->header();

echo $OUTPUT->heading($PAGE->heading);

if ($feedback->exists()) {
    $url = $PAGE->url;
    $url->remove_params('return');    // No going back on these options
    $url->param('action', 'delete');
    echo $OUTPUT->single_button($url, get_string('deletefeedback', 'local_fm'));
}

$editform->display();

echo $OUTPUT->footer();