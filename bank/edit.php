<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../config.php');
require_once($CFG->dirroot.'/local/fm/locallib.php');
require_once($CFG->dirroot.'/local/fm/bank/forms.php');
require_once($CFG->dirroot.'/local/fm/renderer.php');

$contextid  = optional_param('context', null, PARAM_INT);
// AND/OR
$categoryid = optional_param('category', null, PARAM_INT);

global $PAGE, $OUTPUT, $DB, $USER;

$params = array();
if ($categoryid) {
    $params['category'] = $categoryid;
    $category = local_fm_category::get($categoryid);
    $context  = $category->get_context();
    $contextid = $context->id;
    if (!$category->can_use()) {
        throw new Exception('cannot use this category');
    }
} else if ($contextid) {
    $params['context'] = $contextid;
    $context = context::instance_by_id($contextid, MUST_EXIST);
    $category = null;
} else {
    throw new Exception('invalidparams');
}

local_fm_manager::page_context_check($context);

// Verify access
if ($context instanceof context_user && $context->instanceid == $USER->id) {
    require_capability('local/fm:manageownfeedback', context_system::instance());
} else {
    require_capability('local/fm:manage', $context);
}

// Navigation
$baseurl = new moodle_url('/local/fm/bank/edit.php', array('context' => $context->id));
if ($category) {
    $baseurl->param('category', $category->id);
}
$addparams = array('context' => $context->id);
if ($categoryid) {
    $addparams['category'] = $categoryid;
}
$addcaturl = new moodle_url('/local/fm/bank/category.php', $addparams);
$editurl = new moodle_url('/local/fm/bank/feedback.php');
$PAGE->set_url($baseurl);

// Data processing
$table = new local_fm_category_display($context, $category);

// Setup page
$PAGE->set_title(get_string('feedbackbank', 'local_fm'));
$PAGE->set_heading($PAGE->title);
if (isset($category)) {
    $category->add_navigation($PAGE);
}
$renderer = $PAGE->get_renderer('local_fm');

// Display page
echo $OUTPUT->header();

echo $OUTPUT->heading($context->get_context_name());

$options = local_fm_category::get_options_menu($context);
$catselect = '';
if (!empty($options)) {
    $selected = isset($category) ? $category->id : null;
    $select = new single_select($baseurl, 'category', $options, $selected, array(''=>get_string('showall', 'local_fm')));
    $select->set_label(get_string('feedbackcategories', 'local_fm'), array('class'=>'fm_inline_label'));
    $catselect .= $OUTPUT->render($select);
}
if ($table->can_manage_context()) {
    $catselect .= $OUTPUT->single_button($addcaturl, get_string('addnewcategory', 'local_fm'), 'post', array('class'=>'fm_bankaddcategory'));
}
$contents = html_writer::tag('span', $catselect);
echo $OUTPUT->container($contents, 'centerselect');

$renderer->render($table);

echo $OUTPUT->footer();

?>