<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->libdir.'/tablelib.php');

class local_fm_feedback_table extends html_table implements renderable {

    protected $category;

    function __construct(local_fm_category $category){
        $this->category = $category;
    }

    function add_cell_controls(local_fm_feedback $feedback, $url = null){
        global $OUTPUT, $PAGE;

        if (!isset($url)) {
            $displaycategory = $PAGE->url->get_param('category');
            $url = new moodle_url('/local/fm/bank/feedback.php', array('sesskey'=>sesskey(), 'category'=>$displaycategory));
        }

        $controls = array();
        //TODO: Add capabilities check
        if ($feedback->can_manage()) {
            if (!$feedback->is_sorted_first()) {
                $url->params(array('action' => 'up', 'id' => $feedback->id));
                $controls[] = $OUTPUT->action_icon($url, new pix_icon('t/up', get_string('up')));
            } else {
                $controls[] = $this->get_spacer();
            }
            if (!$feedback->is_sorted_last()) {
                $url->params(array('action' => 'down', 'id' => $feedback->id));
                $controls[] = $OUTPUT->action_icon($url, new pix_icon('t/down', get_string('down')));
            } else {
                $controls[] = $this->get_spacer();
            }
            $url->params(array('action' => 'delete', 'id' => $feedback->id));
            $controls[] = $OUTPUT->action_icon($url, new pix_icon('t/delete', get_string('delete')));
            $url->params(array('action' => 'edit', 'id' => $feedback->id));
            $controls[] = $OUTPUT->action_icon($url, new pix_icon('t/edit', get_string('edit')));
            $url->params(array('action' => 'copy', 'id' => $feedback->id));
            $controls[] = $OUTPUT->action_icon($url, new pix_icon('t/copy', get_string('duplicate')));
        }

        return new html_table_cell(implode(' ', $controls));
    }

    function get_spacer() {
        global $OUTPUT;
        return '<img src="' . $OUTPUT->pix_url('spacer') . '" class="smallicon" alt="" /> ';
    }

    function get_controls(local_fm_category $category, moodle_url $url = null){
        global $OUTPUT, $PAGE;

        if (!isset($url)) {
            $displaycategory = $PAGE->url->get_param('category');
            $url = new moodle_url('/local/fm/bank/category.php', array('sesskey'=>sesskey(), 'category'=>$displaycategory));
        }

        $controls = array();
        if ($category->can_manage()) {
            // TODO Fix the edit script so that these make sense in the UI
            //             if (!$category->is_sorted_first()) {
            //                 $url->params(array('action' => 'up', 'id' => $category->id));
            //                 $controls[] = $OUTPUT->action_icon($url, new pix_icon('t/up', get_string('up')));
            //             }
            //             if (!$category->is_sorted_last()) {
            //                 $url->params(array('action' => 'down', 'id' => $category->id));
            //                 $controls[] = $OUTPUT->action_icon($url, new pix_icon('t/down', get_string('down')));
            //             }

            $url->params(array('action' => 'edit', 'id' => $category->id));
            $controls[] = $OUTPUT->action_icon($url, new pix_icon('t/edit', get_string('edit')));
            $url->params(array('action' => 'delete', 'id' => $category->id));
            $controls[] = $OUTPUT->action_icon($url, new pix_icon('t/delete', get_string('delete')));
        }

        return $controls;
    }

    function get_heading_controls(local_fm_category $category){
        global $OUTPUT;

        $controls = $this->get_controls($category);
        switch($category->get_share_type()) {
            case $category::SHARE_GENERAL:
                $sharing = get_string('sharegeneral', 'local_fm');
                break;
            case $category::SHARE_PRIVATE:
                $sharing = get_string('shareprivate', 'local_fm');
                break;
        }

        return implode(' ', $controls) . " ($sharing) ";
    }


    function add_cell_name(local_fm_feedback $feedback){
        return new html_table_cell($feedback->get_name());
    }

    function add_cell_sharing(local_fm_feedback $feedback){
        global $OUTPUT;

        switch($feedback->get_share_type()){
            case $feedback::SHARE_GENERAL:
                $text = get_string('sharegeneral', 'local_fm');
                break;
            case $feedback::SHARE_PRIVATE:
                $text = get_string('shareprivate', 'local_fm');
                break;
            default:
                $text = get_string('shareinvalid', 'local_fm');
        }

        return new html_table_cell($text);
    }

    function add_cell_text(local_fm_feedback $feedback){
        return new html_table_cell($feedback->get_text());
    }

    function add_cell_user_creator(local_fm_feedback $feedback){
        return new html_table_cell(fullname($feedback->get_user_creator()));
    }

    function add_cell_user_modifier(local_fm_feedback $feedback){
        $modifier = $feedback->get_user_modifier();
        if ($modifier) {
            $modifiedby = fullname($modifier);
        } else {
            $modifiedby = '--';
        }

        return new html_table_cell($modifiedby);
    }

    function add_row_feedback($feedback){
        $row = new html_table_row();
        $row->attributes['class'] = 'row_feedback';

        $row->cells[] = $this->add_cell_name($feedback);
        $row->cells[] = $this->add_cell_text($feedback);
        $row->cells[] = $this->add_cell_sharing($feedback);
        $row->cells[] = $this->add_cell_user_creator($feedback);
        $row->cells[] = $this->add_cell_user_modifier($feedback);
        $row->cells[] = $this->add_cell_controls($feedback);
            // TODO: Revive when bulk actions added
//             $checkbox = html_writer::checkbox("selected[$feedback->id]", 1, false);
//             $row->cells[] = new html_table_cell($checkbox);

        $this->data[] = $row;
    }

    function add_row_user(stdClass $user){
        $row = new html_table_row();
        $row->attributes['class'] = 'row_user';

        $cell = new html_table_cell(fullname($user));
        $cell->colspan = count($this->head);
        $row->cells[] = $cell;

        $this->data[] = $row;
    }

    function can_manage_feedback(){
        return $this->category->can_manage_feedback();
    }

    function get_category(){
        return $this->category;
    }

    function get_grouped_feedback(){
        $allfeedback = $this->category->get_feedback();

        $grouped  = array();
        $creators = array();

        // Group feedback by user that created it
        foreach($allfeedback as $feedback){
            $creator = $feedback->get_user_creator();
            if (!isset($creators[$creator->id])) {
                $creators[$creator->id] = $creator;
            }

            $grouped[$creator->id][] = $feedback;
        }

        return array($grouped, $creators);
    }

    function setup(){
        $this->id = 'fm_manage_table';
        $this->head = array(
                get_string('label', 'local_fm'),
                get_string('text', 'local_fm'),
                get_string('sharing', 'local_fm'),
                get_string('createdby', 'local_fm'),
                get_string('modifiedby', 'local_fm'),
                get_string('controls', 'local_fm'),
        );
        $this->align = array_fill(0, count($this->head), 'center');
    }

    function set_data(){
        global $USER;

        list($groupedfb, $creators) = $this->get_grouped_feedback();

        // Show feedback created by current user first
        if (!empty($groupedfb[$USER->id])) {
            foreach($groupedfb[$USER->id] as $feedback){
                $this->add_row_feedback($feedback);
            }
            unset($creators[$USER->id]);
        }

        // Show shared feedback with creator name rows
        foreach ($creators as $userid => $creator) {
            //$this->add_row_user($creators[$userid]);

            foreach($groupedfb[$userid] as $feedback) {
                $this->add_row_feedback($feedback);
            }
        }
    }
}

// TODO: Make this more general (like context_display - handle all context, cat, and question)
class local_fm_category_display implements renderable {

    protected $context;
    protected $category;
    protected $categories;
    protected $canmanage;

    function __construct(context $context, $category = null){
        $this->context = $context;
        $this->category = $category;
        if ($category) {
            $this->categories = array($category);
            $this->canmanage  = $category->can_manage();
        } else {
            // All top-level categories
            // Ensure at least one category exists.
            local_fm_category::get_default_category($context);
            $this->categories = local_fm_category::get_my_categories($context);
            foreach ($this->categories as $catid => $cat) {
                if ($cat->has_parent()) {
                    unset($this->categories[$catid]);
                }
            }
            $this->canmanage  = local_fm_category::can_manage_in_context($context);
        }
    }

    function can_manage_context(){
        return $this->canmanage;
    }

    function get_categories(){
        return $this->categories;
    }

    function get_category(){
        return $this->category;
    }

    function get_context(){
        return $this->context;
    }
}

abstract class fm_bankable_edit_form extends moodleform {

    function add_context_options(context $context){
        $this->_form->addElement('header', 'contexthead', get_string('context', 'role'));

        $label = get_string('context', 'role');
        $name = $context->get_context_name();
        $this->_form->addElement('static', 'contextdisp', $label, $name);
    }

    function add_user_information(local_fm_bankable $bankobj){
        $mform =& $this->_form;

        $mform->addElement('header', 'userhead', get_string('userinfo', 'local_fm'));

        if ($bankobj->exists()) {
            $creator = $bankobj->get_user_creator();
            $label = get_string('createdby', 'local_fm');
            $name  = fullname($creator);
            $mform->addElement('static', 'creatordisp', $label, $name);

            $modifier = $bankobj->get_user_modifier();
            if ($modifier && $modifier->id != $creator->id) {
                $label = get_string('modifiedby', 'local_fm');
                $name  = fullname($modifier);
                $mform->addElement('static', 'modifierdisp', $label, $name);
            }
        }

        $label = get_string('sharing', 'local_fm');
        $sharevals = $bankobj::get_share_values();
        $options = array(
            $sharevals[$bankobj::SHARE_GENERAL] => get_string('sharegeneral', 'local_fm'),
            $sharevals[$bankobj::SHARE_PRIVATE] => get_string('shareprivate', 'local_fm'),
        );
        $mform->addElement('select', 'shared', $label, $options);
        $mform->addHelpButton('shared', 'sharing', 'local_fm');

        // Set sharing based on parent object or set generally
        $default = $bankobj::SHARE_GENERAL;
        if (!$bankobj->exists()) {
            if (($bankobj instanceof local_fm_feedback) && $bankobj->in_category_context()) {
                $default = $bankobj->get_category()->get_share_type();
            } else if ($bankobj instanceof local_fm_category && $bankobj->has_parent()) {
                $default = $bankobj->get_parent()->get_share_type();
            }
        }
        $mform->setDefault('shared', $sharevals[$default]);
        $mform->freeze('shared');
    }
}

class fm_feedback_edit_form extends fm_bankable_edit_form {

    function add_feedback_context_options(local_fm_feedback $feedback){
        $this->add_context_options($feedback->get_context());

        $label = get_string('category', 'local_fm');
        $name = $feedback->get_category()->get_name();

        $this->_form->addElement('static', 'extenddisp', $label, $name);
    }

    function definition() {
        $mform =& $this->_form;

        $feedback = $this->_customdata['feedback'];

        if ($feedback->exists()) {
            $headerstr = 'editfeedback';
            $buttonstr = 'savechanges';
        } else {
            $headerstr = 'addfeedback';
            $buttonstr = 'addfeedback';
        }

        $strrequired = get_string('required');

        $mform->addElement('header', 'generaltagheader', get_string($headerstr, 'local_fm'));

        $options = array('maxlength' => 255, 'size' => 50);
        $mform->addElement('text', 'name', get_string('feedbackname', 'local_fm'), $options);
        $mform->addRule('name', get_string('namecantbeblank', 'local_fm'), 'required', null, 'client');
        $mform->setType('name', PARAM_MULTILANG);

        $options =  array('rows' => 10, 'cols' => 60);
        $mform->addElement('textarea', 'text', get_string('feedbacktext', 'local_fm'), $options);
        $mform->addRule('text', $strrequired, 'required', null, 'client');
        $mform->setType('text', PARAM_NOTAGS);

        $this->add_feedback_context_options($feedback);

        $this->add_user_information($feedback);

        if ($feedback->exists()) {
            $feedback->form_set_data($mform);
        }

        $this->add_action_buttons(true, get_string($buttonstr, 'local_fm'));
    }
}

class fm_category_edit_form extends fm_bankable_edit_form {

    function add_category_context_options(local_fm_category $category){
        $this->add_context_options($category->get_context());

        if ($category->has_parent()) {
            $label = get_string('parentcategory', 'local_fm');
            $name = $category->get_parent()->get_name();
            $this->_form->addElement('static', 'catdisp', $label, $name);
        }
    }

    function definition() {
        $mform =& $this->_form;

        $category = $this->_customdata['category'];

        if ($category->exists()) {
            $headerstr = 'editcategory';
            $buttonstr = 'savechanges';
        } else {
            $headerstr = 'addcategory';
            $buttonstr = 'addcategory';
        }

        $strrequired = get_string('required');

        $mform->addElement('header', 'generaltagheader', get_string($headerstr, 'local_fm'));

        $options = array('maxlength' => 255, 'size' => 50);
        $mform->addElement('text', 'name', get_string('categoryname'), $options);
        $mform->addRule('name', get_string('namecantbeblank', 'local_fm'), 'required', null, 'client');
        $mform->setType('name', PARAM_MULTILANG);

        $options = array('rows' => 10, 'cols' => 60);
        $mform->addElement('textarea', 'info', get_string('categoryinfo', 'local_fm'), $options);

        $this->add_category_context_options($category);

        $this->add_user_information($category);

        // TODO: Parent display
        // TODO: Override

    //         // Other contexts
//         $clevel = optional_param('clevel', null, PARAM_INT);
//         $levels = array();
//         foreach(context_helper::get_all_levels() as $contextlevel => $contextclass){
//             if ($contextlevel == CONTEXT_SYSTEM) {
//                 continue;
//             }
//             $levels[$contextlevel] = $contextclass::get_level_name();
//         }
//         $select = new single_select($url, 'clevel', $levels, $clevel);
//         $select->set_label('Level');
//         echo $OUTPUT->render($select);

//         if ($clevel) {
//             $options = $table->get_other_context_options($clevel);
//             $select = new single_select($url, 'context', $options, $context->id);
//             $select->set_label('Context');
//             echo $OUTPUT->render($select);
//         }

        if ($category->exists()) {
            $category->form_set_data($mform);
        }

        $this->add_action_buttons(true, get_string($buttonstr, 'local_fm'));
    }

}
