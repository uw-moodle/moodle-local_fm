<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager library.
 *
 * @package     local_fm
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

//TODO: Handle permissions with particular attention to user contexts

require_once($CFG->libdir.'/questionlib.php');
require_once($CFG->dirroot . '/local/fm/lib.php');

/**
 * Base feedback bank objects that are sortable and shareable.
 * @author nickkoeppen
 */
abstract class local_fm_bankable extends local_fm_sortable {
    const SHARE_GENERAL = 2;
    const SHARE_PRIVATE = 3;

    static function can_manage_in_context(context $context){
        if (parent::can_manage_in_context($context)) {
            return true;
        }
        global $USER;

        return $context instanceof context_user
               && $context->instanceid == $USER->id
               && has_capability('local/fm:manageownfeedback', context_system::instance());
    }

    protected static function create_full_definition(stdClass $definition){
        global $USER;

        $definition = parent::create_full_definition($definition);
        if (empty($definition->createdby)) {
            $definition->createdby = $USER->id;
        }
        return $definition;
    }

    static function get_share_values(){
        return array(
            static::SHARE_GENERAL => 1,
            static::SHARE_PRIVATE => 0,
        );
    }

    /**
     * The sharing information for this object.
     * @var bool|array
     */
    protected $createdby;
    protected $_creator;
    protected $modifiedby;
    protected $_modifier;

    function can_use(){
        if (!$this->exists()) {
            return false;
        }
        if ($this->is_creator() || $this->is_shared()) {
            return true;
        }

        return $this->can_manage();
    }

    function can_view(){
        //TODO: Review the need for this
        return !$this->is_private();
    }

    //TODO: Abstract display sharing to centralize edit sharing

    function get_db_record(){
        $record = parent::get_db_record();
        $record->createdby  = $this->createdby;
        $record->modifiedby = $this->modifiedby;

        return $record;
    }

    function get_share_type(){
        if ($this->is_shared()) {
            return static::SHARE_GENERAL;
        } else {
            return static::SHARE_PRIVATE;
        }
    }

    function get_user_creator(){
        global $DB;

        if (empty($this->_creator) || $this->_creator->id != $this->createdby) {
            $this->_creator = $DB->get_record('user', array('id' => $this->createdby));
        }

        return $this->_creator;
    }

    function get_user_modifier(){
        global $DB;

        if (empty($this->_modifier) || $this->_modifier->id != $this->modifiedby) {
            if (!isset($this->modifiedby)) {
                $modifier = false;
            } else {
                $modifier = $DB->get_record('user', array('id' => $this->modifiedby));
            }

            $this->_modifier = $modifier;
        }

        return $this->_modifier;
    }

    function is_creator($userid = null){
        global $USER;

        if (!isset($userid)) {
            $userid = $USER->id;
        }

        return ($this->createdby == $userid);
    }

    function is_private(){
        return !$this->is_shared();
    }

    function is_shared($userid = null){
        return !$this->is_private_context();
    }

    function is_private_context () {
        return $this->get_context() instanceof context_user;
    }

    protected function update_record($record){
        global $USER;

        $record->modifiedby = $USER->id;
        parent::update_record($record);
    }
}

class local_fm_feedback extends local_fm_bankable {
    const DB_TABLE = 'local_fm_feedback';

    protected static function _sort_params(stdClass $definition){
        return array(
            'categoryid' => $definition->categoryid,
            'createdby'  => $definition->createdby,
        );
    }

    static function category_has_feedback(local_fm_category $category){
        global $DB;

        return $DB->record_exists(static::DB_TABLE, array('categoryid' => $category->id));
    }

    static function get_all_for_question($questionid){
        global $DB;

        $feedback = array();
        $records = $DB->get_records(static::DB_TABLE, array('questionid' => $questionid), 'createdby, sortorder');
        foreach($records as $id => $fbrecord){
            $feedback[$id] = static::create($fbrecord);
        }

        return $feedback;
    }

    /**
     * Get all feedback defined in a given category.
     * @param local_fm_category $category
     * @return local_fm_feedback[]
     */
    static function get_all_in_category(local_fm_category $category){
        global $DB;

        $sitemanager = static::can_manage_all();

        $fbobjects = array();
        $fbrecords = $DB->get_records(static::DB_TABLE, array('categoryid' => $category->id), 'createdby, sortorder');
        foreach($fbrecords as $id => $fbrecord){
            $fbobject = static::create($fbrecord);
            if ($sitemanager || $fbobject->can_use()) {
                $fbobjects[$id] = $fbobject;
            }
        }

        return $fbobjects;
    }

    /**
     * Get all feedback defined in a given context.
     * @param context $context
     * @param int $userid
     * @return local_fm_feedback[]
     */
    static function get_all_in_context(context $context, $userid = null){
        $feedback = array();
        $categories = local_fm_category::get_all_in_context($context);
        foreach($categories as $catid => $category){
            $feedback[$catid] = static::get_all_in_category($category);
        }

        return $feedback;
    }

    static function get_all_question_linked($questionid){
        global $DB;

        $feedback = array();
        $where = 'questionid IS NOT NULL';
        $records = $DB->get_records_select(static::DB_TABLE, $where, array(), 'sortorder');
        foreach($records as $id => $fbrecord){
            $feedback[$fbrecord->questionid][$id] = static::create($fbrecord);
        }

        return $feedback;
    }

    protected $categoryid;
    /**
     * Feedback category
     * @var local_fm_category
     */
    protected $_category;
    protected $questionid;
    /**
     * Question definition
     * @var question_definition
     */
    protected $_question;
    protected $name;
    protected $text;

    function __toString(){
        return $this->text;
    }

    function can_assign(){
        if (!$this->can_use()) {
            return false;
        }

        return has_capability('local/fm:assignfeedback', $this->get_context());
    }

    function can_manage(){
        if (parent::can_manage()) {
            return true;    // Can manage this whole context
        }
        $context = $this->get_context();
        if (!($context instanceof context)) {
            return false;
        }
        if ($this->is_creator()) {
            return has_capability('local/fm:manageownfeedback', $context);
        } else if ($this->is_shared()) {
            return has_capability('local/fm:managesharedfeedback', $context);
        }

        return false;
    }

    function delete($reorder = true){
        foreach($this->get_instances() as $instance){
            $instance->delete(false);
        }

        parent::delete($reorder);
    }

    /**
     * Get the category within which this feedback is defined.
     * @return local_fm_category
     */
    function get_category(){
        if (!isset($this->_category)) {
            if (empty($this->categoryid)) {
                $this->_category = false;
            } else {
                $this->_category = local_fm_category::get($this->categoryid);
            }
        }

        return $this->_category;
    }

    function get_context(){
        if ($this->in_category_context()) {
            $context = $this->get_category()->get_context();
        } else if ($this->in_question_context()) {
            $contextid = $this->get_question()->contextid;
            $context = context::instance_by_id($contextid, MUST_EXIST);
        } else {
            $context = false;
        }

        return $context;
    }

    function get_db_record(){
        $record = parent::get_db_record();
        unset($record->contextid);    //Feedback has context, but defined by the category
        $record->categoryid = $this->categoryid;
        $record->questionid = $this->questionid;
        $record->name       = $this->name;
        $record->text       = $this->text;

        return $record;
    }

    function get_instances(){
        global $CFG;

        // Don't want to have this library loaded unless necessary
        require_once($CFG->dirroot.'/local/fm/locallib.php');
        return local_fm_feedback_instance::get_all_for_feedback($this->id);
    }

    function get_name(){
        return $this->name;
    }

    function get_question(){
        if (!isset($this->_question)) {
            $qid = $this->questionid;
            $this->_question = $qid ? question_bank::load_question($qid) : false;
        }

        return $this->_question;
    }

    function get_text(){
        return $this->text;
    }

    function in_category_context(){
        return !empty($this->categoryid);
    }

    function in_question_context(){
        return !empty($this->questionid);
    }

    function set_name($newname){
        $this->name = $newname;
    }

    function set_text($newtext){
        $this->text = $newtext;
    }
}

class local_fm_category extends local_fm_bankable {
    const DB_TABLE = 'local_fm_category';

    protected static function _sort_params(stdClass $definition){
        return array(
            'contextid' => $definition->contextid,
            'parentid'  => $definition->parentid,
            'createdby' => $definition->createdby,
        );
    }

    static function can_manage_in_context(context $context){
        if (parent::can_manage_in_context($context)) {
            return true;
        }

        return has_capability('local/fm:managecategories', $context);
    }

    static function get_all(){
        global $DB;

        $cats = array();
        $records = $DB->get_records(self::DB_TABLE, array(), 'contextid, parentid, sortorder');
        foreach($records as $id => $record){
            $cats[$id] = static::create($record);
        }

        return $cats;
    }

    static function get_all_contexts(){
        global $DB;

        $contextids = $DB->get_fieldset_select(static::DB_TABLE, 'contextid', '');

        $contexts = array();
        $records = $DB->get_records_list('context', 'id', array_unique($contextids));
        foreach($records as $id => $context){
            context_helper::preload_from_record($context);    // Avoid additional DB calls
            $contexts[$id] = context::instance_by_id($id);
        }

        return $contexts;
    }

    static function get_all_in_context(context $context){
        global $DB;

        $cats = array();
        $records = $DB->get_records(static::DB_TABLE, array('contextid' => $context->id), 'parentid, sortorder');
        foreach($records as $id => $catrecord){
            $cats[$id] = static::create($catrecord);
        }

        return $cats;
    }

    /**
     * Retrieve categories available to the current user.
     * @param context $context (optional) if specified then only categories in context
     * @return array
     */
    static function get_my_categories(context $context = null){
        if (isset($context)) {
            $categories = static::get_all_in_context($context);
        } else {
            $categories = static::get_all();
        }

        // Narrow list based on permissions
        if (!local_fm_feedback::can_manage_all()) {
            foreach($categories as $id => $cat){
                if (!$cat->can_use()) {
                    unset($categories[$id]);
                }
            }
        }

        return $categories;
    }

    static function get_my_contexts(){
        $allcontexts = static::get_all_contexts();    //Preload defined contexts
        if (local_fm_feedback::can_manage_all()) {
            return $allcontexts;
        }

        $contexts = array();
        $categories = static::get_my_categories();
        foreach($categories as $category){
            $context = $category->get_context();
            if (!isset($contexts[$context->id]) && $category->can_use()) {
                $contexts[$context->id] = $context;
            }
        }

        return $contexts;
    }

    protected static function _make_options_menu($categories, $parentid = null, $prefix = '') {
        $result = array();
        foreach($categories as $id => $cat){
            if ($cat->get_parentid() == $parentid) {
                unset($categories[$id]);
                $name = $cat->get_name();
                $result[$id] = $prefix.$name;
                $result += self::_make_options_menu($categories, $id, $prefix." $name / ");
            }
        }
        return $result;
    }


    static function get_options_menu(context $context){
        $categories = local_fm_category::get_my_categories($context);
        return self::_make_options_menu($categories);
    }

    /**
     * Get default feedback bank category in context, creating if necessary.
     *
     * @param context $context
     */
    static function get_default_category(context $context) {
        global $USER;

        $categories = self::get_my_categories($context);
        if ($categories) {
            // Return first top-level category found.
            foreach ($categories as $category) {
                if (empty($category->parent)) {
                    return $category;
                }
            }
        }
        // Need to create a top-level category
        $catdef = new stdClass();
        $catdef->contextid = $context->id;
        $catdef->parentid  = null;
        $catdef->createdby = $USER->id;
        $catdef->name = get_string('default');
        $category = local_fm_category::create($catdef);
        $category->save();
        return $category;
    }

    protected $parentid;
    protected $name;
    protected $info;
    protected $infoformat;
    protected $_parent;
    protected $_children;

    function add_navigation(moodle_page $page){
        $bankurl = new moodle_url('/local/fm/bank/edit.php');

        $categorypath = $this->get_parent_categories();
        if ($this->exists()) {
            $categorypath[] = $this;
        }
        foreach($categorypath as $cat){
            $url = clone($bankurl);
            $url->param('category', $cat->id);
            $page->navbar->add($cat->get_name(), $url);
        }
    }

    function can_manage_feedback(){
        if (!$this->can_use()) {
            return false;
        }

        return local_fm_feedback::can_manage_in_context($this->get_context());
    }

    function delete($reorder = false){
        foreach($this->get_feedback() as $feedback){
            $feedback->delete(false);
        }

        parent::delete($reorder);
    }

    function get_children(){
        global $DB;

        if (!isset($this->_children)) {
            $this->_children = array();
            $records = $DB->get_records(static::DB_TABLE, array('parentid' => $this->id));
            foreach($records as $id => $record){
                $this->_children[$id] = static::create($record);
            }
        }

        return $this->_children;
    }

    function get_feedback(){
        return local_fm_feedback::get_all_in_category($this);
    }

    function get_info(){
        return format_text($this->info, $this->infoformat);
    }

    function get_name(){
        return $this->name;
    }

    function get_parent(){
        if (!isset($this->_parent)) {
            $this->_parent = static::get($this->parentid);
        }

        return $this->_parent;
    }

    function get_parentid(){
        return $this->parentid;
    }

    function get_parent_categories(){
        $current = $this;

        $parents = array();
        while($current->has_parent()){
            $next = $current->get_parent();
            array_unshift($parents, $next);
            $current = $next;
        }

        return $parents;
    }

    function has_feedback(){
        return local_fm_feedback::category_has_feedback($this);
    }

    function has_parent(){
        return !empty($this->parentid);
    }

    function get_db_record(){
        $record = parent::get_db_record();
        $record->parentid   = $this->parentid;
        $record->name       = $this->name;
        $record->info       = $this->info;
        $record->infoformat = $this->infoformat ? $this->infoformat : 0;

        return $record;
    }
}
